/**
 * @file bloom_fb.h
 * @author Martin Prajka
 * @brief Declaration Bloom_effect class
 */
#ifndef BLOOM_FB_H
#define BLOOM_FB_H

#include "framebuffer.h"

class Bloom_effect: public Framebuffer
{
	//framebuffers for bluring
	GLuint b_framebuffers[2];
	GLuint textures_bfb[2];

	GLuint hdr_fb;
	GLuint hdr_tex;
	GLuint hdr_rbo;

	GLuint prepare_shader;
	GLuint blur_shader;
	GLuint bloom_shader;

	unsigned final_texture;
	void init_bfb();
	void set_textures_bfb();
	void init_hdr_fb();

	void prepare_texture();

	int down_sample;
public:
	Bloom_effect() = default;
	virtual ~Bloom_effect();

	void set_blur_shader(const char *vertex, const char *fragment);
	void set_bloom_shader(const char *vertex, const char *fragment);
    void set_prepare_shader(const char *vertex, const char *fragment, const glm::vec3 &threshold);

    void init (int width, int height, int down_sample = 8);

    //prepare blur texture
	void active_bloom();
    //combine two textures original color and blur
	void draw_bloom();

	void debug_draw(GLuint previos_fb);
};
#endif
