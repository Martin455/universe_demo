/**
 * @author Martin Prajka
 * @file   local_fog.cpp
 */
#ifndef LOCAL_FOG_H
#define LOCAL_FOG_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include <array>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include "../window/object_properties.h"

#include <iostream>

class Local_fog
{
    //GL Object data
    GLuint vertex_array;
    GLuint alpha_texture;
    GLuint shader;
    std::array<GLuint, 2> buffers;

    void init_gl_data();

    //Particle data
    struct particle_data {
        glm::vec3 position;
        float scale;
        float collision;
    };
    int part_num;

    std::vector<particle_data> part_data;
    std::vector<GLfloat> gpu_data;

    void init_particles(int size);

    //for rand diffusion
    float diffusion;
    float alpha_disappear = 15.0f;
public:
    Local_fog() = default;
    ~Local_fog();

    void init(int size); //cube size
    void draw(glm::mat4 &perspective, glm::mat4 &view_matrix,
              glm::vec3 &position, std::vector<glm::vec3> &collisions_block,
              double time, float frametime);
};

#endif
