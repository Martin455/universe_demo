#version 330 core

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv;
layout(location=2) in vec3 normals;

out vec2 uv_coords;
out vec3 normal;
out vec3 pos;
out vec4 frag_pos_light;
out vec4 view_space;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model_matrix;
uniform mat4 light_space_matrix;
uniform mat4 bias;

void main()
{
	pos = vec3(model_matrix * vec4(position,1.0));
	normal = normalize(vec3(model_matrix * vec4(normals,0.0)));
    frag_pos_light = bias * light_space_matrix * vec4(pos, 1.0);
    uv_coords = uv;
    view_space = view * vec4(pos, 1.0);
    gl_Position = projection * view_space;
}
