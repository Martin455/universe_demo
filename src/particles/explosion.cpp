/**
 * @author Martin Prajka
 * @file   explosion.cpp
 */
#include "explosion.h"
#include "../window/glcheck_function.h"

Explosion::~Explosion()
{
    glDeleteBuffers((GLsizei)buffers.size(), buffers.data());
    glDeleteBuffers((GLsizei)stream_buffers.size(), stream_buffers.data());
    glDeleteTextures((GLsizei)textures.size(), textures.data());
    glDeleteVertexArrays((GLsizei)vertex_arrays.size(), vertex_arrays.data());

    glUseProgram(0);
    for (unsigned i = 0; i < shaders.size(); ++i)
    {
        glDeleteProgram(shaders[i]);
    }

    if (Audio_manager::instance()->playing_sound("explosion"))
    {
        Audio_manager::instance()->stop_sound("explosion");
    }
    Audio_manager::instance()->delete_sound("explosion");
}
void Explosion::init(Mesh &meteor)
{
    GLfloat mesh[] = {-0.07f, 0.07f,  0, 1, -0.07f, -0.07f, 0, 0,
                      0.07f,  -0.07f, 1, 0, 0.07f,  0.07f,  1, 1,
                      -0.07f, 0.07f,  0, 1, 0.07f,  -0.07f, 1, 0};

    glGenVertexArrays((GLsizei)vertex_arrays.size(), vertex_arrays.data());
    glGenBuffers((GLsizei)buffers.size(), buffers.data());
    glGenBuffers((GLsizei)stream_buffers.size(), stream_buffers.data());
    glGenTextures((GLsizei)textures.size(), textures.data());

    set_object_buffers(mesh, sizeof(mesh), meteor);
    set_vao_frf();
    set_vao_line_emiter();
    set_vao_shockwave();

    set_textures();
    set_shaders();

	gpu_frf.reserve(5 *
		(NUM_FIRE + NUM_ROCK + NUM_FLASH +
		(NUM_LINE_EMITERS * NUM_MAX_LINES)));
	gpu_emiter.reserve(3 * NUM_LINE_EMITERS);

    init_fire();
    init_rock();
    init_flash();
    init_line_emiter();

    shockwave_scale = 0.0f;

    first_repeat = true;
    last_time = 0.0;
    sound = false;
    Audio_manager::instance()->create_sound("explosion",
                                            "./sounds/explosion.ogg");
}

void Explosion::draw(glm::mat4 perspective, glm::mat4 view_matrix,
                     glm::vec3 &position, float frametime,
                     bool repeat_explosion, double time)
{
    if (!check_active() && !repeat_explosion)
        return;
    else if (!check_active())
    {
        if (first_repeat)
        {
            last_time = time;
            first_repeat = false;
        }
        if (last_time + 500 < time)
        {
            frf_data.clear();
            gpu_emiter.clear();

            init_fire();
            init_rock();
            init_flash();
            init_line_emiter();

            shockwave_scale = 0.0f;
            last_time = time;
            first_repeat = true;
        }
        else
        {
            return;
        }
    }
    this->perspective = perspective;
    transform_matrix = view_matrix * glm::translate(position);
    frame_time = frametime;

	update_fire();
	update_flash();
	update_line_emiter();
    update_rock();

    update_gpu();

    // play explosion sound
    if (!sound)
    {
        if (!Audio_manager::instance()->playing_sound("explosion"))
        {
            Audio_manager::instance()->set_min_distance("explosion", 20.0f,
                                                        10.0f);
            Audio_manager::instance()->set_sound_position("explosion",
                                                          position);

            Audio_manager::instance()->play_sound("explosion");
            sound = true;
        }
    }

    // line emiter draw first because it has not alpha channel
    draw_line_emiter(time);

    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);

    draw_frf();
    draw_shockwave();

	glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    GLCheck::checkGLerror(HERE);
}
// checking if every particle has done
bool Explosion::check_active()
{
    int hits = NUM_ROCK + NUM_FIRE + NUM_FLASH + NUM_LINE_EMITERS + 1;
    int active_fire_line = 0;
    for (const auto &x : frf_data)
    {
        if (!x.active)
            hits--;
    }

    for (const auto &x : line_emiter)
    {
        if (x.life < 0.0)
            hits--;

        for (const auto &y : x.fires)
        {
            if (y.active)
                active_fire_line++;
        }
    }

    if (shockwave_scale > 85)
        hits--;

    if (hits <= 0 && active_fire_line == 0)
    {
        if (Audio_manager::instance()->playing_sound("explosion"))
        {
            Audio_manager::instance()->stop_sound("explosion");
        }
        sound = false;
        return false;
    }

    return true;
}
/***
** Initialization particle functions
** */
void Explosion::init_fire()
{
    for (unsigned i = 0; i < NUM_FIRE; ++i)
    {
        particle_data data_fire;

        data_fire.position = glm::vec3(0, 0, 0);

        data_fire.life = 6.0f + rand() / float(RAND_MAX) * 8.0f;
        data_fire.speed =
            (0.2f + rand() / float(RAND_MAX) * 0.5f) * GLOBAL_SPEED;

        data_fire.active = true;

        float phi_f = rand() / float(RAND_MAX) * (M_PI * 2);
        float PHI_f = rand() / float(RAND_MAX) * M_PI;

        data_fire.direction.x = 0.06f * sin(PHI_f) * cos(phi_f);
        data_fire.direction.y = 0.06f * sin(PHI_f) * sin(phi_f);
        data_fire.direction.z = 0.06f * cos(PHI_f);
        data_fire.direction = glm::normalize(data_fire.direction);
        data_fire.kind = GLfloat(FIRE_PARTS);

        frf_data.push_back(data_fire);
    }
    dust_transition = 0.0f;
}
void Explosion::init_rock()
{
    for (unsigned i = 0; i < NUM_ROCK; ++i)
    {
        particle_data data_rock;
        data_rock.position = glm::vec3(0, 0, 0);
        data_rock.speed =
            (0.6f + rand() / float(RAND_MAX) * 1.2f) * GLOBAL_SPEED;
        data_rock.active = true;
        data_rock.life = 0.0f;

        float phi_p = rand() / float(RAND_MAX) * (M_PI * 2);
        float PHI_p = rand() / float(RAND_MAX) * M_PI;

        data_rock.direction.x = 0.1f * sin(PHI_p) * cos(phi_p);
        data_rock.direction.y = 0.1f * sin(PHI_p) * sin(phi_p);
        data_rock.direction.z = 0.1f * cos(PHI_p);
        data_rock.direction = glm::normalize(data_rock.direction);
        data_rock.kind = GLfloat(ROCK_PARTS);

        frf_data.push_back(data_rock);
    }
}
void Explosion::init_flash()
{
    flash_speed = (0.2f + rand() / float(RAND_MAX) * 0.2f) * GLOBAL_SPEED;

    for (int i = 0; i < NUM_FLASH; ++i)
    {
        particle_data data_flash;

        float phi = rand() / float(RAND_MAX) * (M_PI * 2);
        float PHI = rand() / float(RAND_MAX) * (M_PI);

        // angle without radius for position and direction
        float x_angle = sin(PHI) * cos(phi);
        float y_angle = sin(PHI) * sin(phi);
        float z_angle = cos(PHI);

        data_flash.position.x =
            (0.1f + rand() / float(RAND_MAX) * 0.15f) * x_angle;
        data_flash.position.y =
            (0.05f + rand() / float(RAND_MAX) * 0.05f) * y_angle;
        data_flash.position.z =
            (0.1f + rand() / float(RAND_MAX) * 0.15f) * z_angle;

        data_flash.direction =
            glm::normalize(glm::vec3(x_angle, y_angle, z_angle));
        data_flash.life = 2.0f * (flash_speed * 10.0f);
        data_flash.speed = -1.0f;

        data_flash.active = true;

        data_flash.kind = GLfloat(FLASH_PARTS);

        frf_data.push_back(data_flash);
    }
    is_back_direction = false;
	is_flash = true;
}
void Explosion::init_line_emiter()
{
    line_emiter.clear();
    line_emiter.resize(NUM_LINE_EMITERS);
    for (auto x = line_emiter.begin(); x != line_emiter.end(); ++x)
    {
        float phi = rand() / float(RAND_MAX) * (2 * M_PI);
        float PHI = rand() / float(RAND_MAX) * M_PI;

        float dir_x = 0.2f * sin(PHI) * cos(phi);
        float dir_y = 0.2f * sin(PHI) * sin(phi);
        float dir_z = 0.2f * cos(PHI);

        x->direction = glm::normalize(glm::vec3(dir_x, dir_y, dir_z));
        x->position = glm::vec3(dir_x, dir_y, dir_z);
        x->life = 8.0f + rand() / float(RAND_MAX) * 2.0f;
        x->speed = (0.25f + rand() / float(RAND_MAX)) * GLOBAL_SPEED;
        x->fires.clear();
    }
}
void Explosion::init_line(line_fire &emiter)
{
    for (unsigned i = 0; i < emiter.fires.size(); ++i)
    {
        if (!emiter.fires[i].active)
        {
            emiter.fires.erase(emiter.fires.begin() + i);
        }
    }
    if (emiter.fires.size() > (NUM_MAX_LINES - 1))
    {
        unsigned num_item = unsigned(rand() % NUM_MAX_LINES);
        emiter.fires.erase(emiter.fires.begin() + num_item);
    }
    particle_data data_for_lines;
    data_for_lines.position = emiter.position - (emiter.direction * 0.025f);
    data_for_lines.direction = glm::normalize(emiter.direction);
    data_for_lines.life = 10.0;
    data_for_lines.active = true;
    data_for_lines.speed = 0.2f * GLOBAL_SPEED;
    data_for_lines.kind = GLfloat(FIRE_LINE_PARTS);
    emiter.fires.push_back(data_for_lines);
}
/***
** Update particles functions
** */
void Explosion::update_rock()
{
    for (auto x = frf_data.begin(); x != frf_data.end(); ++x)
    {
        if (x->kind == ROCK_PARTS)
        {
            if (x->active)
            {
                x->position.x += x->direction.x * x->speed * frame_time;
                x->position.y += x->direction.y * x->speed * frame_time;
                x->position.z += x->direction.z * x->speed * frame_time;

                float len = glm::length(x->position);

                x->speed -= 0.02f * frame_time;
                if (x->speed <= 0.2f)
                    x->speed = 0.2f;

                if (len > 3.0)
                    x->active = false;
            }
        }
    }
}
void Explosion::update_fire()
{
    for (auto x = frf_data.begin(); x != frf_data.end(); ++x)
    {
        if (x->kind == FIRE_PARTS)
        {
            if (x->active)
            {
                x->position.x += x->direction.x * x->speed * frame_time;
                x->position.y += x->direction.y * x->speed * frame_time;
                x->position.z += x->direction.z * x->speed * frame_time;

                float len = glm::length(x->position);

                x->speed -= 0.18f * frame_time;
                if (x->speed <= 0.005f)
                    x->speed = 0.005f;

                if (x->life < 0.0f)
                    x->active = false;

                float lost_life = 10.0f * len;
                if (lost_life < 3.8f)
                    lost_life = 3.8f;
                x->life -= lost_life * frame_time;
            }
        }
    }
}
void Explosion::update_flash()
{
	int num_flash = 0;
    for (auto x = frf_data.begin(); x != frf_data.end(); ++x)
    {
        if (x->kind == FLASH_PARTS)
        {			
            if (x->active)
            {
                x->position += x->direction * flash_speed * frame_time;

                float length_particle = glm::length(x->position);

                if (length_particle > 0.34f && !is_back_direction)
                {
                    flash_speed *= -1.0f;
                    is_back_direction = true;
                }

                x->life -= frame_time;
                if ((x->life < 0.0f || length_particle < 0.2f) &&
                    is_back_direction)
                {
                    x->active = false;
                }

				num_flash++;
            }
        }
    }
	if (num_flash == 0)
		is_flash = false;
}
void Explosion::update_line_emiter()
{
    gpu_emiter.clear();
    for (auto x = line_emiter.begin(); x != line_emiter.end(); ++x)
    {
        if (x->life < 0.0f)
        {
            update_line_fire(*x);
            continue;
        }

        gpu_emiter.push_back(x->position.x);
        gpu_emiter.push_back(x->position.y);
        gpu_emiter.push_back(x->position.z);
 
        update_line_fire(*x);
        x->position += x->direction * x->speed * frame_time;

        x->life -= frame_time;
        init_line(*x);
    }

    if (gpu_emiter.empty())
        return;

}
void Explosion::update_line_fire(line_fire &emiter)
{
    if (emiter.fires.empty())
        return;
    for (unsigned i = 0; i < emiter.fires.size(); ++i)
    {
        if (emiter.fires[i].life < 0.0)
        {
            emiter.fires[i].active = false;
            continue;
        }
        emiter.fires[i].position +=
            emiter.fires[i].direction * emiter.fires[i].speed * frame_time;

        emiter.fires[i].life -= frame_time;
    }
}
void Explosion::update_gpu()
{
    std::vector<particle_data> copy_vec;    
    copy_vec = frf_data;
    for (auto x = line_emiter.begin(); x != line_emiter.end(); ++x)
    {
        copy_vec.insert(copy_vec.end(), x->fires.begin(), x->fires.end());
    }

    gpu_frf.clear();
	for (int i = (int)(copy_vec.size() - 1); i >= 0 ; --i)
	{//set to GPU in back direction because line fires must draw first
		if (copy_vec[i].active)
		{
			gpu_frf.push_back(copy_vec[i].position.x);
			gpu_frf.push_back(copy_vec[i].position.y);
			gpu_frf.push_back(copy_vec[i].position.z);
			gpu_frf.push_back(copy_vec[i].life);
			gpu_frf.push_back(float(copy_vec[i].kind));
		}
	}
}
/***
** Drawing methods
** */
void Explosion::draw_frf()
{
    glUseProgram(shaders[0]);
    // Setting multitexturing
    GLuint texture_location = glGetUniformLocation(shaders[0], "tex_a1");
    glUniform1i(texture_location, 0);
    texture_location = glGetUniformLocation(shaders[0], "tex_a2");
    glUniform1i(texture_location, 1);
    texture_location = glGetUniformLocation(shaders[0], "tex_a3");
    glUniform1i(texture_location, 2);
    texture_location = glGetUniformLocation(shaders[0], "tex_a4");
    glUniform1i(texture_location, 3);
    texture_location = glGetUniformLocation(shaders[0], "tex_color");
    glUniform1i(texture_location, 4);
    texture_location = glGetUniformLocation(shaders[0], "tex_color2");
    glUniform1i(texture_location, 5);
    texture_location = glGetUniformLocation(shaders[0], "tex_flash");
    glUniform1i(texture_location, 6);
    texture_location = glGetUniformLocation(shaders[0], "tex_rock");
    glUniform1i(texture_location, 7);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[SMOKET]);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[SMOKET2]);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, textures[SMOKET3]);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, textures[SMOKET4]);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, textures[FIRET]);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, textures[FIRET2]);

    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_2D, textures[FLASHT]);

    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, textures[ROCKT]);

    glBindVertexArray(vertex_arrays[0]);

    GLuint location = glGetUniformLocation(shaders[0], "per_mat");
    glUniformMatrix4fv(location, 1, GL_FALSE, &perspective[0][0]);
    location = glGetUniformLocation(shaders[0], "view");
    glUniformMatrix4fv(location, 1, GL_FALSE, &transform_matrix[0][0]);
    location = glGetUniformLocation(shaders[0], "back_direction");
    glUniform1i(location, is_back_direction);

    int random_value = rand() % 65536;
    location = glGetUniformLocation(shaders[0], "random_value");
    glUniform1i(location, random_value);

    // Transition to dust
    if (!is_flash)
    {
        dust_transition += 0.75f * frame_time;
        if (dust_transition > 1.0f)
            dust_transition = 1.0f;
    }
    location = glGetUniformLocation(shaders[0], "dust_transition");
    glUniform1f(location, dust_transition);

    glBindBuffer(GL_ARRAY_BUFFER, stream_buffers[0]);
    
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(GLfloat) * 5 *
		(NUM_FIRE + NUM_ROCK + NUM_FLASH +
		(NUM_LINE_EMITERS * NUM_MAX_LINES)),
		nullptr, GL_STREAM_DRAW);

    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 5 *
		(NUM_FIRE + NUM_ROCK + NUM_FLASH +
		(NUM_LINE_EMITERS * NUM_MAX_LINES)),
                 gpu_frf.data(), GL_STREAM_DRAW);

    glDrawArraysInstanced(GL_TRIANGLES, 0, 6, gpu_frf.size() / 5);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glActiveTexture(GL_TEXTURE0);
    GLCheck::checkGLerror(HERE);
}
void Explosion::draw_line_emiter(double time)
{
    glUseProgram(shaders[1]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[ROCKET]);

    glBindVertexArray(vertex_arrays[1]);
    GLuint location = glGetUniformLocation(shaders[1], "perspective");

    glUniformMatrix4fv(location, 1, GL_FALSE, &perspective[0][0]);

    location = glGetUniformLocation(shaders[1], "view");
    glUniformMatrix4fv(location, 1, GL_FALSE, &transform_matrix[0][0]);

    glm::mat4 rotate_matrix =
        glm::rotate(float(time * 0.75f), glm::vec3(1, 1, 1));
    location = glGetUniformLocation(shaders[1], "rotate");
    glUniformMatrix4fv(location, 1, GL_FALSE, &rotate_matrix[0][0]);

    glBindBuffer(GL_ARRAY_BUFFER, stream_buffers[1]);
    
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * NUM_LINE_EMITERS,
		nullptr, GL_STREAM_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * NUM_LINE_EMITERS,
                 gpu_emiter.data(), GL_STREAM_DRAW);

    glDrawArraysInstanced(GL_TRIANGLES, 0, count_rock_vertecies,
                          gpu_emiter.size() / 3);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    GLCheck::checkGLerror(HERE);
}
void Explosion::draw_shockwave()
{
    if (shockwave_scale > 85)
        return;

    glUseProgram(shaders[2]);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[SWT]);

    glBindVertexArray(vertex_arrays[2]);

    GLuint location = glGetUniformLocation(shaders[2], "view");
    glUniformMatrix4fv(location, 1, GL_FALSE, &transform_matrix[0][0]);

    location = glGetUniformLocation(shaders[2], "scale_number");
    glUniform1f(location, shockwave_scale);

    location = glGetUniformLocation(shaders[2], "perspective");
    glUniformMatrix4fv(location, 1, GL_FALSE, &perspective[0][0]);

    shockwave_scale += 40.00f * GLOBAL_SPEED * frame_time;
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindVertexArray(0);
    GLCheck::checkGLerror(HERE);
}
/***
** Methods for sending data to opengl
** */
void Explosion::set_object_buffers(GLfloat *mesh, size_t mesh_size, Mesh &rock)
{
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, mesh_size, mesh, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, rock.get_vertices_byte(),
                 rock.vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    count_rock_vertecies = rock.vertices.size();

    glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
    glBufferData(GL_ARRAY_BUFFER, rock.get_texture_uv_byte(),
                 rock.texture_uv.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Explosion::set_vao_frf()
{
    glBindVertexArray(vertex_arrays[0]);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    for (int i = 0; i < 2; ++i)
    {
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4,
                              (void *)(sizeof(GLfloat) * (i * 2)));
    }

    glBindBuffer(GL_ARRAY_BUFFER, stream_buffers[0]);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(GLfloat) * 5 *
                     (NUM_FIRE + NUM_ROCK + NUM_FLASH +
                      (NUM_LINE_EMITERS * NUM_MAX_LINES)),
                 nullptr, GL_STREAM_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5,
                          (void *)(sizeof(GLfloat) * 4));

    glVertexAttribDivisor(0, 0);
    glVertexAttribDivisor(1, 0);
    glVertexAttribDivisor(2, 1);
    glVertexAttribDivisor(3, 1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
void Explosion::set_vao_line_emiter()
{
    glBindVertexArray(vertex_arrays[1]);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, 0);
    glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, 0);

    glBindBuffer(GL_ARRAY_BUFFER, stream_buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * NUM_LINE_EMITERS,
                 nullptr, GL_STREAM_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, 0);

    glVertexAttribDivisor(0, 0);
    glVertexAttribDivisor(1, 0);
    glVertexAttribDivisor(2, 1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
void Explosion::set_vao_shockwave()
{
    glBindVertexArray(vertex_arrays[2]);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    for (int i = 0; i < 2; ++i)
    {
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4,
                              (void *)(sizeof(GLfloat) * (i * 2)));
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Explosion::set_textures()
{
    textures[SMOKET] = Texture::create_texture("./textures/smoke_texture.png");
    textures[SMOKET2] =
        Texture::create_texture("./textures/smoke_texture2.png");
    textures[SMOKET3] =
        Texture::create_texture("./textures/smoke_texture3.png");
    textures[SMOKET4] =
        Texture::create_texture("./textures/smoke_texture4.png");
    textures[ROCKET] =
        Texture::create_texture("./textures/meteor_textures.png");
    textures[ROCKT] = Texture::create_texture("./textures/rock_expl.png");
    textures[FIRET] = Texture::create_texture("./textures/fire.png");
    textures[FLASHT] = Texture::create_texture("./textures/flash_texture.png");
    textures[SWT] = Texture::create_texture("./textures/shockwave_texture.png");
    textures[FIRET2] = Texture::create_texture("./textures/fire2.png");
}
void Explosion::set_shaders()
{
    shaders[0] = Shader::create_program("./shaders/explosion_vert.glsl",
                                        "./shaders/explosion_frag.glsl");
    shaders[1] = Shader::create_program("./shaders/emiter_line_vert.glsl",
                                        "./shaders/emiter_line_frag.glsl");
    shaders[2] = Shader::create_program("./shaders/shockwave_vertex.glsl",
                                        "./shaders/shockwave_fragment.glsl");
}
