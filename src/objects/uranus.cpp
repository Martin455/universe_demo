/**
 * @author Martin Prajka
 * @file   uranus.cpp
 */
#include "uranus.h"
#include "../window/glcheck_function.h"

void Uranus::update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime)
{
	float x_move = (sun_position.x + deflect_axis) + (major_axis*cos(-deg_to_rad(angle)));
	float y_move = sun_position.z + (minor_axis*sin(-deg_to_rad(angle)));
	position = glm::vec3(x_move, 0, y_move);
	
    if (IS_FAST_QUADRANT(angle))
        angle += max_speed / 10.0f * frametime;
    else if (IS_AVG_QUADRANT(angle))
        angle += avg_speed /10.0f * frametime;
    else
        angle += min_speed / 10.0f * frametime;

	if (angle>=360.0f)
		angle = 0.0f;

	transform_matrix = translate(position) * scale_matrix *
					   glm::rotate(float(time*speed_rotate),glm::vec3(0.0,1.0,0.0));
	time_for_ring = time;

    glm::mat4 full_trans = viewMatrix * transform_matrix;
    
    GLuint uni_location = glGetUniformLocation(shader,"fulltrans_matrix");
    glUniformMatrix4fv(uni_location,1,GL_FALSE,&full_trans[0][0]);

    GLuint light_location = glGetUniformLocation(shader, "light_position");
    glUniform3fv(light_location,1,&sun_position[0]);

    GLuint model_location = glGetUniformLocation(shader,"model_matrix");
    glUniformMatrix4fv(model_location,1,GL_FALSE,&transform_matrix[0][0]);

    ring_shader.bind();
    //drawing ring
    ring.draw(viewMatrix, sun_position, position, time_for_ring, ring_shader.get_program_id(), 1);
    glUseProgram(shader);
}
void Uranus::draw()
{
	glBindVertexArray(vao);
	glBindTexture(GL_TEXTURE_2D,textureID);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

	GLCheck::checkGLerror(HERE);
}
void Uranus::init_ring(G_object &meteor)
{
	ring.init(meteor,70,4,22.0);
    ring_shader.set_shader("./shaders/planet_vertex.glsl","./shaders/planet_fragment.glsl");
}
