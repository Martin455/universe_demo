/**
 * @author Martin Prajka
 * @file   tower.cpp
 */
#include "tower.h"
#include "../window/glcheck_function.h"

Tower::~Tower()
{
	glDeleteBuffers(1,&light_vbo);
	glDeleteBuffers(1,&light_uvbo);
	glDeleteTextures(1,&light_texture);
	glDeleteVertexArrays(1,&light_vao);

	glUseProgram(0);
	glDeleteProgram(light_program);
}

void Tower::init(Mesh &mesh, const char* texture_name, Mesh &satelite_mesh, Mesh &bridge_mesh)
{
	G_object::init(mesh, texture_name);
	
	satelite.init(satelite_mesh, "./textures/satelite_texture.png");
    satelite.set_position(glm::vec3(-50,59.4f,-48));
	satelite.set_rotation(115.0f);

	bridge.init(bridge_mesh, "./textures/rocket_bridge_texture.png");
	
	init_light();

    model_matrix = glm::translate(glm::vec3(0.0f,-2.6f,-80.5f))
            * glm::rotate(-deg_to_rad(90),glm::vec3(0,1,0));
}

void Tower::init_light()
{
	GLfloat mesh[] =
	{
		-6.5,6.5,0.0,
		-6.5,-6.5,0.0,
		6.5,-6.5,0.0,
		6.5,6.5,0.0,
		-6.5,6.5,0.0,
		6.5,-6.5,0.0
	};

	GLfloat coords[] = 
	{
		0.0,1.0,
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
		1.0,0.0
	};

	glGenVertexArrays(1,&light_vao);
	glBindVertexArray(light_vao);

	glGenBuffers(1,&light_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(mesh), mesh, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*3,0);

	glGenBuffers(1,&light_uvbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_uvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*2,0);

	light_texture = Texture::create_texture("./textures/alpha_texture.png");
	light_program = Shader::create_program("./shaders/blink_light_vertex.glsl", "./shaders/blink_light_fragment.glsl");
	GLCheck::checkGLerror(HERE);
}

void Tower::draw_light(glm::mat4 &projection, glm::mat4 &view, float time)
{
	glBindVertexArray(light_vao);
	glBindTexture(GL_TEXTURE_2D, light_texture);
	glUseProgram(light_program);

	glm::mat4 model_matrix = view * glm::translate(glm::vec3(0.0f,115.6f,-80.2f));

	GLuint location = glGetUniformLocation(light_program, "projection");
	glUniformMatrix4fv(location, 1, GL_FALSE, &projection[0][0]);
	location = glGetUniformLocation(light_program, "transform_matrix");
	glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);
	location = glGetUniformLocation(light_program, "time");
	glUniform1f(location,time);
	
	glEnable(GL_BLEND);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisable(GL_BLEND);
	GLCheck::checkGLerror(HERE);
}

void Tower::prepare_depth_drawing(GLuint depth_shader)
{
    GLuint location = glGetUniformLocation(depth_shader, "modelMatrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);

    glDisable(GL_CULL_FACE);
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);
    glEnable(GL_CULL_FACE);

    bridge.prepare_depth(depth_shader);
    bridge.draw();

    satelite.prepare_depth(depth_shader);
    satelite.draw();

    GLCheck::checkGLerror(HERE);       
}

void Tower::draw(glm::mat4 &proj, glm::mat4 &viewMatrix, float time, Shader &shader, GLuint shadowmap)
{
	glBindVertexArray(vao);

    shader.set_mat4("model_matrix", model_matrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    satelite.update(shader, shadowmap);
	satelite.draw();

    bridge.update(shader, shadowmap);
    bridge.draw();
	
	draw_light(proj, viewMatrix, time);
    //set to default shader
    shader.bind();
    
    GLCheck::checkGLerror(HERE);
}
