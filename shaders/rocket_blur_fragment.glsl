#version 330 core

out vec4 frag_color;

uniform sampler2D new_color;
uniform sampler2D old_color;

in vec2 uv_coords;

float coef[25] = float[](0.003765 ,	0.015019 ,	0.023792, 	0.015019 ,	0.003765,
0.015019 ,	0.059912 ,	0.094907 ,	0.059912, 	0.015019,
0.023792 ,	0.094907 ,	0.150342 ,	0.094907 ,	0.023792,
0.015019 ,	0.059912 ,	0.094907 ,	0.059912, 	0.015019,
0.003765 ,	0.015019 ,	0.023792 ,	0.015019, 	0.003765);

void main(void)
{
    vec2 texel_size = (1.0/textureSize(old_color, 0)) * 2.5;
    vec3 result = vec3(0);
    for (int i = 0; i < 5; ++i)
    {
        for (int j = 0; j < 5; ++j)
        {
            result += texture(old_color, uv_coords + vec2(texel_size.x * i, texel_size.y * j)).rgb * coef[i * 5 + j];
        }
    }

    frag_color = vec4(mix(texture(new_color, uv_coords).rgb, result, 0.6), 1.0);
}
