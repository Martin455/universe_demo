/**
 * @author Martin Prajka
 * @file   sun.cpp
 */
#include "sun.h"
#include "../window/glcheck_function.h"

Sun::~Sun()
{
	glDeleteBuffers(1,&vbo);
	glDeleteBuffers(1,&uvbo);
	glDeleteBuffers(1,&nbo);
	glDeleteTextures(1,&textureID);
	glDeleteVertexArrays(1,&vao);

	glDeleteBuffers(2,sparkle_buff);
	glDeleteTextures(1,&sparkl_tex);
	glDeleteVertexArrays(1, &sparkle_vao);

}

void Sun::init(Mesh &mesh)
{
    G_object::init(mesh, "./textures/sun_texture.png");

    animate_eruption.init("./textures/particle_texture.png", true);

    for(auto x = eruptions.begin(); x != eruptions.end(); ++x)
    {
        x->init("./textures/particle_texture.png");
    }

    eruption_shader.set_shader("./shaders/eruption_vertex.glsl", "./shaders/eruption_fragment.glsl");

	sparkle.init();
	GLCheck::checkGLerror(HERE);
}

void Sun::draw(glm::mat4 projMat,glm::mat4 viewMat, double time,glm::vec3 &light, float frame_time, GLuint shader)
{
	glm::mat4 transformMat = projMat * viewMat;

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,textureID);
	
    glUseProgram(shader);

	glBindVertexArray(vao);
	
	glm::mat4 full_trans = transformMat * glm::translate(light) 
		* glm::scale(glm::vec3(SUN_ACROSS,SUN_ACROSS,SUN_ACROSS)) 
		* glm::rotate(deg_to_rad(time*0.02),glm::vec3(0,1,1));

	GLuint time_loc = glGetUniformLocation(shader,"time_in");
	glUniform1f(time_loc, (float)time);
	
	GLuint uni_location = glGetUniformLocation(shader,"fulltrans_matrix");
	glUniformMatrix4fv(uni_location,1,GL_FALSE,&full_trans[0][0]);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

	eruption_shader.bind();
	eruption_shader.set_mat4("projection", projMat);
	eruption_shader.set_mat4("view", viewMat);

	for (auto x = eruptions.begin(); x != eruptions.end(); ++x)
		x->draw(frame_time);
	if (time > 5250.0)
	{
		animate_eruption.draw(frame_time, false);
	}

	eruption_shader.unbind();

	//sparkle efekt
	sparkle.draw(projMat,viewMat, light);

    //return default shader
    glUseProgram(shader);
	glBindVertexArray(0);
}
