/**
 * @author Martin Prajka
 * @file earth.cpp
 */
#include "earth.h"
#include "../window/glcheck_function.h"

void Earth::update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime)
{
	float x_move = (sun_position.x + deflect_axis) + (major_axis*cos(-deg_to_rad(angle)));
	float y_move = sun_position.z + (minor_axis*sin(-deg_to_rad(angle)));
	position = glm::vec3(x_move, 0.0f, y_move);
	
	if (IS_FAST_QUADRANT(angle))
        angle += max_speed / 10.0f * frametime;
	else if (IS_AVG_QUADRANT(angle))
        angle += avg_speed /10.0f * frametime;
	else
        angle += min_speed / 10.0f * frametime;

	if (angle>=360.0f)
		angle = 0.0f;

	transform_matrix = translate(position) * scale_matrix *
					   glm::rotate(float(time*speed_rotate),glm::vec3(0.0,1.0,0.0));

    glm::mat4 full_trans = viewMatrix * transform_matrix;
    
    GLuint uni_location = glGetUniformLocation(shader,"fulltrans_matrix");
    glUniformMatrix4fv(uni_location,1,GL_FALSE,&full_trans[0][0]);

    GLuint light_location = glGetUniformLocation(shader, "light_position");
    glUniform3fv(light_location,1,&sun_position[0]);

    GLuint model_location = glGetUniformLocation(shader,"model_matrix");
    glUniformMatrix4fv(model_location,1,GL_FALSE,&transform_matrix[0][0]);

    default_shader = shader;
    if (clouds!=nullptr)
    {
        glm::vec3 scale_vector = glm::vec3(scale_matrix[0][0] + 0.05f,scale_matrix[1][1] + 0.05f,
                scale_matrix[2][2] + 0.05f);
        cloud_matrix =  translate(position) * glm::scale(scale_vector) *
                        glm::rotate(deg_to_rad(200),glm::vec3(0,0,1)) * 
                        glm::rotate(float(time*0.003),glm::vec3(0.0,1.0,0.0));
        cloud_shader.bind();
        clouds->update(viewMatrix, cloud_matrix, sun_position, cloud_shader.get_program_id());
        glUseProgram(shader);        
    }
}

void Earth::draw()
{
	glBindVertexArray(vao);
	glBindTexture(GL_TEXTURE_2D,textureID);	

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

	if (clouds!=nullptr)
	{
		glEnable(GL_BLEND);
        cloud_shader.bind();	
		clouds->draw();
        //Hack for binding default shader
        glUseProgram(default_shader);
		glDisable(GL_BLEND);
	}

	GLCheck::checkGLerror(HERE);
}

void Earth::set_clouds()
{
	Loader loader;
	Mesh clouds_mesh = loader.load_object("./models/planet_model.obj");

	clouds = new G_object();
	clouds->init(clouds_mesh,"./textures/earthcloudmap.png");
    cloud_shader.set_shader("./shaders/planet_vertex.glsl","./shaders/planet_fragment.glsl");
}

Earth::~Earth()
{
	if (clouds!=nullptr)
	{
		delete clouds;
	}
}
