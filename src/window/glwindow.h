/**
 * @author Martin Prajka
 * @brief  Window Class file
 * @file   glwindow.h
 */
#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <fstream>
#include <vector>
#include "basic_function.h"
#include "../scenes/universe_scene.h"
#include "../scenes/shuttle_scene.h"
#include "../scenes/flight_scene.h"
#include "../constants.h"
#include "animation_camera.h"

/**
 * @brief The GLWindow class manage GLFW window
 *        and initialize OpenGL
 */
class GLWindow 
{
public:
    GLWindow() = default;
	~GLWindow();
    /**
     * @brief init, initialize OpenGL and all data
     */
	void init();
    /**
     * @brief exec, execute demo process
     */
	void exec();

	Basic_window_function *basic_function = nullptr;

private:
	GLFWwindow *window = nullptr;
    Animation_camera camera;
    bool vsync;
	std::vector<Scene*> scenes; //list of scenes
    Scene *active_scene; //link to scene in progress

    bool end_application;

    /**
     * @brief initializeGL setting OpenGL
     * @details init glew extension set blend, cullface, depth test and allow multisample
     */
	void initializeGL();
    /**
     * @brief init_vector_scenes, create all scenes of the demo
     */
	void init_vector_scenes();
    /**
     * @brief delete_scenes, clean from scenes
     */
	void delete_scenes();
	
	//events
    static void resizeGL(GLFWwindow *window,int _width,int _height);
    //global keyevent
    static void key_event(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void APIENTRY debug_output(GLenum source, GLenum type, GLuint id, GLenum severity,
		GLsizei length, const GLchar *message, const void *userParam);
    //config function (settings window)
    void configuration(bool &cursor);
};



#endif
