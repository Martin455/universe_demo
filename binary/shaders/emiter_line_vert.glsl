#version 330 core 

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv_coords;
layout(location=2) in vec3 instanced_position;

mat4 translate_matrix = mat4(1.0,0.0,0.0,0.0,
							 0.0,1.0,0.0,0.0,
							 0.0,0.0,1.0,0.0,
							 0.0,0.0,0.0,1.0);
uniform mat4 perspective;
uniform mat4 view;
uniform mat4 rotate;

out vec2 uv;


void main()
{
	translate_matrix[3].xyz = instanced_position;

	mat4 model_view = view * translate_matrix * rotate;
	vec3 scale_pos = position * 0.07;

	uv = uv_coords;
	gl_Position = perspective * model_view * vec4(scale_pos,1.0);
}