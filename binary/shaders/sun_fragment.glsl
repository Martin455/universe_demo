#version 330 core

in vec2 uv_coords;

out vec4 color;


uniform sampler2D tex;
uniform float time_in;


// float rand(vec2 v)
// {
// 	return fract(sin(dot(v.xy,vec2(12.9898,78.233)))*43758.5453);
// }

void main()
{
	vec4 tex_color =  texture(tex,uv_coords);
	float time;

	if (time_in < 6000.0)
		time = time_in * 2;
	else
		time = time_in;

	float blue_cos = abs(cos(time*0.0215)*sin(tex_color.b)); 
	float tmp1 = tex_color.r * 0.0395 * time * blue_cos/4.0;
	float tmp2 = tex_color.g * 0.0235 * time * blue_cos/2.0*0.2;
	float tmp = abs((cos(tmp1)*sin(tmp2))/2.0)+1.1;

	float c_tmp1 =  uv_coords.x * 0.0335 *time;
	float c_tmp2 = uv_coords.y * 0.0120 *time;
	float c_tmp = abs((cos(c_tmp1)*sin(c_tmp2))/2.0+1);
		
	if (c_tmp>0.5)
	{
		color = tex_color * (tmp + (c_tmp+0.35));
	}
	else
	{
		color = tex_color * tmp;
	}

	float brightness = (color.r * 0.3394) + (color.g * 0.2570) + (color.b * 0.0488);
	color *= brightness;
		
	color = vec4(color.rgb, 1.0);

}
