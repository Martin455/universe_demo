/**
 * @author Martin Prajka
 * @file   local_fog.h
 */
#include "local_fog.h"

Local_fog::~Local_fog()
{
    glDeleteBuffers((GLsizei)buffers.size(), buffers.data());
    glDeleteTextures(1, &alpha_texture);
    glDeleteVertexArrays(1, &vertex_array);

    glUseProgram(0);
    glDeleteProgram(shader);
}

void Local_fog::init_gl_data()
{
    GLfloat mesh[] = 
    {   //obj   //uv
        -1, 1,  0,1,
        -1,-1,  0,0,
         1,-1,  1,0,
         1, 1,  1,1,
        -1, 1,  0,1,
         1,-1,  1,0
    };

    glGenVertexArrays(1, &vertex_array);
    glBindVertexArray(vertex_array);

    glGenBuffers((GLsizei)buffers.size() , buffers.data());
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mesh),mesh, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, (void*)(sizeof(GLfloat) * 2));

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 5 * part_num, nullptr, GL_STREAM_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (void *)(sizeof(GLfloat)*4));

    glVertexAttribDivisor(0,0);
    glVertexAttribDivisor(1,0);
    glVertexAttribDivisor(2,1);
    glVertexAttribDivisor(3,1);

    alpha_texture = Texture::create_texture("./textures/smoke_texture.png");
    shader = Shader::create_program("./shaders/cloud_vertex.glsl", "./shaders/cloud_fragment.glsl");
}

void Local_fog::init_particles(int size)
{
    part_data.resize(part_num);
	gpu_data.reserve(5 * part_num);
    glm::vec3 starting_pos;

    for (unsigned i = 0; i < part_data.size(); ++i)
    {
        float diff_x = -(diffusion/2.0f) + (rand()/float(RAND_MAX) * diffusion);
        float diff_y = -(diffusion/2.0f) + (rand()/float(RAND_MAX) * diffusion);
        float diff_z = -(diffusion/2.0f) + (rand()/float(RAND_MAX) * diffusion);

        float size_x = size * 1.80f + diff_x;
        float size_y = size * 1.40f + diff_y;
        float size_z = size * 1.25f + diff_z;

        float x = -(size_x/2.0f) + (rand()/float(RAND_MAX) * size_x);
        float y = -(size_y/2.0f) + (rand()/float(RAND_MAX) * size_y);
        float z = -(size_z/2.0f) + (rand()/float(RAND_MAX) * size_z);

        part_data[i].scale = 6 + (rand()/float(RAND_MAX) * 22);

        part_data[i].collision = 1.0f;

        int del = i % 5;
        switch (del)
        {
            case 0:
                starting_pos = glm::vec3(0,0,rand()/float(RAND_MAX));
                break;
            case 1:
                starting_pos = glm::vec3(size/2,size/2,rand()/float(RAND_MAX) * 2.0f);
                break;
            case 2:
                starting_pos = glm::vec3(rand()/float(RAND_MAX),-size/3,rand()/float(RAND_MAX) * (-2.0f));
                break;
            case 3:
                starting_pos = glm::vec3(-size/2.0f,size/3.0f,rand()/float(RAND_MAX));
                break;
            case 4:   
                starting_pos = glm::vec3(-size/4.0f,-size/4.0f,rand()/float(RAND_MAX));
                break;         
        }

        part_data[i].position = starting_pos + glm::vec3(x,y,z);
    }
}

void Local_fog::init(int size)
{
    part_num = size * 3;
    diffusion = size / 5.2f;
    init_gl_data();
    init_particles(size);
}

void Local_fog::draw(glm::mat4 &perspective, glm::mat4 &view_matrix, glm::vec3 &position,
                     std::vector<glm::vec3> &collisions_block, double time, float frametime)
{
    //count transform matrix
    glm::mat4 transform = view_matrix * glm::translate(position);

    //get distance of camera
    for (auto x = part_data.begin(); x != part_data.end(); ++x)
    {
        //counting collision
        //get real values
        glm::vec3 real_pos = position + x->position;

        //c^2 = a^2 + b^2
        //a = b = 1 * scale = scale
        float collision_len = std::sqrt((x->scale * x->scale) * 2);

        //first limit
        glm::vec3 positive_block = collisions_block[0] + alpha_disappear;
        glm::vec3 negative_block = collisions_block[1] - alpha_disappear;
        bool x_fpositive = real_pos.x < positive_block.x || (real_pos.x - collision_len) < positive_block.x;
        bool x_fnegative = real_pos.x > negative_block.x || (real_pos.x + collision_len) > negative_block.x;

        bool y_fpositive = real_pos.y < positive_block.y || (real_pos.y - collision_len) < positive_block.y;
        bool y_fnegative = real_pos.y > negative_block.y || (real_pos.y + collision_len) > negative_block.y;

        bool z_fpositive = real_pos.z < positive_block.z || (real_pos.z - collision_len) < positive_block.z;
        bool z_fnegative = real_pos.z > negative_block.z || (real_pos.z + collision_len) > negative_block.z;

        //searching if point is between limits
        bool x_positive = real_pos.x < collisions_block[0].x || (real_pos.x - collision_len) < collisions_block[0].x;
        bool x_negative = real_pos.x > collisions_block[1].x || (real_pos.x + collision_len) > collisions_block[1].x;
        
        bool y_positive = real_pos.y < collisions_block[0].y || (real_pos.y - collision_len) < collisions_block[0].y;
        bool y_negative = real_pos.y > collisions_block[1].y || (real_pos.y + collision_len) > collisions_block[1].y;
        
        bool z_positive = real_pos.z < collisions_block[0].z || (real_pos.z - collision_len) < collisions_block[0].z;
        bool z_negative = real_pos.z > collisions_block[1].z || (real_pos.z + collision_len) > collisions_block[1].z;
        

        //check if is collision
        if (x_fpositive && x_fnegative && y_fpositive && y_fnegative && z_fpositive && z_fnegative)
        {
            x->collision -= 0.25f * 60 * frametime; // expect that app running in 60fps

            if (x_positive && x_negative && y_positive && y_negative && z_positive && z_negative)
            {
                x->collision = -0.5f;
            }
        }
        else
        {
            x->collision = 1.0f;
        }
    }

    //setting GPU
    gpu_data.clear();
    for (auto x = part_data.begin(); x != part_data.end(); ++x)
    {
		if (x->collision > -0.1f)
		{
			gpu_data.push_back(x->position.x);
			gpu_data.push_back(x->position.y);
			gpu_data.push_back(x->position.z);
			gpu_data.push_back(x->scale);
			gpu_data.push_back(x->collision);
		}
    }

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 5 * part_num, nullptr, GL_STREAM_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 5 * part_num, gpu_data.data(), GL_STREAM_DRAW);

    //DRAWING
    glUseProgram(shader);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, alpha_texture);

    glBindVertexArray(vertex_array);

    GLuint location = glGetUniformLocation(shader, "perspective");
    glUniformMatrix4fv(location, 1, GL_FALSE, &perspective[0][0]);
    location = glGetUniformLocation(shader, "transform_matrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &transform[0][0]);
    location = glGetUniformLocation(shader, "rotate_angle");
    glUniform1f(location,float(time*0.095));

    glEnable(GL_BLEND);    
	glDepthMask(GL_FALSE);

	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, gpu_data.size()/5);

    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);

    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
}
