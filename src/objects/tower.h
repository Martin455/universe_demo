/**
 * @author Martin Prajka
 * @file   tower.h
 */
#ifndef TOWER_H
#define TOWER_H

#include "g_object.h"
#include "../constants.h"
#include "satelite.h"
#include "rocket_bridge.h"

class Tower: public G_object
{
	Satelite satelite;
	Rocket_bridge bridge;

	GLuint light_vao;
	GLuint light_vbo;
	GLuint light_uvbo;
	GLuint light_texture;
	GLuint light_program;

	void init_light();
	void draw_light(glm::mat4 &projection, glm::mat4 &view, float time);

    glm::mat4 model_matrix;
public:
	Tower() {}
	virtual ~Tower();
	
    void prepare_depth_drawing(GLuint depth_shader);
	void init(Mesh &mesh, const char* texture_name, Mesh &satelite_mesh, Mesh &bridge_mesh);
    void draw(glm::mat4 &proj, glm::mat4 &viewMatrix, float time, Shader &shader, GLuint shadowmap);
};

#endif
