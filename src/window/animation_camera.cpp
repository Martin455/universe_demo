/**
 * @author Martin Prajka
 * @brief  file contains implements of Animation_camera class
 * @file   animation_camera.cpp
 */
#include "animation_camera.h"

void Animation_camera::look_around()
{
    look_data *data = &(look_queue.front());

	if (animation_time >= data->look_start_time && animation_time < data->look_end_time)
	{
		float angle = float((animation_time - data->look_start_time) * (double(data->end_angle) / (data->look_end_time - data->look_start_time)));
		viewDirection = glm::vec3(glm::rotate(deg_to_rad(angle), glm::vec3(data->axis_vec, 0.0f)) * glm::vec4(default_view, 0.0f));
	}
	else if (animation_time >= data->look_end_time)
	{
		viewDirection = glm::vec3(glm::rotate(deg_to_rad(data->end_angle), glm::vec3(data->axis_vec, 0.0f)) * glm::vec4(default_view, 0.0f));
		default_view = viewDirection;
		look_queue.pop();
	}
}

void Animation_camera::look_at()
{
    look_data *data = &(look_queue.front());

	if (animation_time >= data->look_start_time && animation_time < data->look_end_time)
	{
		viewDirection = glm::normalize(data->look_point - position);
	}
	else if (animation_time >= data->look_end_time)
	{
		default_view = viewDirection;
		look_queue.pop();
	}
}

void Animation_camera::linear_move()
{
	move_data *data = &(move_queue.front());

	if (animation_time >= data->move_start_time && animation_time < data->move_end_time)
	{
		float normalize_time = float((animation_time - data->move_start_time) / (data->move_end_time - data->move_start_time));
		position = (1.0f - normalize_time) * data->start_position + normalize_time * data->end_position;
	}
	else if (animation_time >= data->move_end_time)
	{
		position = data->end_position;
		move_queue.pop();
	}
}

void Animation_camera::quadratic_bezier()
{
    move_data *data = &(move_queue.front());

	if (animation_time >= data->move_start_time && animation_time < data->move_end_time)
	{
		float normalize_time = float((animation_time - data->move_start_time) / (data->move_end_time - data->move_start_time));

		position = float(std::pow((1.0f - normalize_time), 2.0)) * data->start_position +
		           2.0f * normalize_time * (1.0f - normalize_time) * data->control1 +
		           float(std::pow(normalize_time, 2.0)) * data->end_position;
	}
	else if (animation_time >= data->move_end_time)
	{
		position = data->end_position;
		move_queue.pop();
	}

}

void Animation_camera::cubic_bezier()
{
    move_data *data = &(move_queue.front());

	if (animation_time >= data->move_start_time && animation_time < data->move_end_time)
	{
		float normalize_time = float((animation_time - data->move_start_time) / (data->move_end_time - data->move_start_time));

		position = float(std::pow((1.0f - normalize_time), 3.0)) * data->start_position +
			3.0f * normalize_time * float(std::pow((1.0f - normalize_time), 2.0)) * data->control1 +
			3.0f * float(std::pow(normalize_time, 2.0)) * (1.0f - normalize_time) * data->control2 +
			float(std::pow(normalize_time, 3.0)) * data->end_position;
	}
	else if (animation_time >= data->move_end_time)
	{
		position = data->end_position;
		move_queue.pop();
	}
}

void Animation_camera::cut()
{
    cut_data *data = &(cut_queue.front());

	if (animation_time >= data->cut_start_time && animation_time < data->cut_end_time)
	{
		transition = float(double(default_transition) + double(animation_time - data->cut_start_time)
			               * ((double(data->end_state) - double(default_transition)) / (data->cut_end_time - data->cut_start_time)));
	}
	else if (animation_time > data->cut_end_time)
	{
		transition = data->end_state;
		default_transition = transition;
		cut_queue.pop();
	}
}

void Animation_camera::take_look_around(float end_angle, const glm::vec2 & axis_vec, double start_time, double end_time)
{
	look_data data;
	data.end_angle = end_angle;
	data.axis_vec = axis_vec;
	data.look_start_time = start_time;
	data.look_end_time = end_time;
	data.k = kind_look::angle;

	look_queue.push(data);
}

void Animation_camera::look_at_point(glm::vec3 look_point, double start_time, double end_time)
{
	look_data data;
	data.look_start_time = start_time;
	data.look_end_time = end_time;
	data.look_point = look_point;
	data.k = kind_look::lookat;

	look_queue.push(data);
}

void Animation_camera::move_to_point(const glm::vec3 & start_point, const glm::vec3 & target_point, double start_time, double end_time)
{
	move_data data;
	data.start_position = start_point;
	data.end_position = target_point;
	data.move_start_time = start_time;
	data.move_end_time = end_time;
	data.k = kind_move::linear;
	
	move_queue.push(data);
}

void Animation_camera::move_quadratic_bezier(const glm::vec3 & start_point, const glm::vec3 & target_point, const glm::vec3 & control_point, double start_time, double end_time)
{
	move_data data;
	data.start_position = start_point;
	data.end_position = target_point;
	data.move_start_time = start_time;
	data.move_end_time = end_time;
	data.control1 = control_point;
	data.k = kind_move::quadratic;

	move_queue.push(data);
}


void Animation_camera::move_cubic_bezier(const glm::vec3 & start_point, const glm::vec3 &target_point, const glm::vec3 &control_point1, const glm::vec3 &control_point2, double start_time, double end_time)
{
	move_data data;
	data.start_position = start_point;
	data.end_position = target_point;
	data.move_start_time = start_time;
	data.move_end_time = end_time;
	data.control1 = control_point1;
	data.control2 = control_point2;
	data.k = kind_move::cubic;

	move_queue.push(data);
}

void Animation_camera::set_cut(double start_time, double end_time, bool to_dark)
{
	cut_data data;
	data.cut_start_time = start_time;
	data.cut_end_time = end_time;
	if (to_dark)
	{
		data.end_state = 0.0f;
	}
	else
	{
		data.end_state = 1.0f;
	}
	data.k = kind_cut::transition;
	default_transition = transition;
	cut_queue.push(data);
}

void Animation_camera::no_cut(double time)
{
    cut_data data;
    data.cut_end_time = time;
    data.k = kind_cut::nothing;

    cut_queue.push(data);
}

void Animation_camera::set_transition(float value)
{
    transition = value;
}

void Animation_camera::animate_camera(double time)
{
	animation_time = time;	
    if (!look_queue.empty())
    {
        switch (look_queue.front().k)
        {
        case kind_look::angle:
            look_around();
            break;
        case kind_look::lookat:
            look_at();
            break;
        }
    }
	if (!move_queue.empty())
	{	
        switch (move_queue.front().k)
        {
        case kind_move::linear:
            linear_move();
            break;
        case kind_move::quadratic:
            quadratic_bezier();
            break;
        case kind_move::cubic:
            cubic_bezier();
            break;
        }
    }
    if (!cut_queue.empty())
    {
        cut();
    }
}

void Animation_camera::clear_animation()
{
    while(!look_queue.empty())
    {
        look_queue.pop();
    }
    while(!move_queue.empty())
    {
        move_queue.pop();
    }
    while(!cut_queue.empty())
    {
        cut_queue.pop();
    }

	animation_time = 0.0;
    look_animation = false;
    move_animation = false;
    cut_animation = false;
	default_view = glm::vec3(0.f, 0.f, -1.f);
}

float Animation_camera::get_transition() const
{
    return transition;
}
