/**
 * @author Martin Prajka
 * @file   sparkle.h
 */
#ifndef SPARKLE_H
#define SPARKLE_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include "../window/object_properties.h"

class Sparkle
{
	GLuint vao;
	GLuint program;
	GLuint a_tex;

    GLuint vbo;

public:
    Sparkle() = default;
	~Sparkle();

	void init();
	void draw(glm::mat4 &perspective, glm::mat4 &view, glm::vec3 &sun_position);
};


#endif
