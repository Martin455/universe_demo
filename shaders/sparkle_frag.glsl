#version 330 core

out vec4 color;

in vec2 uv;

uniform sampler2D tex;

void main()
{
	float alpha = texture(tex,uv).g;

	color = vec4(1.0,0.2,0.0,alpha);
}