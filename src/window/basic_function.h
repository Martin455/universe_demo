/**
 * @author Martin Prajka
 * @file basic_function.h
 */
#ifndef BASIC_WINDOW_FUNCTION_H
#define BASIC_WINDOW_FUNCTION_H

#include <GLFW/glfw3.h>
#include <string>
#include "../constants.h"

/**
 * @brief The Basic_window_function class implements methods for counting frametime and set or unset cursor
 */
class Basic_window_function
{
	double start_fps; //start time for fps

    float frame_time;
    double last_time;

    int msaa_value;
    int ssaa_value;

	bool cursor;
	GLFWwindow *window;
public:
	Basic_window_function(GLFWwindow *win, bool _cursor);
	int width;
	int height;

	void switch_cursor_visible();
	bool is_cursor();
	void fps_counter(double scene_time);
    void init_frame_time();
    void frame_time_counter();
    void set_msaa(int value);
    void set_ssaa(int value);

    float get_frametime();
    int get_msaa();
    int get_ssaa();
};

#endif
