/**
 * @author Martin Prajka
 * @file   terrain.h
 */
#ifndef TERRAIN_H
#define TERRAIN_H

#include <GL/glew.h>
#include <SOIL/SOIL.h>
#include <array>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../loader/mesh.h"
#include "../window/object_properties.h"

/**
 * @brief The Terrain class generate terrain from heightmap and draw it
 */
class Terrain
{
    unsigned char *image_data;
    int width, length;
    const float start = -0.5f;

    Mesh gen_terrain(const char *heightmap, float size, float height);

    void get_image_data(const char * heightmap);
    void calculating_normals(Mesh &mesh);

    //GL
    GLuint vao;
    std::array<GLuint, 4> buffers;
    GLuint texture;

    GLuint triangles;

    glm::mat4 model_matrix;

public:
    virtual ~Terrain();

    void init(const char *heightmap, float size, float height);
    void set_texture(const char *filename);
    void update(Shader &shader, GLuint shadowmap);
    void draw_terrain();

    float get_height(float x, float z);
};

#endif
