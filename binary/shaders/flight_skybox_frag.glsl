#version 330 core

in vec2 uv_coords;

out vec4 color;

uniform sampler2D tex;
uniform sampler2D tex2;

uniform float dark_mix;
uniform float star_mix;

uniform bool dark_sky;
uniform bool mix_second_texture;

vec3 dark_color = vec3(0.05f, 0.05f, 0.05f);

void main()
{
    color = vec4(texture(tex,uv_coords).rgb, 1.0);

    if (dark_sky)
    {
        float mix_const = dark_mix/32;

        color = vec4(mix(color.rgb, dark_color, mix_const), 1.0);

        if (mix_second_texture)
        {
            mix_const = star_mix/20;

            vec3 texel2 = texture(tex2, uv_coords).rgb;

            color = vec4 (mix(color.rgb, texel2, mix_const), 1.0);
        }
    }

}