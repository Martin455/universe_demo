/**
 * @file   loader.h
 * @author Martin Prajka
 */
#ifndef LOADER_H
#define LOADER_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "mesh.h"

class Loader
{
public:
	Loader() = default;
	Mesh load_object(const char *file_name);
};

#endif
