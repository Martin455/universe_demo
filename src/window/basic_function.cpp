/**
 * @author Martin Prajka
 * @file basic_function.cpp
 */
#include "basic_function.h"
#include <iostream>

Basic_window_function::Basic_window_function(GLFWwindow *win, bool _cursor)
{
	window = win;
	cursor = _cursor;

    start_fps = glfwGetTime();
    msaa_value = 4;
    ssaa_value = 1;
}

void Basic_window_function::fps_counter(double scene_time)
{
	static unsigned frames = 0;
	double now = glfwGetTime();	
	double tmp = now - start_fps;

	frames++;
	if (tmp >= 1.0)
	{
		double fps = frames/tmp;
		frames = 0;
		start_fps += tmp;

#ifdef NDEBUG
		(void)scene_time; //delete warning
        std::string title = "Universe demo: " + std::to_string(fps) + " fps";
        std::cout << title << std::endl;
#else
        std::string title = "Universe debug: " + std::to_string(fps) + " fps SceneTime: " + std::to_string(scene_time);
        glfwSetWindowTitle(window,title.c_str());
#endif
	}
}

bool Basic_window_function::is_cursor()
{
	return cursor;
}

void Basic_window_function::switch_cursor_visible()
{
    if (cursor)
        glfwSetInputMode(window,GLFW_CURSOR,GLFW_CURSOR_DISABLED);
    else
        glfwSetInputMode(window,GLFW_CURSOR,GLFW_CURSOR_NORMAL);
    cursor = !cursor;
}

void Basic_window_function::init_frame_time()
{
    last_time = glfwGetTime();
    frame_time = 0;
}

void Basic_window_function::frame_time_counter()
{
    double current_time = glfwGetTime();
    frame_time = (float)(current_time - last_time);
    last_time = current_time;
}

void Basic_window_function::set_msaa(int value)
{
    if (value < 1 || value > 8)
    {
        msaa_value = 4;
        return;
    }
    msaa_value = value;
}

void Basic_window_function::set_ssaa(int value)
{
    if (value < 1 || value > 8)
    {
        ssaa_value = 4;
        return;
    }
    ssaa_value = value;
}

float Basic_window_function::get_frametime()
{
    return frame_time;
}

int Basic_window_function::get_msaa()
{
    return msaa_value;
}

int Basic_window_function::get_ssaa()
{
    return ssaa_value;
}
