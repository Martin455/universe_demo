/**
 * @author Martin Prajka
 * @file rocket_bridge.cpp
 */
#include "rocket_bridge.h"
#include "../window/glcheck_function.h"

void Rocket_bridge::init(Mesh &mesh, const char *texture_name)
{
    G_object::init(mesh, texture_name);

    model_matrix = glm::translate(glm::vec3(26,75.4,-75))
            * glm::scale(glm::vec3(22,12,5))
            * glm::rotate(deg_to_rad(90), glm::vec3(0,1,0));
}

void Rocket_bridge::prepare_depth(GLuint depth_shader)
{
    GLuint location = glGetUniformLocation(depth_shader, "modelMatrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);

    GLCheck::checkGLerror(HERE);       
}

void Rocket_bridge::update(Shader &shader, GLuint shadowmap)
{
    shader.set_mat4("model_matrix", model_matrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);

    GLCheck::checkGLerror(HERE);
}
void Rocket_bridge::draw()
{
	glBindVertexArray(vao);

    glEnable(GL_BLEND);
    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);
    glDisable(GL_BLEND);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    GLCheck::checkGLerror(HERE);
}
