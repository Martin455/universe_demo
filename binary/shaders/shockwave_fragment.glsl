#version 330 core

in vec2 uv;
in float life;
out vec4 out_color;

uniform sampler2D tex;

void main()
{
	//max life 85
	float sub_alpha = 1.0/85.0 * life;
	vec4 texel = texture(tex, uv);

	texel.a -= sub_alpha;

	if (texel.a < 0.05)
		discard;

	out_color = texel;
}