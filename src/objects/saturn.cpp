/**
 * @author Martin Prajka
 * @file   saturn.cpp
 */
#include "saturn.h"
#include "../window/glcheck_function.h"

void Saturn::update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime)
{
	float x_move = (sun_position.x + deflect_axis) + (major_axis*cos(-deg_to_rad(angle)));
	float y_move = sun_position.z + (minor_axis*sin(-deg_to_rad(angle)));
	position = glm::vec3(x_move, 0, y_move);
	
    if (IS_FAST_QUADRANT(angle))
        angle += max_speed / 10.0f * frametime;
    else if (IS_AVG_QUADRANT(angle))
        angle += avg_speed /10.0f * frametime;
    else
        angle += min_speed / 10.0f * frametime;

	if (angle>=360.0f)
		angle = 0.0f;

	transform_matrix = translate(position) * scale_matrix * 
					   glm::rotate(float(time*speed_rotate),glm::vec3(0.0,1.0,0.0));
	time_for_ring = time;
	ring_matrix = translate(position) * glm::scale(glm::vec3(78.5,78.5,78.5)) * 
				  glm::rotate(float(0.3),glm::vec3(1.0,0,0.3)) * 
				  glm::rotate(float(time * 0.02),glm::vec3(0.0,1.0,0.0));

    glm::mat4 full_trans = viewMatrix * transform_matrix;
    
    GLuint uni_location = glGetUniformLocation(shader,"fulltrans_matrix");
    glUniformMatrix4fv(uni_location,1,GL_FALSE,&full_trans[0][0]);

    GLuint light_location = glGetUniformLocation(shader, "light_position");
    glUniform3fv(light_location,1,&sun_position[0]);

    GLuint model_location = glGetUniformLocation(shader,"model_matrix");
    glUniformMatrix4fv(model_location,1,GL_FALSE,&transform_matrix[0][0]);

    default_shader = shader;

    ring_saturn_shader.bind();
    ring_saturn.draw(viewMatrix, sun_position, position, time_for_ring, ring_saturn_shader.get_program_id());
    ring_shader.bind();
    ring.update(viewMatrix, ring_matrix, sun_position, ring_shader.get_program_id());
    glUseProgram(shader);    

    GLCheck::checkGLerror(HERE);
}
void Saturn::draw()
{
	glBindVertexArray(vao);
	glBindTexture(GL_TEXTURE_2D,textureID);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

	glEnable(GL_BLEND);
    ring_shader.bind();
	ring.draw();
	glDisable(GL_BLEND);
    //HACK
    glUseProgram(default_shader);
	GLCheck::checkGLerror(HERE);
}
void Saturn::init_rings(G_object &meteor)
{
	Loader loader;
	Mesh ring_mesh = loader.load_object("./models/sat_ring.obj");
	
	ring_saturn.init(meteor,150,4,78.8f);

	ring.init(ring_mesh,"./textures/sat_ring.png");

    ring_saturn_shader.set_shader("./shaders/planet_vertex.glsl","./shaders/planet_fragment.glsl");
    ring_shader.set_shader("./shaders/planet_vertex.glsl","./shaders/planet_fragment.glsl");
	GLCheck::checkGLerror(HERE);
}
