/**
 * @author Martin Prajka
 * @file   saturn.h
 */
#ifndef SATURN_H
#define SATURN_H

#include "planet.h"
#include "ring.h"
#include "../loader/loader.h"
#include "../window/object_properties.h"

class Saturn: public Planet
{
	G_object ring;
	Meteor_Ring ring_saturn;
	double time_for_ring;
	glm::mat4 ring_matrix;

    Shader ring_saturn_shader;
    Shader ring_shader;

    //Hack
    GLuint default_shader;

public:
	Saturn(const float _angle,const float _def_axis, const float _maj_axis,
		   const float _min_axis, const float _max_speed, const float _avg_speed, 
		   const float _min_speed,const float _speed_rotate, const float _scale)
	: Planet(_angle,_def_axis,_maj_axis,_min_axis,_max_speed,_avg_speed,
			 _min_speed,_speed_rotate,_scale) {}
	
    virtual void update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime);
    virtual void draw();

	virtual ~Saturn() = default;
	void init_rings(G_object &meteor);
};

#endif //SATURN_H
