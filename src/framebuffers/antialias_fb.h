/**
 * @author Matrin Prajka
 * @file   antialias_fb.h
 * @brief  declaration Antialiasing class
 */
#ifndef MULTISAMPLE_FB_H
#define MULTISAMPLE_FB_H

#include "framebuffer.h"

/**
 * @brief The Antialiasing class implement supersampling and multisampling antialias method
 */
class Antialiasing: public Framebuffer
{
    GLuint nonms_fbo;
	int samples;
protected:
	virtual void set_textures(int count);
	virtual void set_render_buffer();
public:
    Antialiasing() = default;
    virtual ~Antialiasing();

    virtual void init(int width, int height, int samples, int supersample = 1);

    void apply(GLuint draw_framebuffer, int draw_width, int draw_height);
};

#endif
