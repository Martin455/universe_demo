/**
 * @author Martin Prajka
 * @file   flight_scene.cpp
 */
#include "flight_scene.h"
#include "../window/glcheck_function.h"

void Flight_scene::init(GLFWwindow *win, Animation_camera *cam, Basic_window_function *bwf)
{
	//setting scene
	window = win;
	camera = cam;
	basic_function = bwf;

#ifndef NDEBUG
    animation = true;
#endif

	glClearColor(0.0f, 0.92f, 1.0f, 1.0f);
	
	Loader loader;
	Mesh shuttle_deck_mesh = loader.load_object("./models/shuttle_deck.obj");
	Mesh deshboard_mesh = loader.load_object("./models/dashboard.obj");
	Mesh controlbox_mesh = loader.load_object("./models/controlbox.obj");
	Mesh ring_handle_mesh = loader.load_object("./models/ring_handle.obj");
	Mesh knipl_mesh = loader.load_object("./models/knipl.obj");
	Mesh gear_stick_mesh = loader.load_object("./models/gear_stick.obj");
	Mesh double_gear_mesh = loader.load_object("./models/double_gear_stick.obj");
    Mesh skybox_sphere_mesh = loader.load_object("./models/skybox_sphere.obj");

    obj_glow_shader.set_shader("./shaders/shuttle_vertex.glsl","./shaders/shuttle_glow_fragment.glsl");
	obj_shader.set_shader("./shaders/shuttle_vertex.glsl", "./shaders/shuttle_fragment.glsl");
    skybox_shader.set_shader("./shaders/flight_skybox_vert.glsl","./shaders/flight_skybox_frag.glsl");
    window_shader.set_shader("./shaders/window_rocket_vert.glsl", "./shaders/window_rocket_frag.glsl");

    objects[0].init(shuttle_deck_mesh, "./textures/shuttle_deck_texture.png");
    glm::mat4 transform = glm::scale(glm::vec3(0.5,0.5,0.5))
            * glm::rotate(deg_to_rad(180),glm::vec3(0,1,0));
    objects[0].set_model_transform(transform);

    objects[1].init(ring_handle_mesh, "./textures/ring_handle_texture.png");
    transform = glm::translate(glm::vec3(-0.075,-0.125,8.5))
                * glm::scale(glm::vec3(0.5,0.5,0.5))
                * glm::rotate(-deg_to_rad(90), glm::vec3(1, 0, 0));
    objects[1].set_model_transform(transform);

    objects[2].init(knipl_mesh, "./textures/knipl_texture.png");
    transform = glm::translate(glm::vec3(0, -9, -49))
                * glm::scale(glm::vec3(0.65,0.9,0.6));
    objects[2].set_model_transform(transform);

    objects[3].init(gear_stick_mesh, "./textures/gear_stick_texture.png");
    transform = glm::translate(glm::vec3(-8.05, -5.5, -42))
                            * glm::scale(glm::vec3(0.175,0.175,0.175));
    objects[3].set_model_transform(transform);

    objects[4].init(gear_stick_mesh, "./textures/gear_stick_texture.png");
    transform = glm::translate(glm::vec3(-5.65, -5.5, -46))
                * glm::scale(glm::vec3(0.175,0.175,0.175));
    objects[4].set_model_transform(transform);

    objects[5].init(double_gear_mesh, "./textures/gear_stick_texture.png");
    transform = glm::translate(glm::vec3(-9.75, -5.5, -46.5))
                * glm::scale(glm::vec3(0.35,0.25,0.25));
    objects[5].set_model_transform(transform);

    objects[6].init(deshboard_mesh, "./textures/dashboard_texture.png");
    transform = glm::translate(glm::vec3(0.53,-6.6,-52.75))
                * glm::scale(glm::vec3(0.32,0.32,0.32));
    objects[6].set_model_transform(transform);
    objects[6].set_glow_texture("./textures/dashboard_light_texture.png");

    objects[7].init(controlbox_mesh, "./textures/controlbox_texture.png");
    transform = glm::translate(glm::vec3(-8, -9.5, -41.25))
                * glm::scale(glm::vec3(0.5,1,0.5));
    objects[7].set_model_transform(transform);
    objects[7].set_glow_texture("./textures/controlbox_light_texture.png");

    skybox.init(skybox_sphere_mesh, "./textures/desert_skybox.png", "./textures/stars_sky_texture.png");

    windows.resize(5);
    for (int i = 0; i < 5; ++i)
    {
        windows[i].init(i);
    }

    clouds.init(64);

    clouds_position.push_back(glm::vec3(492,19,-280));
    clouds_position.push_back(glm::vec3(100,-5,-400));
    clouds_position.push_back(glm::vec3(-320,-25,-370));
    clouds_position.push_back(glm::vec3(0,0,-340));

    //positive values
    collision_block.push_back(glm::vec3(27.0f,27.0f,15.0f));
    //negative values
    collision_block.push_back(glm::vec3(-27.0f,-20.0f,-56.06f));    

    clouds_speed = 0.0f;

	end_scene = false;
	glm::vec3 cam_pos = glm::vec3(0.16f,5.01f,-44.66f);
	camera->set_position(cam_pos);
	camera->set_direction(glm::vec3(0.0f, 0.0f, -1.0f));

    ambient_color = glm::vec3(1,0.75,0.75);
    diffuze_color = glm::vec3(0.75,0,0);    
    switch_colors = false;

    dark_sky = false;
    dark_time = 0;
    dark_mix = 0;

    stars_mix = 0;

    shake_effect = false;
    frequency = 0.0f;
    
    obj_shader.bind();
    set_scene_colors(obj_shader.get_program_id());
    obj_shader.unbind();
    
    obj_glow_shader.bind();
    set_scene_colors(obj_glow_shader.get_program_id());
    obj_glow_shader.unbind();
    
    //Multisample prepare
    int height, width;
    glfwGetWindowSize(window,&width,&height);   
    camera->set_far_plane(2000.0f);
    camera->setProjectionMat(width, height);

    multisampling.init(width, height, basic_function->get_msaa(), basic_function->get_ssaa());

    blur_framebuffer.init(width, height);
    blur_shader.set_shader("./shaders/framebuffer_default_vertex.glsl", "./shaders/rocket_blur_fragment.glsl");

    cut_framebuffer.init(width, height);
    cut_framebuffer.set_shaders("./shaders/framebuffer_default_vertex.glsl", "./shaders/cut_frag.glsl");

    basic_function->init_frame_time();

	GLCheck::checkGLerror(HERE);
    camera->set_transition(0.0f);
    //load sounds
    Audio_manager::instance()->create_sound("prestart", "./sounds/engine_loop.ogg");
    Audio_manager::instance()->create_sound("start", "./sounds/jet_engine.ogg");
    Audio_manager::instance()->create_sound("engine", "./sounds/engine_loop2.ogg");
}

void Flight_scene::draw()
{
	if (!window)
		return;

    //init animation
    set_animation();
    Audio_manager::instance()->play_sound("prestart", true);
	start_scene_time = glfwGetTime();
	do
	{
		scene_time = glfwGetTime() - start_scene_time;
		key_event();

#ifndef NDEBUG        
        if (animation)
        {
            camera->animate_camera(scene_time);
        }
        else
        {
            mouse_event();
        }
		basic_function->fps_counter(scene_time);
#else
        camera->animate_camera(scene_time);
#endif
        painting();
        basic_function->frame_time_counter();
    	
        bool engine_sound = !Audio_manager::instance()->playing_sound("prestart") &&
                !Audio_manager::instance()->playing_sound("start") &&
                !Audio_manager::instance()->playing_sound("engine");

        if (engine_sound) //jestlize nehraje zadny zvuk tak je pravdepodobne raketa ve vzduchu a pust engine loop
        {
            Audio_manager::instance()->play_sound("engine", true);
        }

        glfwSwapBuffers(window);
    	glfwPollEvents();
	}
	while(glfwWindowShouldClose(window)==0 && !end_scene);
    camera->clear_animation();
    if (Audio_manager::instance()->playing_sound("start"))
    {
        Audio_manager::instance()->stop_sound("start");
    }
    Audio_manager::instance()->delete_sound("start");
    if (Audio_manager::instance()->playing_sound("prestart"))
    {
        Audio_manager::instance()->stop_sound("prestart");
        Audio_manager::instance()->delete_sound("prestart");
    }
    if (Audio_manager::instance()->playing_sound("engine"))
    {
        Audio_manager::instance()->stop_sound("engine");
        Audio_manager::instance()->delete_sound("engine");
    }
}

void Flight_scene::painting()
{
    Camera new_cam = *camera;

    if (shake_effect)
    {
        glm::vec3 new_position = new_cam.get_position();
        
        float amplitude = 0.01f + rand()/float(RAND_MAX) * 2.01f;
        new_position.x += (0.1f + rand()/float(RAND_MAX))/frequency * amplitude;
        new_position.y += (0.1f + rand()/float(RAND_MAX))/frequency * amplitude;
        new_position.z += (0.1f + rand()/float(RAND_MAX))/frequency * amplitude;

        new_cam.set_position(new_position);
    }

    //prepare to drawing
	glm::mat4 projection = new_cam.getProjectionMat();
    glm::mat4 view = new_cam.getViewMatrix();
    glm::vec3 camera_position = new_cam.get_position();

    multisampling.bind_fbo();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 skybox_model = projection * view * glm::translate(camera_position)
                             * glm::rotate(-deg_to_rad(90), glm::vec3(1,0,0));
    
    //drawing
    skybox_shader.bind();
    GLuint shader = skybox_shader.get_program_id();
    skybox.draw(skybox_model, shader);
    skybox_shader.unbind();

    obj_shader.bind();
    shader = obj_shader.get_program_id();
    for (int i = 0; i < 6; ++i)
    {
        if (i==0)
        {
            glDisable(GL_CULL_FACE);
            objects[i].update(shader, projection, view);
            objects[i].draw();
            glEnable(GL_CULL_FACE);
        }
        else
        {
            objects[i].update(shader, projection, view);
            objects[i].draw();
        }
    }
    obj_shader.unbind();
    obj_glow_shader.bind();
    shader = obj_glow_shader.get_program_id();
    for (int i = 6; i < 8; ++i)
    {
        objects[i].update(shader, projection, view);
        objects[i].draw();
    }
    obj_glow_shader.unbind();

    //update clouds
    for (auto x = clouds_position.begin(); x != clouds_position.end(); ++x)
        x->z += clouds_speed * basic_function->get_frametime();
    sort_clouds(clouds_position, camera_position);

    //draw clouds

    for (auto x = clouds_position.begin(); x != clouds_position.end(); ++x)
        clouds.draw(projection, view, *x, collision_block, scene_time, basic_function->get_frametime());

    window_shader.bind();
    shader = window_shader.get_program_id();
    for (auto x = windows.begin(); x != windows.end(); ++x)
    {
        x->update(projection, view, shader);
        x->draw();
    }

    window_shader.bind();

    multisampling.apply(blur_framebuffer.get_fbo(), blur_framebuffer.get_width(), blur_framebuffer.get_height());

    //postprocessing
    //supersample + multisample

    if (shake_effect)
    {
        blur_framebuffer.unbind(cut_framebuffer.get_framebuffer(), basic_function->width, basic_function->height);
        blur_shader.bind();
        blur_framebuffer.binding_draw(blur_shader);
        blur_framebuffer.draw();
        blur_shader.unbind();
        blur_framebuffer.swap_textures();
    }
    else
    {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, blur_framebuffer.get_fbo());
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, cut_framebuffer.get_framebuffer());
        glBlitFramebuffer(0, 0, blur_framebuffer.get_width(), blur_framebuffer.get_height(),
                          0, 0, basic_function->width, basic_function->height,
                          GL_COLOR_BUFFER_BIT, GL_LINEAR);

        cut_framebuffer.bind_fbo();
    }

    cut_framebuffer.unbind_fbo(0, basic_function->width, basic_function->height);
    cut_framebuffer.binding_draw();
    GLuint location = glGetUniformLocation(cut_framebuffer.get_shader_id(), "value");
    glUniform1f(location, camera->get_transition());
    cut_framebuffer.draw_quad();

    //allow start rocket
    if (!switch_colors && scene_time > 3.0)
    {
        switch_colors = true;
        ambient_color = glm::vec3(0.75,1,0.75);
        diffuze_color = glm::vec3(0,0.75,0);
        
        obj_shader.bind();
        set_scene_colors(obj_shader.get_program_id());
        obj_shader.unbind();
        obj_glow_shader.bind();
        set_scene_colors(obj_glow_shader.get_program_id());
        obj_glow_shader.unbind();

        shake_effect = true;
        frequency = 10.0f;
        if (Audio_manager::instance()->playing_sound("prestart"))
        {
            Audio_manager::instance()->stop_sound("prestart");
            Audio_manager::instance()->delete_sound("prestrat");
        }
        Audio_manager::instance()->play_sound("start");
    }
    if (shake_effect && scene_time > 5.0 && clouds_speed < 0.5f)
    {
        dark_sky = true;
        dark_time = scene_time;
        skybox_shader.bind();
        GLuint location = glGetUniformLocation(skybox_shader.get_program_id(), "dark_sky");
        glUniform1i(location, 1);
        skybox_shader.unbind();
        
        clouds_speed = 26.6f;
        frequency -= 0.15f;        
    }
    if (dark_sky)
    {
        frequency = 3.5f;

        skybox_shader.bind();
        
        float time = scene_time - dark_time;
        if (time > 20.0f)
        {

            stars = true;
            stars_mix = scene_time - dark_time - 20.0f;
            if (stars_mix > 13.0f)
            {
                end_scene = true;
            }
            else
            {
                frequency += 0.06f;
            }
            GLuint location = glGetUniformLocation(skybox_shader.get_program_id(), "star_mix");
            glUniform1f(location, stars_mix);
            location = glGetUniformLocation(skybox_shader.get_program_id(), "mix_second_texture");
            glUniform1i(location, 1);

        }
        else
        {
            dark_mix = time;
            GLuint location = glGetUniformLocation(skybox_shader.get_program_id(), "dark_mix");
            glUniform1f(location, dark_mix);
        }
        
        skybox_shader.unbind();
    }
}

void Flight_scene::key_event()
{
#ifdef NDEBUG
	if(glfwGetKey(window,GLFW_KEY_F) == GLFW_PRESS)
	{
		basic_function->fps_counter(scene_time);    //print fps to stdout
	}
#endif
}

void Flight_scene::mouse_event()
{
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    camera->mouseMove(glm::vec2(xpos, ypos));
}

void Flight_scene::end()
{
    end_scene = true;
}

void Flight_scene::set_scene_colors(GLuint programID)
{
    GLuint location = glGetUniformLocation(programID, "ambientColor");
    glUniform3fv(location,1, &ambient_color[0]);

    location = glGetUniformLocation(programID, "diffuzeColor");
    glUniform3fv(location,1, &diffuze_color[0]);
}

void Flight_scene::set_animation()
{
	//before start
	camera->take_look_around(50.0f, glm::vec2(-1.0f, 0.8f), 0.25, 1.55);
	camera->take_look_around(45.0f, glm::vec2(0.6f, -1.0f), 1.7, 3.0);

	//start
	camera->take_look_around(80.0f, glm::vec2(-0.1f, -1.0f), 3.2, 4.8);
	camera->take_look_around(160.0f, glm::vec2(0.1f, 1.0f), 4.92, 7.52);
	camera->take_look_around(0.2f, glm::vec2(0.1f, 1.0f), 7.52, 7.62);
	camera->take_look_around(80.0f, glm::vec2(0.0f, 1.0f), 7.62, 8.22);
	camera->take_look_around(175.0f, glm::vec2(0.2f, -1.0f), 8.7, 9.7);
	camera->take_look_around(40.0f, glm::vec2(0.0f, 1.0f), 10.1, 10.9);
	camera->take_look_around(-20.0f, glm::vec2(0.0f, 1.0f), 11.1, 11.5);

	//clouds
	camera->take_look_around(45.0f, glm::vec2(0.0f, 1.0f), 13.5, 14.1);
	camera->take_look_around(-5.0f, glm::vec2(0.0f, 1.0f), 14.1, 14.9);
	camera->take_look_around(-95.0f, glm::vec2(0.0f, 1.0f), 14.9, 16.5);
	camera->take_look_around(-45.0f, glm::vec2(0.0f, 1.0f), 16.5, 20.5);

	camera->take_look_around(100.0f, glm::vec2(0.0f, 1.0f), 21.5, 25.5);

	//cuts
	camera->set_cut(0.0, 3.5, false);
	camera->set_cut(34.0, 36.0);
}
//Bubble sort (array should be small)
void Flight_scene::sort_clouds(std::vector<glm::vec3> &clouds_vec, glm::vec3 &cam_pos)
{
    for (unsigned i = 0; i < clouds_vec.size() - 1; ++i)
    {
        for (unsigned j = 0; j < clouds_vec.size() - i - 1; ++j)
        {
            double length_j = glm::length(clouds_vec[j] - cam_pos);
            double length_j1 = glm::length(clouds_vec[j+1] - cam_pos);
            if (length_j < length_j1)
            {
                glm::vec3 tmp = clouds_vec[j];
                clouds_vec[j] = clouds_vec[j+1];
                clouds_vec[j+1] = tmp;
            }
        }
    }
}

void Flight_scene::Window_obj::init(int number_of_win)
{
    GLfloat data[] =
    {
        -1,1,
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        1,-1        
    };

    switch (number_of_win)
    {
        case 0:
            model_matrix = glm::translate(glm::vec3(-15.75,4.5,-41))
                            * glm::rotate(deg_to_rad(85), glm::vec3(0,1,0))
                            * glm::rotate(deg_to_rad(11), glm::vec3(1,0,0))
                            * glm::scale(glm::vec3(4.8,5,0.5));
            shear = 0.0f;
            break;
        case 1:
            model_matrix = glm::translate(glm::vec3(-10.65,4.5,-51.35))
                           * glm::rotate(deg_to_rad(47), glm::vec3(0,1,0))
                           * glm::rotate(deg_to_rad(8), glm::vec3(1,0,0))
                           * glm::scale(glm::vec3(7,5.25,0.5));
            shear = 0.1f;
            break;
        case 2:
            model_matrix = glm::translate(glm::vec3(0,4.05,-56.05))                         
                           * glm::scale(glm::vec3(6.5,5.5,0.5));
            shear = 0.0f;       
            break;
        case 3:
            model_matrix = glm::translate(glm::vec3(11.5,4.5,-50.75))
                           * glm::rotate(-deg_to_rad(47), glm::vec3(0,1,0))
                           * glm::rotate(deg_to_rad(8), glm::vec3(1,0,0))
                           * glm::scale(glm::vec3(7,5.25,0.5));
            shear = -0.1f;
            break;
        case 4:
            model_matrix = glm::translate(glm::vec3(15.75,5,-41))
                           * glm::rotate(-deg_to_rad(85), glm::vec3(0,1,0))
                           * glm::rotate(deg_to_rad(11), glm::vec3(1,0,0))
                           * glm::scale(glm::vec3(4.8,5.1,0.5));
            shear = 0.0f;
            break;
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2,0);
}

void Flight_scene::Window_obj::update(glm::mat4 &projection, glm::mat4 &viewMatrix, GLuint shader)
{
    glm::mat4 view = projection * viewMatrix;
    GLuint location = glGetUniformLocation(shader, "viewMatrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &view[0][0]);

    location = glGetUniformLocation(shader, "modelMatrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);

    location = glGetUniformLocation(shader, "shear_const");
    glUniform1f(location, shear);
}

void Flight_scene::Window_obj::draw()
{
    glBindVertexArray(vao);

    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);

    GLCheck::checkGLerror(HERE);
}

Flight_scene::Window_obj::~Window_obj()
{
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}
