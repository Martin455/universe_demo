/**
 * @author Martin Prajka
 * @file   rocket_bridge.h
 */
#ifndef ROCKET_BRIDGE_H
#define ROCKET_BRIDGE_H

#include "g_object.h"
#include "../constants.h"

class Rocket_bridge: public G_object
{
    glm::mat4 model_matrix;
public:
	Rocket_bridge() {}
	
    void init(Mesh &mesh, const char *texture_name);
    void prepare_depth(GLuint depth_shader);
    void update(Shader &shader, GLuint shadowmap);
	void draw();
};
#endif
