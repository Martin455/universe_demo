#version 330 core

layout(location=0) in vec2 position;
layout(location=1) in vec2 tex_coords;

out vec2 uv_coords;

void main()
{
	gl_Position = vec4(position, 0.0, 1.0);
	uv_coords = tex_coords;
}