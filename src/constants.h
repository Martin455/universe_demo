/**
 * @author Martin Prajka
 * @file   constants.h
 */
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <glm/glm.hpp>
/**
 * Konstanty pro Universe Demo 
 * Tvori je nalezene tabulkove hodnot ze zdroju wikipedie
 * Nebo Prepocitane hodnoty z keplerovych zakonu
 * */
// pocatecni uhly nebo-li pocatecni pozice planet
const float MERCURY_DEFAULT_ANGLE = 183.0f;
const float VENUS_DEFAULT_ANGLE = 110.0f;
const float EARTH_DEFAULT_ANGLE = 0.0f;
const float MARS_DEFAULT_ANGLE = 310.0f;
const float JUPITER_DEFAULT_ANGLE = 180.0f;
const float SATURN_DEFAULT_ANGLE = 90.0f;
const float URANUS_DEFAULT_ANGLE = 40.0f;
const float NEPTUNE_DEFAULT_ANGLE = 190.0f;

/*Rychlosti v [km/s] */
const float MERCURY_MAX_SPEED = 58.98f;
const float MERCURY_MIN_SPEED = 38.86f;
const float MERCURY_AVG_SPEED = 47.36f;

const float VENUS_MAX_SPEED = 35.26f;
const float VENUS_MIN_SPEED = 34.78f;
const float VENUS_AVG_SPEED = 35.02f;

const float EARTH_MAX_SPEED = 30.29f;
const float EARTH_MIN_SPEED = 29.29f;
const float EARTH_AVG_SPEED = 29.78f;

const float MARS_MAX_SPEED = 26.5f;
const float MARS_MIN_SPEED = 21.97f;
const float MARS_AVG_SPEED = 24.08f;

const float JUPITER_MAX_SPEED = 13.71f;
const float JUPITER_MIN_SPEED = 12.44f;
const float JUPITER_AVG_SPEED = 13.05f;

const float SATURN_MAX_SPEED = 10.18f;
const float SATURN_MIN_SPEED = 9.14f;
const float SATURN_AVG_SPEED = 9.64f;

const float URANUS_MAX_SPEED = 7.13f;
const float URANUS_MIN_SPEED = 6.49f;
const float URANUS_AVG_SPEED = 6.8f;

const float NEPTUNE_MAX_SPEED = 5.48f;
const float NEPTUNE_MIN_SPEED = 5.39f;
const float NEPTUNE_AVG_SPEED = 5.43f;

/*vypocitane vychyleni stredu od pozice slunce*/
const float MERCURY_DEFLECT_AXIS = 114.0f;
const float VENUS_DEFLECT_AXIS = 6.0f;
const float EARTH_DEFLECT_AXIS = 18.0f;
const float MARS_DEFLECT_AXIS = 126.0f;
const float JUPITER_DEFLECT_AXIS = 30.0f;
const float SATURN_DEFLECT_AXIS = 126.0f;
const float URANUS_DEFLECT_AXIS = 114.0f;
const float NEPTUNE_DEFLECT_AXIS = 24.0f;

/* poloosy planet v a,b zaroven urcuji vzdalenost planety od slunce*/
const float MERCURY_MAJOR_AXIS = 940.0f;
const float MERCURY_MINOR_AXIS = 916.0f;

const float VENUS_MAJOR_AXIS = 1080.0f;
const float VENUS_MINOR_AXIS = 1074.0f;

const float EARTH_MAJOR_AXIS = 1200.0f;
const float EARTH_MINOR_AXIS = 1194.0f;

const float MARS_MAJOR_AXIS = 1326.0f;
const float MARS_MINOR_AXIS = 1314.0f;

const float JUPITER_MAJOR_AXIS = 2320.0f;
const float JUPITER_MINOR_AXIS = 2314.0f;

const float SATURN_MAJOR_AXIS = 2656.0f;
const float SATURN_MINOR_AXIS = 2650.0f;

const float URANUS_MAJOR_AXIS = 2866.0f;
const float URANUS_MINOR_AXIS = 2860.0f;

const float NEPTUNE_MAJOR_AXIS = 3100.0f;
const float NEPTUNE_MINOR_AXIS = 3094.0f;
/*Rychlost rotace planety 
* Nasobi cas aplikace
**/
const float MERCURY_ROTATE = 0.0001f;
const float VENUS_ROTATE = 0.00001f;
const float EARTH_ROTATE = 0.005f;
const float MARS_ROTATE = 0.005f;
const float JUPITER_ROTATE = 0.007f;
const float SATURN_ROTATE = 0.007f;
const float URANUS_ROTATE = 0.0085f;
const float NEPTUNE_ROTATE = 0.0099f;

/*Prumer planet  */
const float MERCURY_ACROSS = 1.52f;
const float VENUS_ACROSS = 3.96f;
const float EARTH_ACROSS = 4.0f;
const float MARS_ACROSS = 2.12f;
const float JUPITER_ACROSS = 32.0f;
const float SATURN_ACROSS = 20.0f;
const float URANUS_ACROSS = 10.8f;
const float NEPTUNE_ACROSS = 10.8f;

const float SUN_ACROSS = 182.0f;

/* Pozice slunce zaroven je rovna pozici svetla */
const glm::vec3 SUN_POSITION(-200.0f,0.0f,-200.0f);

/* kvadranty drahy pro planety 2 kepleruv zakon (zjednoduseny) 
           avg speed
         SECOND  90|     75°
             , - ~ ~ ~ - ,
  130°   , '       |   /   ' ,  FIRST  SLOWLY
FAST   ,  .        |  .        ,
THIRD ,      .     | /          ,  Min Speed
 180 ,          .  |.            ,
-----,------------.|-------------,-----
max  ,         .   |.            ,  0 - 360
speed ,     .      | \          ,
       , .         |  .        ,
   230°  ,         |   \    , '
           ' - , _ _ _ ,  '
       SECOND   180|     285°
          avg speed
***/                
inline bool IS_SLOW_QUADRANT(const float angle)
{
    //nemuze byt uhel mensi nez 0 nebo vetsi nez 360
    if ((angle < 75.0f || angle > 285.0f))
        return true;
    
    return false;
}

inline bool IS_AVG_QUADRANT(const float angle)
{
    if ((angle >= 75.0f && angle < 130.0f) || (angle > 230.0f && angle <= 285.0f))
        return true;
    
    return false;
}

inline bool IS_FAST_QUADRANT(const float angle)
{
    if (angle >= 130.0f && angle <= 230.0f)
        return true;
    return false;
}
/* Kvadranty konec */

/* prevod stupne na radiany */
inline float deg_to_rad(const float angle)
{
    return float((angle/180.0f)*M_PI);
}
/* prevod radiany na stupne */
inline float rad_to_deg(const float angle)
{
    return float((angle*180.0f)/M_PI);
}

#endif       // CONSTANTS_H
