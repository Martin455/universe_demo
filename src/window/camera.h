/**
 * @author Martin Prajka
 * @file camera.h
 * @brief file with Camera class declaration
 */
#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../constants.h"

/**
 * @brief The Camera class
 */
class Camera 
{
protected:	
	glm::vec3 position;
	glm::vec3 viewDirection;
	const glm::vec3 UP;
	glm::vec2 old_mouse_position;

    //projection matrix
	glm::mat4 projectMat;

    //speed camera moving
	float cameraSpeed;
	float mouseSensitive;
    float far_plane;

public:
	Camera();

	//moving methods
	void move_forward(float frametime);
	void move_backward(float frametime);
	void move_left(float frametime);
	void move_right(float frametime);

    //methods for changing camera speed
	void active_sprint();
	void deactive_sprint();
	void super_sprint();

	void mouseMove(const glm::vec2 &new_position);
	void setProjectionMat(int width, int height);

	void set_position(glm::vec3 new_position);
	void set_direction(glm::vec3 new_direction);
    void set_far_plane(float value);

	glm::mat4 getProjectionMat() const;
	glm::mat4 getViewMatrix() const;
	glm::vec3 get_position() const;
    glm::vec3 get_direction() const;
    float get_far_plane() const;
};



#endif
