/**
 * @author Martin Prajka
 * @file   rocket_tank.h
 */
#ifndef ROCKET_TANK_H
#define ROCKET_TANK_H

#include "g_object.h"
#include "../constants.h"

class Rocket_tank: public G_object
{
    glm::mat4 model_matrix;
public:
    Rocket_tank() = default;

    void init(Mesh &mesh, const char *texture_name);
    void prepare_depth(GLuint depth_shader);
    void update(Shader &shader, GLuint shadowmap);
	void draw();
};

#endif
