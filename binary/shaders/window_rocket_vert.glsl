#version 330 core

layout(location=0) in vec2 position;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

uniform float shear_const;

void main()
{
    //X axis shearing
    float x = position.x + shear_const * position.y;

    gl_Position = viewMatrix * modelMatrix * vec4 (x, position.y, 0.0, 1.0);
}