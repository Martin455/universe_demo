#version 330 core

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv;
layout(location=2) in vec3 normals;

out vec2 uv_coords;


uniform mat4 fulltrans_matrix;

void main()
{
    uv_coords = uv;
    gl_Position = fulltrans_matrix * vec4(position,1.0);
}
