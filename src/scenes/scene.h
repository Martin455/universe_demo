/**
 * @file scene.h
 * @brief Base class for scenes file
 * @author Martin Prajka
 */
#ifndef SCENE_H
#define SCENE_H

//abstract class for every scene

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include "../window/basic_function.h"
#include "../window/animation_camera.h"
#include "../loader/loader.h"
#include "../audio/audio_manager.h"

/**
 * @brief The Scene class is base class for each scene
 * @details The class contains only abstract methods
 */
class Scene
{
protected:
    Animation_camera *camera;
	GLFWwindow *window;
	Basic_window_function *basic_function;

	double start_scene_time = 0;
	double scene_time = 0;

    /**
     * @brief for painting graphics entities
     */
	virtual void painting() = 0;
    /**
     * @brief key_event, catch keys
     */
	virtual void key_event() = 0;
    /**
     * @brief mouse_event, catch mouse move
     */
    virtual void mouse_event() = 0;
public:
	Scene() = default;
	virtual ~Scene() {}

    /**
     * @brief initialize scene
     * @details load objects set framebuffers etc.
     * @param win pointer to window
     * @param cam pointer to camera object
     * @param bwf (basic window function) contain basic functions with window
     */
    virtual void init(GLFWwindow *win,Animation_camera *cam, Basic_window_function *bwf) = 0;
    /**
     * @brief draw, execute infinity loop
     */
    virtual void draw() = 0;
    /**
     * @brief close scene
     */
    virtual void end() = 0;

#ifndef NDEBUG
    bool animation;
#endif
};

#endif
