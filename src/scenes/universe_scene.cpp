/**
 * @author Martin Prajka
 * @file   universe_scene.h
 */
#include "universe_scene.h"

void Universe_scene::init(GLFWwindow *win, Animation_camera *cam, Basic_window_function *bwf)
{
	//setting scene
	window = win;
	camera = cam;
	basic_function = bwf;

#ifndef NDEBUG
    animation = true;
#endif

	TIME = 0.0;	
    end_time = std::numeric_limits<double>::max();
    end_scene = false;
	glClearColor(0.0f,0.0f,0.0f,1.0f);

    //get sun vector from constants
	sun_light = SUN_POSITION;
    //Loading objects
	Loader loader;
	Mesh meteor_mesh = loader.load_object("./models/meteor_1.obj");
	Mesh planet_mesh = loader.load_object("./models/planet_model.obj");
	Mesh skybox_sphere_mesh = loader.load_object("./models/skybox_sphere.obj");
	
    //shaders init
    planet_shader.set_shader("./shaders/planet_vertex.glsl","./shaders/planet_fragment.glsl");
    sun_shader.set_shader("./shaders/sun_vertex.glsl","./shaders/sun_fragment.glsl");
    skybox_shader.set_shader("./shaders/skybox_vertex.glsl","./shaders/skybox_fragment.glsl");

	meteor.init(meteor_mesh,"./textures/meteor_textures.png");
    //creating planets and sun
	sun.init(planet_mesh);
	//merkur
	planets.push_back(new Planet(MERCURY_DEFAULT_ANGLE, MERCURY_DEFLECT_AXIS, MERCURY_MAJOR_AXIS, 
								 MERCURY_MINOR_AXIS, MERCURY_MAX_SPEED, MERCURY_AVG_SPEED,
								 MERCURY_MIN_SPEED, MERCURY_ROTATE, MERCURY_ACROSS));
	planets.back()->init(planet_mesh,"./textures/merkur_texture.png");
	//venuse
	planets.push_back(new Planet(VENUS_DEFAULT_ANGLE, VENUS_DEFLECT_AXIS, VENUS_MAJOR_AXIS, 
								 VENUS_MINOR_AXIS, VENUS_MAX_SPEED, VENUS_AVG_SPEED,
								 VENUS_MIN_SPEED, VENUS_ROTATE, VENUS_ACROSS));
	planets.back()->init(planet_mesh,"./textures/venus_texture.png");
	//zeme
	Earth *tmp_earth = new Earth(EARTH_DEFAULT_ANGLE, EARTH_DEFLECT_AXIS, EARTH_MAJOR_AXIS, 
								 EARTH_MINOR_AXIS, EARTH_MAX_SPEED, EARTH_AVG_SPEED,
								 EARTH_MIN_SPEED, EARTH_ROTATE, EARTH_ACROSS); 
	tmp_earth->set_clouds();
	planets.push_back(tmp_earth);
	planets.back()->init(planet_mesh,"./textures/earth_texture.png");
	//mars
	planets.push_back(new Planet(MARS_DEFAULT_ANGLE, MARS_DEFLECT_AXIS, MARS_MAJOR_AXIS, 
								 MARS_MINOR_AXIS, MARS_MAX_SPEED, MARS_AVG_SPEED,
								 MARS_MIN_SPEED, MARS_ROTATE, MARS_ACROSS));
	planets.back()->init(planet_mesh,"./textures/mars_texture.png");
	//jupiter
	planets.push_back(new Planet(JUPITER_DEFAULT_ANGLE, JUPITER_DEFLECT_AXIS, JUPITER_MAJOR_AXIS, 
								 JUPITER_MINOR_AXIS, JUPITER_MAX_SPEED, JUPITER_AVG_SPEED,
								 JUPITER_MIN_SPEED, JUPITER_ROTATE, JUPITER_ACROSS));
	planets.back()->init(planet_mesh,"./textures/jupiter_texture.png");
	//saturn
	Saturn *tmp_saturn = new Saturn(SATURN_DEFAULT_ANGLE, SATURN_DEFLECT_AXIS, SATURN_MAJOR_AXIS, 
								 SATURN_MINOR_AXIS, SATURN_MAX_SPEED, SATURN_AVG_SPEED,
								 SATURN_MIN_SPEED, SATURN_ROTATE, SATURN_ACROSS);
	tmp_saturn->init_rings(meteor);
	planets.push_back(tmp_saturn);
	planets.back()->init(planet_mesh,"./textures/saturn_texture.png");
	//uran
	Uranus *tmp_uranus = new Uranus(URANUS_DEFAULT_ANGLE, URANUS_DEFLECT_AXIS, URANUS_MAJOR_AXIS, 
								 URANUS_MINOR_AXIS, URANUS_MAX_SPEED, URANUS_AVG_SPEED,
								 URANUS_MIN_SPEED, URANUS_ROTATE, URANUS_ACROSS);
	tmp_uranus->init_ring(meteor);
	planets.push_back(tmp_uranus);
	planets.back()->init(planet_mesh,"./textures/uranus_texture.png");
	//neptun
	planets.push_back(new Planet(NEPTUNE_DEFAULT_ANGLE, NEPTUNE_DEFLECT_AXIS, NEPTUNE_MAJOR_AXIS, 
								 NEPTUNE_MINOR_AXIS, NEPTUNE_MAX_SPEED, NEPTUNE_AVG_SPEED,
								 NEPTUNE_MIN_SPEED, NEPTUNE_ROTATE, NEPTUNE_ACROSS));
	planets.back()->init(planet_mesh,"./textures/neptune_texture.png");
	
    skybox.init(skybox_sphere_mesh, "./textures/skybox.png");

    //setting meteors
	meteors.init(meteor,500,15,1522.5f,0.3f,true);

	explosion.init(meteor_mesh);

    //init camera position for scene
    camera->set_position(glm::vec3(1007,0,-183));
	camera->set_direction(glm::vec3(0.0f, 0.0f, -1.0f));

	int height, width;
	glfwGetWindowSize(window,&width,&height);	
    camera->set_far_plane(7000.0f);
    camera->setProjectionMat(width, height);

    multisampling.init(width, height, basic_function->get_msaa(), basic_function->get_ssaa());

    bloom.init(width, height, 10);
	bloom.set_shaders("./shaders/framebuffer_default_vertex.glsl", "./shaders/framebuffer_default_fragment.glsl");
	bloom.set_blur_shader("./shaders/framebuffer_default_vertex.glsl", "./shaders/blur_fragment.glsl");
    bloom.set_prepare_shader("./shaders/framebuffer_default_vertex.glsl","./shaders/prepare_bloom_fragment.glsl", glm::vec3(0.4976, 0.5002, 0.0489) * 1.06f);
	bloom.set_bloom_shader("./shaders/framebuffer_default_vertex.glsl", "./shaders/bloom_fragment.glsl");

    camera->set_transition(0.0f);
    cut_framebuffer.init(width, height);
    cut_framebuffer.set_shaders("./shaders/framebuffer_default_vertex.glsl", "./shaders/cut_frag.glsl");

    basic_function->init_frame_time();   

    GLCheck::checkGLerror(HERE);

    Audio_manager::instance()->create_sound("universe", "./sounds/universe.ogg");    
}

void Universe_scene::draw()
{
	if (!window)
		return;

    set_animation();
	Audio_manager::instance()->play_sound("universe", true);
    start_scene_time = glfwGetTime();
	do
	{
		scene_time = glfwGetTime() - start_scene_time;

		key_event();

#ifndef NDEBUG        
        if (animation)
        {
            camera->animate_camera(scene_time);
        }
        else
        {
            mouse_event();
        }
		basic_function->fps_counter(scene_time);
#else
        camera->animate_camera(scene_time);
#endif
        painting();
        //for getting one frame time
        basic_function->frame_time_counter();

		tick_timer();
    	glfwSwapBuffers(window);
    	glfwPollEvents();
	}
	while(glfwWindowShouldClose(window)==0 && !end_scene);
	clean_vec_of_planets();
    camera->clear_animation();
	if (Audio_manager::instance()->playing_sound("universe"))
	{
		Audio_manager::instance()->stop_sound("universe");
		Audio_manager::instance()->delete_sound("universe");
	}
}

void Universe_scene::painting()
{
	glm::mat4 viewMatrix = camera->getProjectionMat() * camera->getViewMatrix(); 
	
    Audio_manager::instance()->set_listener(camera->get_position(), camera->get_direction());

    multisampling.bind_fbo();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 skyboxMat = viewMatrix * glm::translate(camera->get_position());

    skybox_shader.bind();
    GLuint shader = skybox_shader.get_program_id();
	skybox.draw(skyboxMat, shader);
    skybox_shader.unbind();

    //drawing planets
    planet_shader.bind();
    shader = planet_shader.get_program_id();
	for(unsigned int i = 0; i < planets.size(); ++i)
	{
        planets.at(i)->update(TIME,sun_light,viewMatrix, shader, basic_function->get_frametime());
		planets.at(i)->draw();		
	}

    //static rock for explosion
    glm::mat4 transMatrix = glm::translate(glm::vec3(995,0,-180));
    meteor.update(viewMatrix,transMatrix,sun_light, shader);
    meteor.draw();

    //moving meteors
    meteors.draw(viewMatrix,sun_light,sun_light,TIME, shader);

    planet_shader.unbind();
	
    sun_shader.bind();
    shader = sun_shader.get_program_id();
	sun.draw(camera->getProjectionMat(),camera->getViewMatrix(),TIME,sun_light, basic_function->get_frametime(), shader);
    sun_shader.unbind();

    //exposion
#ifndef NDEBUG
    if (animation)
    {
#endif
    if (TIME >= 6050.0)
	{

		glm::vec3 explosion_position = glm::vec3(995.0,0.0,-180.0);
        explosion.draw(camera->getProjectionMat(),camera->getViewMatrix(), explosion_position, basic_function->get_frametime(), true, TIME);
	}
#ifndef NDEBUG
    }
    else
    {
        if (TIME >= 500.0)
        {

            glm::vec3 explosion_position = glm::vec3(995.0,0.0,-180.0);
            explosion.draw(camera->getProjectionMat(),camera->getViewMatrix(), explosion_position, basic_function->get_frametime(), true, TIME);
        }
    }
#endif

    //Postprocessing
    multisampling.apply(bloom.get_framebuffer(), bloom.get_width(), bloom.get_height());

    bloom.unbind_fbo(0, basic_function->width, basic_function->height);

    bloom.active_bloom();
    bloom.unbind_fbo(cut_framebuffer.get_framebuffer(), basic_function->width, basic_function->height);

    bloom.draw_bloom();

    cut_framebuffer.unbind_fbo(0, basic_function->width, basic_function->height);
    cut_framebuffer.binding_draw();
    GLuint location = glGetUniformLocation(cut_framebuffer.get_shader_id(), "value");
    glUniform1f(location, camera->get_transition());
    cut_framebuffer.draw_quad();

    //ending scene 4s after dark screen
    if (glm::length(camera->get_position() - glm::vec3(993.0f, 0.0f,-180.8f)) < 0.001f
       && camera->get_transition() < 0.01
       && !(end_time < std::numeric_limits<double>::max()))
    {
        end_time = scene_time + 4.0;
    }
    if (end_time < scene_time)
    {
        end_scene = true;
    }
}

void Universe_scene::key_event()
{
#ifdef NDEBUG
	if(glfwGetKey(window,GLFW_KEY_F) == GLFW_PRESS)
 	{
		basic_function->fps_counter(scene_time);   //print fps to stdout
 	}
#else
    if (!animation)
    {
        if (glfwGetKey(window,GLFW_KEY_W) == GLFW_PRESS)
        {
            camera->move_forward(basic_function->get_frametime());
        }
        if (glfwGetKey(window,GLFW_KEY_S) == GLFW_PRESS)
        {
            camera->move_backward(basic_function->get_frametime());
        }
        if (glfwGetKey(window,GLFW_KEY_A) == GLFW_PRESS)
        {
            camera->move_left(basic_function->get_frametime());
        }
        if (glfwGetKey(window,GLFW_KEY_D) == GLFW_PRESS)
        {
            camera->move_right(basic_function->get_frametime());
        }
        if(glfwGetKey(window,GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
        {
            camera->deactive_sprint();
        }
        if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_RELEASE)
        {
            camera->deactive_sprint();
        }
        if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        {
            camera->super_sprint();
        }
        if(glfwGetKey(window,GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        {
            camera->active_sprint();
        }
    }
#endif	
}

void Universe_scene::mouse_event()
{
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    camera->mouseMove(glm::vec2(xpos, ypos));
}
void Universe_scene::end()
{
    end_scene = true;
}
void Universe_scene::clean_vec_of_planets()
{
	for(auto x = planets.begin(); x!=planets.end();++x)
	{
		delete (*x);
    }
}

void Universe_scene::set_animation()
{
    //moves
	glm::vec3 start_position = camera->get_position();
    camera->move_to_point(start_position, glm::vec3(960.0f, 0.0f, -494.0f), 0.0, 3.55);
    camera->move_cubic_bezier(glm::vec3(960.0f, 0.0f, -494.0f),
							  glm::vec3(1001.0f, 0.0f, -563.0f),
                              glm::vec3(951.0f, 0.0f, -565.0f),
                              glm::vec3(969.0f, 0.0f, -569.0f), 3.55, 5.45);
    camera->move_cubic_bezier(glm::vec3(1001.0f, 0.0f, -563.0f),
						      glm::vec3(1228.0f, 0.0f, -250.0f),
                              glm::vec3(1136.0f, 0.0f, -542.0f),
                              glm::vec3(1117.0f, 0.0f, -342.0f), 5.45, 17.45);
    camera->move_cubic_bezier(glm::vec3(1228.0f, 0.0f, -250.0f), 
							  glm::vec3(1269.0f, 0.0f, -169.0f),
                              glm::vec3(1278.0f, 0.0f, -213.0f),
                              glm::vec3(1260.0f, 0.0f, -240.0f), 17.45, 21.45);
    camera->move_cubic_bezier(glm::vec3(1269.0f, 0.0f, -169.0f), 
							  glm::vec3(1319.0f, 0.0f, -90.0f),
                              glm::vec3(1278.0f, 0.0f, -119.0f),
                              glm::vec3(1335.0f, 0.0f, -134.0f), 21.45, 23.95);
    camera->move_to_point(glm::vec3(1319.0f, 0.0f, -90.0f), glm::vec3(1286.0f, 0.0f, 117.0f), 23.95, 27.95);
    camera->move_to_point(glm::vec3(1286.0f, 0.0f, 117.0f), glm::vec3(1259.0f, -2.0f, 248.0f), 27.95, 29.5);
    camera->move_to_point(glm::vec3(1259.0f, -2.0f, 248.0f), glm::vec3(1241.0f, -32.0f, 358.0f), 29.5, 31.5);
    camera->move_cubic_bezier(glm::vec3(1241.0f, -32.0f, 358.0f), 
							  glm::vec3(994.0f, 0.0f, 1499.0f),
                              glm::vec3(1051.0f, 0.0f, 627.0f),
                              glm::vec3(1150.0f, 0.0f, 938.0f), 31.5, 34.5);
    camera->move_cubic_bezier(glm::vec3(994.0f, 0.0f, 1499.0f), 
							  glm::vec3(834.0f, 0.0f, 1848.0f),
                              glm::vec3(971.0f, 0.0f, 1612.0f),
                              glm::vec3(971.0f, 0.0f, 1763.0f), 34.5, 35.3);
    camera->move_cubic_bezier(glm::vec3(834.0f, 0.0f, 1848.0f), 
							  glm::vec3(57.0f, 0.0f, 2069.0f),
                              glm::vec3(537.0f, 0.0f, 2041.0f),
                              glm::vec3(306.0f, 0.0f, 2041.0f), 35.3, 36.5);
    camera->move_cubic_bezier(glm::vec3(57.0f, 0.0f, 2069.0f), 
							  glm::vec3(-670.0f, 0.0f, 2131.0f),
                              glm::vec3(-368.0f, 0.0f, 2140.0f),
                              glm::vec3(-500.0f, 0.0f, 2277.0f), 36.5, 37.5);
   
	camera->move_cubic_bezier(glm::vec3(-670.0f, 0.0f, 2131.0f), 
							  glm::vec3(-1273.0f, 0.0f, 1693.0f),
                              glm::vec3(-803.0f, 0.0f, 2033.0f),
                              glm::vec3(-1047.0f, 0.0f, 1843.0f), 37.5, 42.5);
  
	camera->move_cubic_bezier(glm::vec3(-1273.0f, 0.0f, 1693.0f), 
							  glm::vec3(-2297.0f, 0.0f, 1067.0f),
                              glm::vec3(-1617.0f, 0.0f, 1470.0f),
                              glm::vec3(-2043.0f, 0.0f, 1383.0f), 42.5, 46.5);
   
	camera->move_cubic_bezier(glm::vec3(-2297.0f, 0.0f, 1067.0f), 
							  glm::vec3(-2687.0f, 0.0f, -77.0f),
                              glm::vec3(-2543.0f, 0.0f, 760.0f),
                              glm::vec3(-2617.0f, 0.0f, 317.0f), 46.5, 50.5);
   
	camera->move_cubic_bezier(glm::vec3(-2687.0f, 0.0f, -77.0f), 
							  glm::vec3(-2453.0f, 20.0f, -1290.0f),
                              glm::vec3(-2757.0f, 20.0f, -480.0f),
                              glm::vec3(-2453.0f, 20.0f, -1290.0f), 50.5, 54.5);
   
	camera->move_cubic_bezier(glm::vec3(-2453.0f, 20.0f, -1290.0f), 
							  glm::vec3(-1983.0f, 50.0f, -2117.0f),
                              glm::vec3(-2522.0f, 50.0f, -1956.0f),
                              glm::vec3(-2145.0f, 50.0f, -2117.0f), 54.5, 62.5);
  
	camera->move_cubic_bezier(glm::vec3(-1983.0f, 50.0f, -2117.0f), 
							  glm::vec3(-1673.0f, 50.0f, -1904.0f),
                              glm::vec3(-1772.0f, 50.0f, -2098.0f),
                              glm::vec3(-1673.0f, 50.0f, -1904.0f), 62.5, 65.5);

    camera->move_to_point(glm::vec3(-1673.0f, 50.0f, -1904.0f), glm::vec3(865.0f, 0.0f, -398.0f), 65.5, 65.51);
    
    camera->move_to_point(glm::vec3(865.0f, 0.0f, -398.0f), glm::vec3(430.0f, -430.0f, -135.0f), 76.5, 80.7);

  
	camera->move_cubic_bezier(glm::vec3(430.0f, -430.0f, -135.0f), 
							  glm::vec3(190.0f, -430.0f, -25.0f),
                              glm::vec3(430.0f, -430.0f, -135.0f),
                              glm::vec3(327.0f, -430.0f, 67.0f), 80.7, 82.5);
  
	camera->move_cubic_bezier(glm::vec3(190.0f, -430.0f, -25.0f), 
							  glm::vec3(-170.0f, -435.0f, 0.0f),
                              glm::vec3(90.0f, -435.0f, 10.0f),
                              glm::vec3(-57.0f, -435.0f, -17.0f), 82.5, 84.9);
 
	camera->move_cubic_bezier(glm::vec3(-170.0f, -435.0f, 0.0f), 
							  glm::vec3(-430.0f, -430.0f, 70.0f),
                              glm::vec3(-260.0f, -430.0f, 13.0f),
                              glm::vec3(-337.0f, -430.0f, -15.0f), 84.9, 87.5);
  
	camera->move_cubic_bezier(glm::vec3(-430.0f, -430.0f, 70.0f), 
							  glm::vec3(-700.0f, -405.0f, -500.0f),
                              glm::vec3(-517.0f, -405.0f, 118.0f),
                              glm::vec3(-700.0f, -405.0f, -500.0f), 87.5, 91.5);
 
	camera->move_to_point(glm::vec3(-700.0f, -405.0f, -500.0f), glm::vec3(-700.0f, -150.0f, -500.0f), 91.5, 95.5);

    camera->move_to_point(glm::vec3(-700.0f, -150.0f, -500.0f), glm::vec3(992.0f, 0.0f, -181.0f), 95.5, 95.51);

    camera->move_cubic_bezier(glm::vec3(992.0f, 0.0f, -181.0f), 
							  glm::vec3(997.0f, 0.0f, -184.0f),
                              glm::vec3(993.0f, 0.0f, -183.0f),
                              glm::vec3(995.0f, 0.0f, -184.0f), 95.51, 99.5);
  
	camera->move_cubic_bezier(glm::vec3(997.0f, 0.0f, -184.0f), 
							  glm::vec3(999.0f, 0.0f, -179.0f),
                              glm::vec3(999.0f, 0.0f, -183.0f),
                              glm::vec3(999.0f, 0.0f, -181.0f), 99.5, 103.5);
  
	camera->move_cubic_bezier(glm::vec3(999.0f, 0.0f, -179.0f), 
							  glm::vec3(994.0f, 0.0f, -177.0f),
                              glm::vec3(999.0f, 0.0f, -177.0f),
                              glm::vec3(996.0f, 0.0f, -176.0f), 103.5, 107.5);
 
	camera->move_cubic_bezier(glm::vec3(994.0f, 0.0f, -177.0f), 
							  glm::vec3(992.0f, 0.0f, -181.0f),
                              glm::vec3(992.0f, 0.0f, -178.0f),
                              glm::vec3(992.0f, 0.0f, -179.0f), 107.5, 111.5);

    camera->move_to_point(glm::vec3(992.0f, 0.0f, -181.0f), glm::vec3(993.0f, 0.0f,-180.8f), 111.5, 112.5);
    
	//looks
    camera->take_look_around(180.0f, glm::vec2(0.0f, -1.0f), 0.0, 5.0);
    camera->take_look_around(140.0f, glm::vec2(0.0f, -1.0f), 5.0, 6.2);
    
    camera->take_look_around(145.0f, glm::vec2(0.0f, 1.0f), 7.0, 15.0);
    
    camera->take_look_around(150.0f, glm::vec2(0.0f, -1.0f), 19.8, 23.8);
    
    camera->take_look_around(145.0f, glm::vec2(0.2f, 1.0f), 24.4, 29.5);
    camera->take_look_around(70.0f, glm::vec2(0.4f, -1.0f), 29.5, 31.5);
    
    camera->take_look_around(130.0f, glm::vec2(0.25f, -1.0f), 32.0, 36.5);
    camera->take_look_around(115.0f, glm::vec2(0.0f, 1.0f), 36.5, 40.5);
    camera->take_look_around(110.0f, glm::vec2(0.0f, 1.0f),40.5, 44.5);
    camera->take_look_around(150.0f, glm::vec2(0.0f, 1.0f),44.5, 50.5);
    
    camera->take_look_around(160.0f, glm::vec2(0.0f, -1.0f),55.1, 58.0);
    camera->take_look_around(38.0f, glm::vec2(0.0f, 1.0f), 58.0, 62.0);
    
    camera->take_look_around(40.0f, glm::vec2(0.0f, -1.0f), 65.21, 65.24);
    camera->take_look_around(5.0f, glm::vec2(-1.0f, 0.0f), 65.24, 65.26);
    
    camera->take_look_around(165.0f, glm::vec2(0.0f, -1.0f), 73.52, 73.62);
    
    camera->take_look_around(70.0f, glm::vec2(0.0f, 1.0f),74.0, 78.4);
    camera->take_look_around(170.0f, glm::vec2(-1.0f, 0.0f), 78.4, 82.6);
    camera->take_look_around(140.0f, glm::vec2(0.01f, -1.0f), 82.6, 87.8);
    camera->take_look_around(90.0f, glm::vec2(1.0f, -0.8f), 87.8, 90.8);
    camera->take_look_around(45.0f, glm::vec2(-1.0f, 0.0f), 90.8, 95.4);

    camera->look_at_point(glm::vec3(995.0f,0.0f,-180.0f), 95.5, 130.0);

    //cuts
    camera->set_cut(0.0, 2.0, false);
    
    camera->set_cut(64.0, 65.2);
    camera->set_cut(66.0, 67.0, false);    

    camera->set_cut(72.5, 73.5);    
    camera->set_cut(74.0, 75.0, false);

    camera->set_cut(94.4, 95.4);
    camera->set_cut(96.0, 97.0, false);

    camera->set_cut(125.0, 130.0);
}
void Universe_scene::tick_timer()
{
    TIME = (scene_time * 100.0) / 1.6;
}
