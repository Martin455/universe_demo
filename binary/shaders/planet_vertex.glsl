#version 330 core

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv;
layout(location=2) in vec3 normals;

out vec2 uv_coords;
out vec3 normal;
out vec3 pos;

uniform mat4 fulltrans_matrix;
uniform mat4 model_matrix;

void main()
{
	pos = vec3(model_matrix * vec4(position,1.0));
	normal = normalize(vec3(model_matrix * vec4(normals,0.0)));
    gl_Position = fulltrans_matrix * vec4(position, 1.0);
    uv_coords = uv;
}
