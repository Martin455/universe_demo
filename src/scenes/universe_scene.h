/**
 * @author Martin Prajka
 * @file   universe_scene.h
 */
#ifndef UNIVERSE_SCENE_H
#define UNIVERSE_SCENE_H

#include <limits>
#include "scene.h"
#include "../objects/planet.h"
#include "../objects/g_object.h"
#include "../objects/skybox.h"
#include "../objects/sun.h"
#include "../objects/ring.h"
#include "../particles/explosion.h"
#include "../objects/earth.h"
#include "../objects/saturn.h"
#include "../objects/uranus.h"
#include "../constants.h"
#include "../window/glcheck_function.h"
#include "../framebuffers/antialias_fb.h"
#include "../framebuffers/bloom_fb.h"
#include "../window/object_properties.h"

class Universe_scene: public Scene
{
	//Times variables
	double TIME;
    double end_time;

    bool end_scene;
    //sun (light) position
	glm::vec3 sun_light;
	Sun sun;
	Skybox skybox;
	Meteor_Ring meteors;
	Explosion explosion;
	//plantes
	std::vector<Planet *> planets;
	//meteor
	G_object meteor;

    //shaders
    Shader planet_shader;
    Shader sun_shader;
    Shader skybox_shader;

	void tick_timer();
	void clean_vec_of_planets();

	Antialiasing multisampling;
	Bloom_effect bloom;
    Framebuffer cut_framebuffer;

    void set_animation();
protected:
	virtual void painting();
	virtual void key_event();
    virtual void mouse_event();
public:
	Universe_scene() = default;
    virtual void init(GLFWwindow *win,Animation_camera *cam, Basic_window_function *bwf);
	virtual void draw();
    virtual void end();
};

#endif
