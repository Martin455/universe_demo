/**
 * @author Martin Prajka
 * @file   satelite.cpp
 */
#include "satelite.h"
#include "../window/glcheck_function.h"

Satelite::Satelite(glm::vec3 _position)
{
	position = _position;
	rotation = 0;
}

void Satelite::prepare_depth(GLuint depth_shader)
{
    GLuint location = glGetUniformLocation(depth_shader, "modelMatrix");
    glm::mat4 model_mat =  glm::translate(position) * glm::rotate(deg_to_rad(rotation), glm::vec3(0,1,0));
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_mat[0][0]);

    GLCheck::checkGLerror(HERE);       
}

void Satelite::update(Shader &shader, GLuint shadowmap)
{
    glm::mat4 model_mat = glm::translate(position) * glm::rotate(deg_to_rad(rotation), glm::vec3(0,1,0));

    shader.set_mat4("model_matrix", model_mat);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);

    GLCheck::checkGLerror(HERE);
}

void Satelite::draw()
{
    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    GLCheck::checkGLerror(HERE);
}
