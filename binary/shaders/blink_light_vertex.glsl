#version 330 core

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv_coords;

out vec2 uv;

uniform mat4 projection;
uniform mat4 transform_matrix;

void main()
{
	mat4 modelView = transform_matrix;

	//bilboarding
	modelView[0].xyz = vec3(1.0,0.0,0.0);
	modelView[1].xyz = vec3(0.0,1.0,0.0);
	modelView[2].xyz = vec3(0.0,0.0,1.0);

	gl_Position = projection * modelView * vec4(position, 1.0);
	uv = uv_coords;
}