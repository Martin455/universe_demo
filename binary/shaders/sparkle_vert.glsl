#version 330 core

layout(location=0) in vec3 pos;
layout(location=1) in vec2 uv_coords;

uniform mat4 perspective;
uniform mat4 view;
uniform float scale_r;

out vec2 uv;

void main()
{
	mat4 modelview = view;

	//billboarding
	modelview[0][0] = 1.0;
	modelview[0][1] = 0.0;
	modelview[0][2] = 0.0;

	modelview[1][0] = 0.0;
	modelview[1][1] = 1.0;
	modelview[1][2] = 0.0;

	modelview[2][0] = 0.0;
	modelview[2][1] = 0.0;
	modelview[2][2] = 1.0;

	uv = uv_coords;
	gl_Position = perspective * modelview * vec4(pos * scale_r,1.0);
}