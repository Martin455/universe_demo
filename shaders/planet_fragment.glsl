#version 330 core

in vec2 uv_coords;
in vec3 pos;
in vec3 normal;

out vec4 color;

uniform sampler2D tex;
uniform vec3 light_position;

void main()
{
	float length = (light_position.x - pos.x) * (light_position.x - pos.x) + (light_position.y - pos.y) * (light_position.y - pos.y) + (light_position.z - pos.z) * (light_position.z - pos.z);
	vec3 light_vec = normalize(light_position.xyz - pos);
	float light = dot(light_vec,normal);
	if (light<0.07)
	{
		light = 0.07;
	}

    color = vec4(texture(tex, uv_coords).rgb * (light/(length*0.0000008)),texture(tex, uv_coords).a);
//    color = vec3(0.5,0.5,0.5) * (light/(length*0.0003));
}
