/**
 * @author Martin Prajka
 * @file loader.cpp
 */
#include "loader.h"
#include <stdexcept>

Mesh Loader::load_object(const char *file_name)
{
	Mesh new_mesh;
	Assimp::Importer import;

	const aiScene *scene = import.ReadFile(file_name,aiProcess_Triangulate); 

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		throw std::runtime_error("Invalid object file!\n");
	}

    //for extension (support for more meshes)
	unsigned num_meshes = 1; // scene->mNumMeshes;
	for (unsigned i = 0; i < num_meshes; ++i)
	{
		aiMesh *mesh = scene->mMeshes[i];

		for (unsigned j = 0; j < mesh->mNumVertices ; ++j)
		{
			aiVector3D pos = mesh->mVertices[j];
            //fill vertex
			new_mesh.vertices.push_back(glm::vec3(pos.x,pos.y,pos.z));

            //support for 1 set UV coords
			if (mesh->mTextureCoords[0]!=nullptr)
			{
                //fill uv coords
				aiVector3D uv = mesh->mTextureCoords[0][j];
				new_mesh.texture_uv.push_back(glm::vec2(uv.x,uv.y));
			}	

            //fill normals
			if (mesh->mNormals!=nullptr)
			{
				aiVector3D normal = mesh->mNormals[j];
				new_mesh.normals.push_back(glm::vec3(normal.x,normal.y,normal.z));
			}
		}
        //setting indicies
		if (mesh->HasFaces())
		{	
			for (unsigned j = 0; j < mesh->mNumFaces; ++j)
			{
				aiFace face = mesh->mFaces[j];
				for (unsigned l = 0; l < face.mNumIndices; ++l)
				{
					new_mesh.indices.push_back(face.mIndices[l]);
				}
			}
		}
	}

	return new_mesh;
}
