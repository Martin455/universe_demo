/**
 * @author Martin Prajka
 * @file   object_properties.h
 * @brief  file contains classes for shaders and textures
 */
#ifndef GL_OBJECT_PROPERTIES_H
#define GL_OBJECT_PROPERTIES_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <fstream>
#include <string>
#include <stdexcept>
#include <exception>


/**
 * @brief The Shader class encapsulated workflow with shaders
 */
class Shader
{
    //Static methods for using in framebuffers
	static std::string get_shader_code(const char *filename);
	static void check_shader(GLuint shaderID);
	static void check_program(GLuint programID);

    GLuint id;
public:
    Shader() = default;
    virtual ~Shader();

    void set_shader(const char *vertexfile, const char *fragmentfile);
    void bind();
    void unbind();
    GLuint get_program_id();

    void set_mat4(const char *name, glm::mat4 &matrix);
    void set_mat3(const char *name, glm::mat3 &matrix);
    void set_vec4(const char *name, glm::vec4 &vec);
    void set_vec3(const char *name, glm::vec3 &vec);
    void set_float(const char *name, float value);
    void set_int(const char *name, int value);

	static GLuint create_program(const char *vertexfile, const char *fragmentfile);
};

/**
 * @brief The Texture class create textures from file, empty or multisample
 */
class Texture
{
public:
	static GLuint create_texture(const char *filename, bool mipmap = true);
	static GLuint create_empty_texture(int width, int height, bool wrap = false);
	static GLuint create_multisample_texture(int samples, int width, int height);
};

#endif
