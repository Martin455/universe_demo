/**
 * @author Martin Prajka
 * @file ring.h
 */
#ifndef RING_H
#define RING_H

#include "g_object.h"
#include <cmath>

class Meteor_Ring
{
	G_object *meteor;
	struct variables {
		glm::vec3 position;
		float angle;
		float radius;
		float speed;
		float scale_x;
		float scale_y;
		float scale_z;
	};
	
	std::vector<variables> data;
	unsigned maximum; 
public:
	Meteor_Ring();
	void init(G_object &obj, unsigned max,unsigned num,float r,float speed_limit = 1.0, bool rand_scale = false);
	void draw(glm::mat4 &view,glm::vec3 &light,glm::vec3 planet,double time, GLuint shader, int rotation_dir = 0);
};

#endif
