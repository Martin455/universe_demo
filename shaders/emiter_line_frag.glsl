#version 330 core

out vec4 color;
in vec2 uv;

uniform sampler2D tex;

void main()
{
	vec3 ambient_color = vec3(0.85,0.45,0.11);
	vec4 texel_color = texture(tex, uv);
	color = vec4(texel_color.rgb * ambient_color, 1.0);
}