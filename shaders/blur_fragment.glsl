#version 330 core

in vec2 uv_coords;

out vec4 out_color;

uniform sampler2D tex;

uniform bool horizontal;

float weight[6] = float[](0.141836,0.13424,0.113806,0.086425,0.05879,0.035822);

void main()
{
	vec2 texel_size =  (1.0 / textureSize(tex, 0));
	vec3 result_color = texture(tex, uv_coords).rgb * weight[0];

	if (horizontal)
	{
		for (int i = 1; i < 6; ++i)
		{
			result_color += texture(tex, uv_coords + vec2((texel_size.x * i), 0.0)).rgb * weight[i];
			result_color += texture(tex, uv_coords - vec2((texel_size.x * i), 0.0)).rgb * weight[i];
		}
	}
	else
	{
		for (int i = 1; i < 6; ++i)
		{
			result_color += texture(tex, uv_coords + vec2(0.0, (texel_size.y * i))).rgb * weight[i];
			result_color += texture(tex, uv_coords - vec2(0.0, (texel_size.y * i))).rgb * weight[i];
		}	
	}
	out_color = vec4(result_color, 1.0);
}