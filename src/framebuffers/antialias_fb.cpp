/**
 * @file antialias_fb.cpp
 * @author Martin Prajka
 * @brief file contains implementation of Antialiasing class
 */
#include "antialias_fb.h"
#include <stdexcept>
#include <exception>
#include "../window/glcheck_function.h"

Antialiasing::~Antialiasing()
{
	glDeleteTextures((GLsizei)textures.size(), textures.data());

	glDeleteRenderbuffers(1, &rbo);
	glDeleteFramebuffers(1, &fbo);
}

void Antialiasing::init(int width, int height, int samples, int supersample)
{
    if (supersample < 1)
        supersample = 1;
    this->width = width * supersample;
    this->height = height * supersample;
	this->samples = samples;

	glGenFramebuffers(1,&fbo);
    glGenFramebuffers(1, &nonms_fbo);
    set_textures(2);

	set_render_buffer();

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	
	GLCheck::checkGLFBstatus(HERE);
	glBindFramebuffer(GL_FRAMEBUFFER,0);

	GLCheck::checkGLerror(HERE);
}

void Antialiasing::set_render_buffer()
{
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH24_STENCIL8, width, height);
	GLCheck::checkGLerror(HERE);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	GLCheck::checkGLerror(HERE);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
	GLCheck::checkGLerror(HERE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GLCheck::checkGLerror(HERE);
}

void Antialiasing::set_textures(int count)
{
    if (count != 2)
		throw std::runtime_error("Wrong count of textures for multisampling");

	textures.resize(count);
	textures[0] = Texture::create_multisample_texture(samples, width, height);
    textures[1] = Texture::create_empty_texture(width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, textures[0], 0);

    glBindFramebuffer(GL_FRAMEBUFFER, nonms_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[1], 0);


    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    GLCheck::checkGLerror(HERE);

}

void Antialiasing::apply(GLuint draw_framebuffer, int draw_width, int draw_height)
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, nonms_fbo);
    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, nonms_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, draw_framebuffer);
    glBlitFramebuffer(0, 0, width, height, 0, 0, draw_width, draw_height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    glBindFramebuffer(GL_FRAMEBUFFER, draw_framebuffer);
}
