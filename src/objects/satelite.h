/**
 * @author Martin Prajka
 * @file   satelite.h
 */
#ifndef SATELITE_H
#define SATELITE_H

#include "g_object.h"
#include "../constants.h"

class Satelite:public G_object
{
	glm::vec3 position;
	float rotation;
public:
	Satelite() { rotation = 0; }
	Satelite(glm::vec3 _position);
	
	void set_position(glm::vec3 _position) { position = _position; }
	void set_rotation(float _rotation) { rotation = _rotation; }

    void prepare_depth(GLuint depth_shader);
    void update(Shader &shader, GLuint shadowmap);
	void draw();
};

#endif
