#version 330 core

in vec2 uv;

out vec4 color_out;

uniform sampler2D tex;
//uniform sampler2D tex_alpha;

float rand(vec2 v)
{
	return fract(sin(dot(v.xy,vec2(12.9898,78.233)))*43758.5453);
}

void main()
{
	vec4 color = texture(tex,uv);
	float alpha_channel = 1.0; //texture(tex_alpha, uv).a;
	// if (alpha_channel <= 0.25)
	// 	discard;

	float rand_num = rand(uv);
	float r_num = rand(color.rg);

	float colR = color.r * 0.325 * r_num;
	float colG = color.g * 0.525 * r_num;
	float colB = color.b * 0.285 * r_num;
	float tmp = abs((sin(colB)*cos(colG))/1.5) + 0.4;
	float colors = abs((sin(colR)*cos(tmp))/2.0) + 1.2;
	
	float xaxis = uv.x * rand_num * 0.321;
	float yaxis = uv.y * rand_num * 0.231;
	float axisis = abs((cos(xaxis)*sin(yaxis))/2.0) + 1.0;

	color_out = color * (colors + axisis);
	float brigthness = (color_out.r * 0.2290) + (color_out.g * 0.3331) + (color_out.b * 0.1453);
	color_out *= brigthness;

	color_out = vec4(color_out.rgb * vec3(1.0,0.4,0.3),alpha_channel);
}