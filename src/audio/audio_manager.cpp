/**
 * @author Martin Prajka
 * @file   audio_manager.cpp
 * @brief  file contains implementation of Audio_manager class
 */
#include "audio_manager.h"

Audio_manager *Audio_manager::instance_am = nullptr;

Audio_manager::~Audio_manager()
{
    //free music memory
    if (!music_list.empty())
    {
        for (auto x = music_list.begin(); x != music_list.end(); ++x)
        {
            if (x->second != nullptr)
                delete x->second;
        }
    }

    //free sound memory
    if (!sound_list.empty())
    {
        for (auto x = sound_list.begin(); x != sound_list.end(); ++x)
        {
            if (x->second.sound != nullptr)
                delete x->second.sound;
            if (x->second.buffer != nullptr)
                delete x->second.buffer;
        }
    }
}

void Audio_manager::init()
{
    if (instance_am == nullptr)
        instance_am = new Audio_manager();
}

Audio_manager *Audio_manager::instance()
{
    if (!instance_am)
    {
        instance_am = new Audio_manager();
    }
    return instance_am;
}

void Audio_manager::dispose()
{
	if (instance_am != nullptr)
	{
		delete instance_am;
		instance_am = nullptr;
	}
}

bool Audio_manager::create_music(std::string name, std::string filename)
{
    if (find_music(name))
        return false;   //music is already exist

    music_list[name] = new sf::Music();

    if (!music_list[name]->openFromFile(filename))
    {
        delete_music(name);
        return false;
    }

    return true;
}

bool Audio_manager::find_music(std::string name)
{
    if (music_list.find(name) != music_list.end())
        return true;
    return false;
}

void Audio_manager::play_music(std::string name, bool loop)
{
    if (!find_music(name))
        return;

    music_list[name]->setLoop(loop);
    music_list[name]->play();
}

void Audio_manager::set_music_volume(std::string name, float volume)
{
    if (!find_music(name))
        return;

    music_list[name]->setVolume(volume);
}

void Audio_manager::pause_music(std::string name)
{
    if (!find_music(name))
        return;

    music_list[name]->pause();
}

bool Audio_manager::playing_music(std::string name)
{
    if (!find_music(name))
        return false;

    if (music_list[name]->getStatus() == sf::Music::Playing)
        return true;

    return false;
}

void Audio_manager::stop_music(std::string name)
{
    if (!find_music(name))
        return;

    music_list[name]->stop();
}

void Audio_manager::delete_music(std::string name)
{
    auto it_music = music_list.find(name);
    if (it_music != music_list.end())
    {
        if (music_list[name] != nullptr)
        {
            delete music_list[name];
            music_list[name] = nullptr;
            music_list.erase(it_music);
        }
    }
}

bool Audio_manager::create_sound(std::string name, std::string filename)
{
    if (find_sound(name))
        return false; //sound is already exist

    Sound_n_buffer tmp_sound;

    //create sound buffer
    tmp_sound.buffer = new sf::SoundBuffer();
    if (!tmp_sound.buffer->loadFromFile(filename))
    {
        delete_sound(name);
        return false;
    }

    tmp_sound.sound = new sf::Sound(*(tmp_sound.buffer));
    sound_list[name] = tmp_sound;


    return true;
}

bool Audio_manager::find_sound(std::string name)
{
    if (sound_list.find(name) != sound_list.end())
        return true;
    return false;
}

void Audio_manager::play_sound(std::string name, bool loop)
{
    if (!find_sound(name))
        return;

    sound_list[name].sound->setLoop(loop);
    sound_list[name].sound->play();
}

void Audio_manager::set_sound_volume(std::string name, float value)
{
    if (!find_sound(name))
        return;

    sound_list[name].sound->setVolume(value);
}

void Audio_manager::set_sound_position(std::string name, glm::vec3 position, bool relative_to_listener)
{
    if (!find_sound(name))
        return;

    sound_list[name].sound->setRelativeToListener(relative_to_listener);
    sound_list[name].sound->setPosition(position.x, position.y, position.z);
}

void Audio_manager::set_min_distance(std::string name, float min_distance, float attenuation)
{
    if (!find_sound(name))
        return;

    if (min_distance > -0.01f)
    {
        sound_list[name].sound->setMinDistance(min_distance);
    }
    if (attenuation > -0.01f)
    {
        sound_list[name].sound->setAttenuation(attenuation);
    }
}

bool Audio_manager::playing_sound(std::string name)
{
    if (!find_sound(name))
        return false;

    if (sound_list[name].sound->getStatus() == sf::Sound::Playing)
        return true;
    return false;
}

void Audio_manager::pause_sound(std::string name)
{
    if (!find_sound(name))
        return;

    sound_list[name].sound->pause();
}

void Audio_manager::stop_sound(std::string name)
{
    if (!find_sound(name))
        return;

    sound_list[name].sound->stop();
}

void Audio_manager::delete_sound(std::string name)
{
    auto it_sound = sound_list.find(name);
    if (it_sound != sound_list.end())
    {
        if (sound_list[name].buffer != nullptr)
        {
            delete sound_list[name].buffer;
            sound_list[name].buffer = nullptr;
        }
        if (sound_list[name].sound != nullptr)
        {
            delete sound_list[name].sound;
            sound_list[name].sound = nullptr;
        }
        sound_list.erase(it_sound);
    }
}

glm::vec3 Audio_manager::get_sound_position(std::string name)
{
    if (!find_sound(name))
        return glm::vec3(); //zero vector if doesn't exist sound

    sf::Vector3f tmpvec = sound_list[name].sound->getPosition();
    return glm::vec3 (tmpvec.x, tmpvec.y, tmpvec.z);
}

void Audio_manager::set_listener(glm::vec3 position, glm::vec3 direction)
{
    sf::Listener::setPosition(position.x, position.y, position.z);
    sf::Listener::setDirection(direction.x, direction.y, direction.z);
}

void Audio_manager::set_listener_position(glm::vec3 position)
{
    sf::Listener::setPosition(position.x, position.y, position.z);
}

void Audio_manager::set_listener_direction(glm::vec3 direction)
{
    sf::Listener::setDirection(direction.x, direction.y, direction.z);
}

glm::vec3 Audio_manager::get_listener_position() const
{
    sf::Vector3f tmpvec = sf::Listener::getPosition();
    return glm::vec3 (tmpvec.x, tmpvec.y, tmpvec.z);
}

glm::vec3 Audio_manager::get_listener_direction() const
{

    sf::Vector3f tmpvec = sf::Listener::getDirection();
    return glm::vec3 (tmpvec.x, tmpvec.y, tmpvec.z);
}


