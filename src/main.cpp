/**
 * @file main.cpp
 * @brief Project Universe demo - Graphics demo placed in space
 * @details Demo implements post-processing effects, phong light model,
 * particle system, animations, sounds
 * @author Martin Prajka
 **/

#include "window/glwindow.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <stdexcept>

#ifdef _WIN32
    //force start powerfull GPU in Windows
    //AMD
	extern "C"
	{
		__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
	}
    //NVIDIA
	extern "C"
	{
		__declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
	}
#endif

int main()
{
	try
	{
	    GLWindow w;        
        std::srand(std::time(nullptr));
	    
	    w.init();
	    w.exec();
	}
	catch(std::string e)
	{
		std::cerr << e << std::endl;
		return 1;
	}
	catch(std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		return 2;
	}
    return 0;
}
