#version 330 core

/*
*	Shader without lighting
*
**/

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv;
layout(location=2) in vec3 normal;

out vec2 uv_coords;

uniform mat4 perspective;
uniform mat4 transform_matrix;

void main()
{
	uv_coords = uv;

	gl_Position = perspective * transform_matrix * vec4(position, 1.0);
}


