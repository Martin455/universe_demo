/**
 * @file blur_framebuffer.cpp
 * @author Martin Prajka
 * @brief Implementation Blur_framebuffer class
 */
#include "blur_framebuffer.h"


GLuint Blur_framebuffer::get_fbo() const
{
    return fbo;
}

int Blur_framebuffer::get_width() const
{
    return width;
}

int Blur_framebuffer::get_height() const
{
    return height;
}

Blur_framebuffer::~Blur_framebuffer()
{
    glDeleteTextures((GLsizei)textures.size(), textures.data());

    glDeleteRenderbuffers(1, &rbo);
    glDeleteFramebuffers(1, &fbo);
}

void Blur_framebuffer::init(int width, int height)
{
    this->width = width;
    this->height = height;

    glGenFramebuffers(1, &fbo);

    textures[0] = Texture::create_empty_texture(width, height, true);
    textures[1] = Texture::create_empty_texture(width, height, true);
    active_texture = 0;
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[active_texture], 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Blur_framebuffer::bind()
{
    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void Blur_framebuffer::unbind(GLuint old_fbo, int old_width, int old_height)
{
    glViewport(0, 0, old_width, old_height);
    glBindFramebuffer(GL_FRAMEBUFFER, old_fbo);
}

void Blur_framebuffer::binding_draw(Shader &shader)
{
    int no_active = active_texture ^ 1; //get actual texture

    shader.set_int("new_color", 0);
    shader.set_int("old_color", 1);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[active_texture]);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[no_active]);
}

void Blur_framebuffer::draw()
{
    quad.draw();
}

void Blur_framebuffer::swap_textures()
{
    active_texture ^= 1; //switch index and change texture target

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[active_texture], 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

