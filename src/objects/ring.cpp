/**
 * @author Martin Prajka
 * @file ring.cpp
 */
#include "ring.h"
#include <cstdlib>

Meteor_Ring::Meteor_Ring() {}

void Meteor_Ring::init(G_object &obj, unsigned max, unsigned num, float r, float speed_limit, bool rand_scale)
{
	meteor = &obj;
	maximum = max;

	for (unsigned i = 0; i < maximum; ++i)
	{
		variables tmp;
		tmp.angle = (rand()/ float(RAND_MAX)) * 360.0f;

		tmp.position.y = 0;
		int mod = i%num;
		switch(mod)
		{
			case 0:
				tmp.radius = r + 1.5f;
				tmp.speed = 0.5f;
				break;
			case 1:
				tmp.radius = r + 1.8f;
				tmp.speed = 0.5f;
				break;
			case 2:
				tmp.radius = r + 2.0f;
				tmp.speed = 0.45f;
				break;
			case 3:
				tmp.radius = r + 2.4f;
				tmp.speed = 0.4f;
				break;
			case 4:
				tmp.radius = r + 2.6f;
				tmp.speed = 0.4f;
				break;
			case 5:
				tmp.radius = r + 2.8f;
				tmp.speed = 0.35f;
				break;
			case 6:
				tmp.radius = r + 3.2f;
				tmp.speed = 0.3f;
				break;
			case 7:
				tmp.radius = r + 3.4f;
				tmp.speed = 0.3f;
				break;
			case 8:
				tmp.radius = r + 3.6f;
				tmp.speed = 0.25f;
				break;
			case 9:
				tmp.radius = r + 3.8f;
				tmp.speed = 0.2f;
				break;
			case 10:
				tmp.radius = r + 4.0f;
				tmp.speed = 0.15f;
				break;
			case 11:
				tmp.radius = r + 4.3f;
				tmp.speed = 0.12f;
				break;
			case 12:
				tmp.radius = r + 4.7f;
				tmp.speed = 0.1f;
				break;
			case 13:
				tmp.radius = r + 5.0f;
				tmp.speed = 0.1f;
				break;
			default:
				tmp.radius = r + 5.3f;
				tmp.speed = 0.1f;
		}
		tmp.speed *= speed_limit;

		if(rand_scale)
		{
			tmp.scale_x = (rand()/ float(RAND_MAX)) * 3.9f;
			tmp.scale_y = (rand()/ float(RAND_MAX)) * 3.9f;
			tmp.scale_z = (rand()/ float(RAND_MAX)) * 3.9f;
		}
		else
		{
			tmp.scale_x = 0.3f;
			tmp.scale_y = 0.3f;
			tmp.scale_z = 0.3f;
		}

		data.push_back(tmp);
	}
}

void Meteor_Ring::draw(glm::mat4 &view, glm::vec3 &light, glm::vec3 planet, double time, GLuint shader, int rotation_dir)
{
    for (unsigned i = 0; i < maximum; ++i)
    {
        if (rotation_dir==0)
        {
            data[i].position.x = (cos((data[i].angle/180.0f)*(-M_PI))*data[i].radius);
            data[i].position.z = (sin((data[i].angle/180.0f)*(-M_PI))*data[i].radius);
        }
        else
        {
            data[i].position.x = (cos((data[i].angle/180.0f)*M_PI)*data[i].radius);
            data[i].position.y = (sin((data[i].angle/180.0f)*M_PI)*data[i].radius);
        }

        glm::mat4 transmat = glm::translate(planet) * glm::translate(glm::rotate(0.3f,glm::vec3(1,0,0.3f)),data[i].position) * glm::scale(glm::vec3(data[i].scale_x,data[i].scale_y,data[i].scale_z)) * glm::rotate(float(time*0.1),glm::vec3(1.0f,1.0f,1.0f));

        meteor->update(view,transmat,light, shader);
        meteor->draw();

        data[i].angle += 2 * data[i].speed;
        if (data[i].angle>=360)
            data[i].angle=0;
    }

}
