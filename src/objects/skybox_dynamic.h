/**
 * @author Martin Prajka
 * @file   skybox_dynamic.h
 */
#ifndef SKYBOX_DYNAMIC_H
#define SKYBOX_DYNAMIC_H

#include "skybox.h"

class Skybox_dynamic: public Skybox
{
    GLuint texture2_id;
public:
    virtual ~Skybox_dynamic();
    void init(Mesh &mesh, const char *texture_file, const char *texture2_file);
    void draw(glm::mat4 &transformMat, GLuint shader);
};

#endif
