/**
 * @author Martin Prajka
 * @file framebuffer.cpp
 */
#include "framebuffer.h"
#include <stdexcept>
#include <exception>
#include "../window/glcheck_function.h"

Framebuffer::~Framebuffer()
{
	glDeleteTextures((GLsizei)textures.size(), textures.data());

	glUseProgram(0);
	glDeleteProgram(program_fb);

	glDeleteRenderbuffers(1, &rbo);
	glDeleteFramebuffers(1, &fbo);
}

void Framebuffer::init(int width,int height,int supersampling, int count_of_textures)
{
    if (supersampling < 1)
        supersampling = 1;

    this->width = width * supersampling;
    this->height = height * supersampling;

	glGenFramebuffers(1,&fbo);

	set_textures(count_of_textures);

	set_render_buffer();

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	
	GLCheck::checkGLFBstatus(HERE);
	glBindFramebuffer(GL_FRAMEBUFFER,0);

	GLCheck::checkGLerror(HERE);
}

void Framebuffer::bind_fbo()
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glViewport(0, 0, width, height);
}

void Framebuffer::unbind_fbo(GLuint old_fbo, int old_width, int old_height)
{
    glBindFramebuffer(GL_FRAMEBUFFER, old_fbo);
    glViewport(0, 0, old_width, old_height);
	GLCheck::checkGLerror(HERE);
}

void Framebuffer::set_render_buffer()
{
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);

    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::set_shaders(const char *vertex, const char *fragment)
{
	program_fb = Shader::create_program(vertex, fragment);
}

void Framebuffer::binding_draw()
{
	glUseProgram(program_fb);
	std::stringstream stream;
	GLuint location = 0;
	for (unsigned int i = 0; i < textures.size(); ++i)
	{
		if (i==0)
		{
			stream << "tex";	
		}
		else
		{
			stream << "tex" << i;
		}
		
		location = glGetUniformLocation(program_fb, stream.str().c_str());
		glUniform1i(location,i);

		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, textures[i]);
	}
}

int Framebuffer::get_width() const
{
    return width;
}

int Framebuffer::get_height() const
{
    return height;
}

GLuint Framebuffer::get_shader_id() const
{
    return program_fb;
}

void Framebuffer::set_textures(int count)
{
    textures.resize(count);

	for (int i = 0; i < count; ++i)
	{
        textures[i] = Texture::create_empty_texture(width, height);
		
	    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, textures[i], 0);
	    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

std::vector<GLuint> Framebuffer::get_textures()
{
	return textures;
}

void Framebuffer::draw_quad()
{
	quad.draw();
}

void Framebuffer::draw_quad(glm::mat4 &, glm::mat4 &)
{
	throw std::runtime_error("Base framebuffer has not implementation draw with matrix");
}
GLuint Framebuffer::get_framebuffer()
{
	return fbo;
}

//Quad object for drawing
Quad_object::Quad_object()
{
	GLfloat quad[] = 
	{
		-1,1,0,1,
		-1,-1,0,0,		
		1,1,1,1,
		1,-1,1,0
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &bo);
	glBindBuffer(GL_ARRAY_BUFFER, bo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*4,0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*4,(void *)(sizeof(GLfloat)*2));

	glBindVertexArray(0);
}

void Quad_object::draw()
{
	glDisable(GL_DEPTH_TEST);
		
	glBindVertexArray(vao);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glEnable(GL_DEPTH_TEST);
}

Quad_object::~Quad_object()
{
	glDeleteBuffers(1, &bo);
	glDeleteVertexArrays(1, &vao);
}
