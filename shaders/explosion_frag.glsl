#version 330 core

in vec2 uv;
in float life;
in float len;
flat in int instance_id;
flat in int kindi;

out vec4 out_color;

uniform sampler2D tex_a1;
uniform sampler2D tex_a2;
uniform sampler2D tex_a3;
uniform sampler2D tex_a4;
uniform sampler2D tex_color;
uniform sampler2D tex_color2;
uniform sampler2D tex_flash;
uniform sampler2D tex_rock;

uniform bool back_direction;

uniform int random_value;

uniform float dust_transition;

const float a = 0.0999;
const float b = 0.001;

vec3 dust_color = vec3(0.133,0.133,0.113);

void main()
{
	//shader for fire and rock part
	if (kindi==0)
	{		
        int instance_num = instance_id + random_value;
        int index_a = instance_num - (4 * (instance_num/4));
            
        vec4 color;
        if (index_a == 0)
            color = texture(tex_a1, uv);
        else if (index_a == 1)
            color = texture(tex_a2, uv);
        else if (index_a == 2)
            color = texture(tex_a3, uv);
        else
            color = texture(tex_a4, uv);

        float alpha = color.a - 0.015;
        if (alpha < 0.02)
            discard;

        int percent = instance_num - (100 * (instance_num/100));
        vec4 color_tex = texture(tex_color, uv);
        if (percent < 15)   //15% tex2 mix with tex - 85% only tex
		{  
            vec4 new_color = texture(tex_color2, uv);
            //mix with alpha channel
            color_tex = mix(color_tex, new_color, 0.3);
        }

        if (alpha > color_tex.a)
            alpha = color_tex.a;

		float brightness = life;
		if (brightness > 1.0)
			brightness = 1.0;
        
        float transition_value = dust_transition - (index_a/100.0);
        if (transition_value < 0.0)
            transition_value = 0.0;

        vec3 final_color = mix(color_tex.rgb, dust_color, transition_value);
		//nasobeni s alfou aby pri ciste cerne castice rovnou zmizela	
        out_color = vec4(final_color, alpha) * brightness;
	}
    else if (kindi == 1)
    {
        vec4 color = texture(tex_a1,uv);
        float alpha = color.a - 0.015;
        if (alpha < 0.05)
            discard;

        vec4 color_tex = texture(tex_rock, uv);

        out_color = vec4(color_tex.rgb, alpha);
    }
	else if (kindi == 2)
	{
		//shader for flash
		float alpha = texture(tex_flash,uv).a;
		float leng = len;
		if (alpha < 0.025)
			discard;

		if (back_direction)
		{
			if (leng > 1.0)
			 	leng = 1.0;
			out_color = vec4(1.0,1.0,1.0-leng,alpha);
		}
		else
		{
			out_color = vec4(1.0,1.0,1.0,alpha);
		}
	}
	else if (kindi == 3)
	{
		//shader for line fire
		float alpha = texture(tex_a1,uv).a;
		if (alpha < 0.05)
			discard;

		float alpha_coefficient = a * life + b;
		alpha *= alpha_coefficient;

        //Max life is 10
        float mix_constant = life/10.0;
        if (mix_constant > 1.0)
            mix_constant = 1.0;

        vec3 color1 = vec3(1,0,0);
        vec3 color2 = vec3(1,0.43,0);
        vec3 result_color = mix(color1, color2, mix_constant);

        mix_constant += 0.01;
        if (mix_constant>1.0)
            mix_constant = 1.0;

        result_color = mix(dust_color, result_color, mix_constant);

        out_color = vec4(result_color, alpha);
	}
}
