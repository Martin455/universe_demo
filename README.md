# README #

##Popis##
Grafické demo z vesmírného prostředí bez omezení velikosti využívající částicové systémy a post-processingové efekty.

##Knihovny##
Aplikace používá opengl ve verzi 3.3 core  
* glew 2.0 - Rozšíření opengl.  
* glfw 3.2.1 - Kontext a práce s oknem aplikace.  
* glm 0.9.6.3 - Matematická knihovna pro práci s vektory a maticemi.  
* SOIL - Pro čtení textur.  
* Assimp 3.3.1 - Pro čtení objektů.  
* SFML 2.4.2 - Rozsáhlá knihovna z které je použita pouze audio část.  
Všechny zmíněné knihovny jsou 64bitové.  
  

##Build##
Jako buildovací systém je použit CMake, který zajišťuje multiplatformní build.  
  
* Windows build - V cmake se musí nastavit cesty k include souborům a ke knihovnám.  
INCLUDE_GL_DIR - cesta k include souborům glew, glfw a gl.  
LIBRARY_GL_DIR - cesta ke knihovnám glew, glfw a gl.  
INCLUDE_ASSIMP_DIR - cesta k include souborům pro assimp.
LIBRARY_ASSIMP_DIR - cesta ke knihovně assimp.  
INCLUDE_SFML_DIR - cesta k include souborům SFML knihovny.  
LIBRARY_SFML_DIR - cesta ke knihovně SFML.  
  
Upozornění pro INCLUDE_GL_DIR a LIBRARY_GL_DIR  
Ve složce pro includové soubory musí být pro každou knihovnu vlastní složka, tedy GL/ GLFW/ glm/ SOIL/.  
Pro složku s knihovnami platí, že všechny knihovny musí být v jedné složce například libs.  

* Linux build - V cmake se nemusí nic nastavovat pouze musí být příslušné závislosti nainstalovány předem v systému.  
Všechny závislosti jdou v ubuntu nainstalovat pomocí balíčkového systému.  

* Seznam oficiálních stránek knihoven - Oficiální stránky knihoven kde jsou popsány i licence.  
Většina knihoven je OpenSource a některé jsou pod BSD licencí.  
  
- www.glfw.org  
- assimp.org/  
- www.lonesock.net/soil.html  
- www.sfml-dev.org/  
- glew.sourceforge.net/  
- glm.g-truc.net  
  
Postup:  
- vytvořit složku build pro cmake výstup  
- ve složce build zadat příkaz cmake ..  
- zadat příkaz make
  
##Spuštění##
Návod na spuštění z dodaných binárních souborů na operačním systému Windows.  
  
Stačí spustit aplikaci souborem Universe_demo.exe, pokud se aplikace nespustí zkontrolujte zda jsou všechny dodané dynamické knihovny ve stejné složce jako soubor Universe_demo.exe.  
Pokud stalé nelze aplikaci spustit doinstalujte Microsoft Visual C++ 2015 Redistributable, instalátor je přiložen ve složce binary (soubor vc_redist.x64.exe).  

##Konfigurace dema##
V demu lze nastavovat uroveń antialiasingu a zapínat či vypínat vertikální synchronizaci.  
Konfigurační soubor není povinný, aplikace jinak nastaví defaultní hodnoty, které jsou MSAA4x SSAA vypnuto (1x) a VSYNC zapnuto.
  
Syntaxe v konfiguračním souboru: typ metody, znak rovno a hodnota.  
příklad    
MSAA=4  
SSAA=2  
VSYNC=1  
  
Hodnoty pro multisampling (MSAA) a supersampling (SSAA) jsou v intervalu <1;8>.  
Pro VSYNC znamená 0 vypnuto a cokoliv větší než 0 zapnuto.  
  
  
  
Autor: Martin Prajka.