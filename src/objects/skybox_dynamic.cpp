/**
 * @author Martin Prajka
 * @file   skybox_dynamic.cpp
 */
#include "skybox_dynamic.h"
#include "../window/glcheck_function.h"

Skybox_dynamic::~Skybox_dynamic()
{
    glDeleteTextures(1, &texture2_id);
}

void Skybox_dynamic::init(Mesh &mesh, const char *texture_file, const char *texture2_file)
{
    Skybox::init(mesh, texture_file);

    texture2_id = Texture::create_texture(texture2_file);
}

void Skybox_dynamic::draw(glm::mat4 &transformMat, GLuint shader)
{
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glCullFace(GL_FRONT);
    
    GLuint uni_location = glGetUniformLocation(shader, "tex");
    glUniform1i(uni_location, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,textureID);

    uni_location = glGetUniformLocation(shader, "tex2");
    glUniform1i(uni_location, 1);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,texture2_id);
    
    glBindVertexArray(vao); 

    uni_location = glGetUniformLocation(shader,"projection");
    glUniformMatrix4fv(uni_location,1,GL_FALSE,&transformMat[0][0]);

    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

    glEnable(GL_DEPTH_TEST);
    
    glDepthMask(GL_TRUE);
    glCullFace(GL_BACK);
    glActiveTexture(GL_TEXTURE0);
    GLCheck::checkGLerror(HERE);
}
