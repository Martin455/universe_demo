/**
 * @author Martin Prajka
 * @file   glcheck_function.h
 * @brief  check gl error functions for debug app
 *         functions returns file, function and line and GL error enum
 */
#ifndef GLCHECK_H
#define GLCHECK_H

#include <sstream>
#include <iostream>
#include <iomanip>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <stdexcept>
#include <exception>

#define HERE  __FILE__ , __func__ , __LINE__

namespace GLCheck
{
    inline void checkGLerror(const char *file, const char *func, unsigned line)
    {
        std::stringstream s;
        switch (glGetError())
        {
        case GL_INVALID_ENUM:
            s << file << ": " << func << ": " << line << ": " << "GL_INVALID_ENUM" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_INVALID_VALUE:
            s << file << ": " << func << ": " << line << ": " << "GL_INVALID_VALUE" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_INVALID_OPERATION:
            s << file << ": " << func << ": " << line << ": " << "GL_INVALID_OPERATION" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            s << file << ": " << func << ": " << line << ": " << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_OUT_OF_MEMORY:
            s << file << ": " << func << ": " << line << ": " << "GL_OUT_OF_MEMORY" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_STACK_UNDERFLOW:
            s << file << ": " << func << ": " << line << ": " << "GL_STACK_UNDERFLOW" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_STACK_OVERFLOW:
            s << file << ": " << func << ": " << line << ": " << "GL_STACK_OVERFLOW" << std::endl;
            throw std::runtime_error(s.str());
            break;
        default:
            break;
        }
    }

    inline void checkGLFBstatus(const char *file, const char *func, unsigned line)
    {
        std::stringstream s;
        switch (glCheckFramebufferStatus(GL_FRAMEBUFFER))
        {
        case GL_INVALID_ENUM:
            s << file << ": " << func << ": " << line << ": " << "GL_INVALID_ENUM" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_INVALID_OPERATION:
            s << file << ": " << func << ": " << line << ": " << "GL_INVALID_OPERATION" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_UNDEFINED:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_UNDEFINED" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_UNSUPPORTED" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE" << std::endl;
            throw std::runtime_error(s.str());
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
            s << file << ": " << func << ": " << line << ": " << "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS" << std::endl;
            throw std::runtime_error(s.str());
            break;
        default:
            break;
        }
    }
}
#endif
