/**
 * @author Martin Prajka
 * @file planet.h
 */
#ifndef PLANET_H
#define PLANET_H

#include "g_object.h"
#include "../constants.h"

class Planet: public G_object
{
protected:
	float angle;
	const float deflect_axis;
	const float major_axis;
	const float minor_axis;
	const float max_speed;
	const float avg_speed;
	const float min_speed;
	const float speed_rotate;
	const glm::mat4 scale_matrix;
	glm::mat4 transform_matrix;
	glm::vec3 position;
public:
	Planet(const float _angle,const float _def_axis, const float _maj_axis,
		   const float _min_axis, const float _max_speed, const float _avg_speed, 
		   const float _min_speed,const float _speed_rotate, const float _scale): 
		angle(_angle), deflect_axis(_def_axis), major_axis(_maj_axis), minor_axis(_min_axis),
		max_speed(_max_speed), avg_speed(_avg_speed), min_speed(_min_speed), 
		speed_rotate(_speed_rotate), scale_matrix(glm::scale(glm::vec3(_scale,_scale,_scale))) {}

    virtual void update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime);
	virtual void draw();

	glm::vec3 get_planet_position() const;
};

#endif
