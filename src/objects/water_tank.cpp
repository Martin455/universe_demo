/**
 * @author Martin Prajka
 * @file   water_tank.cpp
 */
#include "water_tank.h"
#include "../window/glcheck_function.h"

void Water_tank::init(Mesh &mesh, const char *texture_name)
{
    G_object::init(mesh, texture_name);

    model_matrix = glm::translate(glm::vec3(145.5f,-2.75f,-95.5f))
            * glm::scale(glm::vec3(0.5f,0.5f,0.5f));
}

void Water_tank::prepare_depth(GLuint depth_shader)
{
    GLuint location = glGetUniformLocation(depth_shader, "modelMatrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);

    GLCheck::checkGLerror(HERE);       
}

void Water_tank::update(Shader &shader, GLuint shadowmap)
{
    shader.set_mat4("model_matrix", model_matrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);

    GLCheck::checkGLerror(HERE);
}

void Water_tank::draw()
{
    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    GLCheck::checkGLerror(HERE);
}
