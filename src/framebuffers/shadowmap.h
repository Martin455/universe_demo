/**
 * @file shadowmap.h
 * @author Martin Prajka
 */
#ifndef SHADOWMAP_H
#define SHADOWMAP_H

#include "framebuffer.h"
#include "../constants.h"

/**
 * @brief The Shadowmap class create depth map from light projection
 */
class Shadowmap
{
    GLuint fbo;
    GLuint depth_texture;

    int width, height;
    int old_width, old_height;
    //for debug
    Quad_object quad;

public:
    Shadowmap() = default;
    ~Shadowmap();

    //framebuffer operation
    void init(int width, int height);
    void bind(int actually_width, int actually_height);
    void unbind(GLuint framebuffer);

    //configure lightspacematrix
    glm::mat4 get_light_projection();
    glm::mat4 get_light_view(glm::vec3 &light_position);

    //get depth map
    GLuint get_depth_texture();

    //Debug
    void draw_quad();
};


#endif
