/**
 * @author Martin Prajka
 * @file   rocket_interier_object.h
 */
#ifndef ROCKET_INTERIER_OBJECT_H
#define ROCKET_INTERIER_OBJECT_H

#include "g_object.h"
#include "../constants.h"

class Rocket_interier_object : public G_object
{
    glm::mat4 transform;
    GLuint glow_texture = 0;
public:
    void set_model_transform(const glm::mat4 &transform_matrix);
    void set_glow_texture(const char *glow_texture);
    void update(GLuint shader_id, glm::mat4 &perspective, glm::mat4 &view_matrix);
    void draw();

    virtual ~Rocket_interier_object();
};


#endif // ROCKET_INTERIER_OBJECT_H
