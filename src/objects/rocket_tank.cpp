/**
 * @author Martin Prajka
 * @file   rocket_tank.cpp
 */
#include "rocket_tank.h"
#include "../window/glcheck_function.h"


void Rocket_tank::init(Mesh &mesh, const char *texture_name)
{
    G_object::init(mesh, texture_name);

    model_matrix = glm::translate(glm::vec3(50.0f, 70.0f,-90.0f))
            * glm::rotate(-deg_to_rad(90), glm::vec3(1,0,0));
}

void Rocket_tank::prepare_depth(GLuint depth_shader)
{
    GLuint location = glGetUniformLocation(depth_shader, "modelMatrix");    
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);

    GLCheck::checkGLerror(HERE);       
}

void Rocket_tank::update(Shader &shader, GLuint shadowmap)
{
    
    shader.set_mat4("model_matrix", model_matrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);

    GLCheck::checkGLerror(HERE);
}

void Rocket_tank::draw()
{
    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    GLCheck::checkGLerror(HERE);
}
