#version 330 core

in vec2 uv_coords;

out vec4 color;

uniform sampler2D tex;

void main()
{
	color = vec4(texture(tex,uv_coords).rgb, 1.0);
}