#version 330 core

layout(location=0) out vec4 bright_color;

uniform sampler2D tex;
uniform vec3 threshold;

in vec2 uv_coords;

void main()
{
	vec3 main_texel = texture(tex, uv_coords).rgb;

    float brightness = dot (main_texel, threshold);
	
	if (brightness > 1.0)
		bright_color = vec4(main_texel, 1.0);
	else
		bright_color = vec4(0,0,0,1);
}
