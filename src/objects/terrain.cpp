/**
 * @author Martin Prajka
 * @file   terrain.cpp
 */
#include "terrain.h"

Terrain::~Terrain()
{
    glDeleteBuffers((GLsizei)buffers.size(), buffers.data());
    glDeleteTextures(1, &texture);
    glDeleteVertexArrays(1, &vao);
}

void Terrain::init(const char *heightmap, float size, float height)
{
    Mesh mesh = gen_terrain(heightmap, size, height);

    triangles = mesh.indices.size();
    //Set GL
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers((GLsizei)buffers.size(), buffers.data());
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, mesh.get_vertices_byte(), &mesh.vertices[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, mesh.get_normals_byte(), &mesh.normals[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, 0);


    glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
    glBufferData(GL_ARRAY_BUFFER, mesh.get_texture_uv_byte(), &mesh.texture_uv[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.get_indices_byte(), &mesh.indices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    model_matrix = glm::translate(glm::vec3(-75, -13.5f, 150));
}

void Terrain::set_texture(const char *filename)
{
    texture = Texture::create_texture(filename, true);
}

void Terrain::update(Shader &shader, GLuint shadowmap)
{
    shader.set_mat4("model_matrix", model_matrix);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);
}

void Terrain::draw_terrain()
{
    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[3]);
    glDrawElements(GL_TRIANGLES, triangles, GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

Mesh Terrain::gen_terrain(const char *heightmap, float size, float height)
{
    Mesh terrain_mesh;

    get_image_data(heightmap);

    for (int row = 0; row < length; ++row)
    {
        for (int col = 0; col < width; ++col)
        {
            float x = (col / float(width - 1.0f) + start) * size;
            float z = (row / float(length - 1.0f) + start) * size;
            float y = (image_data[row * width + col] / 255.0f) * height;
            glm::vec3 vertex(x, y, z);
            glm::vec2 uv(col / float(width - 1), row / float(length- 1));

            terrain_mesh.vertices.push_back(vertex);
            terrain_mesh.texture_uv.push_back(uv);

            if (col < width - 1 && row < length -1)
            {
                int top_left = row * width + col;
                int top_right = row * width + (col + 1);
                int bottom_left = (row + 1) * width + col;
                int bottom_right = (row+1) * width + (col+1);

                terrain_mesh.indices.push_back(top_left);
                terrain_mesh.indices.push_back(bottom_left);
                terrain_mesh.indices.push_back(top_right);

                terrain_mesh.indices.push_back(bottom_right);
                terrain_mesh.indices.push_back(top_right);
                terrain_mesh.indices.push_back(bottom_left);
            }
         }
    }

    calculating_normals(terrain_mesh);

    return terrain_mesh;
}

void Terrain::get_image_data(const char *heightmap)
{
    int h,w;
    image_data = SOIL_load_image(heightmap,&w,&h,0,SOIL_LOAD_L);
    if (!image_data)
    {
        throw std::runtime_error("Can't load heightmap texture");
    }

    width = w;
    length = h;
}

void Terrain::calculating_normals(Mesh &mesh)
{
    for (int row = 0; row < length; ++row)
    {
        for (int col = 0; col < width; ++col)
        {
            glm::vec3 p0(0), p1(0), p2(0), p3(0), p4(0);
            glm::vec3 v1(0), v2(0), v3(0), v4(0);
            glm::vec3 n12(0), n23(0), n34(0), n41(0);

            p0 = mesh.vertices[row * width + col];

            if (col > 0)
            {
                p1 = mesh.vertices[row * width + (col - 1)];
            }
            else
            {
                p1 = mesh.vertices[row * width + col];
            }
            if (col < width - 1)
            {
                p3 = mesh.vertices[row * width + (col + 1)];
            }
            else
            {
                p3 = mesh.vertices[row * width + col];
            }
            if (row > 0)
            {
                p2 = mesh.vertices[(row - 1) * width + col];
            }
            else
            {
                p2 = mesh.vertices[row * width + col];
            }
            if (row < length - 1)
            {
                p4 = mesh.vertices[(row + 1) * width + col];
            }
            else
            {
                p4 = mesh.vertices[row * width + col];
            }

            v1 = p1 - p0;
            v2 = p2 - p0;
            v3 = p3 - p0;
            v4 = p4 - p0;

            if (glm::length(v1) > 0.0f && glm::length(v2) > 0.0f)
            {
                n12 = glm::normalize(glm::cross(v2, v1));
            }

            if (glm::length(v2) > 0.0f && glm::length(v3) > 0.0f)
            {
                n23 = glm::normalize(glm::cross(v3, v2));
            }

            if (glm::length(v3) > 0.0f && glm::length(v4) > 0.0f)
            {
                n34 = glm::normalize(glm::cross(v4, v3));
            }

            if (glm::length(v4) > 0.0f && glm::length(v1) > 0.0f)
            {
                n41 = glm::normalize(glm::cross(v1, v4));
            }

            glm::vec3 normal = glm::normalize(n12 + n23 + n34 + n41);
            mesh.normals.push_back(normal);
        }
    }
}

float Terrain::get_height(float x, float z)
{//TODO mozne rozsireni pro reseni kolizi s terenem
    (void)x;
    (void)z;
    //return actual height
    return 0.0f;
}
