/**
 * @author Martin Prajka
 * @file   sparkle.cpp
 */
#include "sparkle.h"
#include "../window/glcheck_function.h"

Sparkle::~Sparkle()
{
	glDeleteBuffers(1,&vbo);
	glDeleteTextures(1,&a_tex);
	glDeleteVertexArrays(1,&vao);

	glUseProgram(0);
	glDeleteProgram(program);
}

void Sparkle::init()
{
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	const float mesh[] =
	{
        -1.0,1.0,0.0,0.0,1.0,
        1.0,1.0,0.0,1.0,1.0,
        1.0,-1.0,0.0,1.0,0.0,
        -1.0,-1.0,0.0,0.0,0.0,
        -1.0,1.0,0.0,0.0,1.0,
        1.0,-1.0,0.0,1.0,0.0
	};

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER,vbo);
	glBufferData(GL_ARRAY_BUFFER,sizeof(mesh),mesh,GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(float)*5,0);

	glEnableVertexAttribArray(1);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(float)*5,(void *)(sizeof(float)*3));

	a_tex = Texture::create_texture("./textures/alpha_texture.png");
	//nastaveni shaderu a programu
	program = Shader::create_program("./shaders/sparkle_vert.glsl","./shaders/sparkle_frag.glsl");
	GLCheck::checkGLerror(HERE);
}

void Sparkle::draw(glm::mat4 &perspective, glm::mat4 &view, glm::vec3 &sun_position)
{
	
	glBindVertexArray(vao);
	glUseProgram(program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,a_tex);


	glm::mat4 full_trans = view * glm::translate(sun_position);
	GLuint location = glGetUniformLocation(program,"perspective");
	glUniformMatrix4fv(location,1,GL_FALSE,&perspective[0][0]);
	location = glGetUniformLocation(program,"view");
	glUniformMatrix4fv(location,1,GL_FALSE,&full_trans[0][0]);
	location = glGetUniformLocation(program,"scale_r");
	glUniform1f(location,732.0f);
	
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glDrawArrays(GL_TRIANGLES,0,6);
	glDisable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	GLCheck::checkGLerror(HERE);
}
