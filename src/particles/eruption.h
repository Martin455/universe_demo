/**
 * @author Martin Prajka
 * @file   eruption.h
 */
#ifndef ERUPTION_H
#define ERUPTION_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <vector>
#include <array>
#include <cmath>
#include <cstdlib>
#include <algorithm>

#include "../window/object_properties.h"
#include "../loader/mesh.h"
#include "../audio/audio_manager.h"
#include "../constants.h"

class Eruption
{
    GLuint vao;
    GLuint static_buffer;
    GLuint instance_buffer;
    GLuint texture;

    //hack for animation
    bool animation;

    const int PARTICLES = 750;

    glm::vec3 start_position;
    float phi;
    float var_phi;

    glm::mat4 normal_rotate;

    struct particle_data{
        glm::vec3 position;
        float radius;
        float angle;
        float speed;
        bool active;
    };

    std::vector<particle_data> parts;
    std::vector<GLfloat> gpu_buffer;
    static float RandRange(float min, float max)
    {
        float r = float(rand()/(double)RAND_MAX);
        return min + r * (max - min);
    }

    void init_parts();
    bool checkActive();

    void update(float frame_time);
    void draw_parts();
public:
    Eruption() = default;
    virtual ~Eruption();

    void init(const char *texture_name, bool anim = false);
    void draw(float frame_time, bool loop = true);
};

#endif // ERUPTION_H
