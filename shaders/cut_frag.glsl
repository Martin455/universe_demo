#version 330 core

in vec2 uv_coords;
out vec4 out_color;

uniform sampler2D tex;
uniform float value; // 0 for dark, 1 original color

void main()
{
    vec3 scene_texel = texture(tex, uv_coords).rgb;
    vec3 final_color = mix(vec3(0.0,0.0,0.0), scene_texel.rgb, value);
    out_color = vec4(final_color, 1.0);
}
