#version 330 core

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv;
layout(location=2) in vec3 normal;

out vec2 uv_coords;
out vec3 normals;
out vec3 pos;

uniform mat4 perspective;
uniform mat4 view;
uniform mat4 model;

void main()
{
    uv_coords = uv;

    pos = vec3(model * vec4(position, 1.0));
    normals = normalize(vec3(model * vec4(normal,0.0)));
    gl_Position = perspective * view * model * vec4(position, 1.0);
}