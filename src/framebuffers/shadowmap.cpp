/**
 * @file shadowmap.cpp
 * @author Martin Prajka
 */
#include "shadowmap.h"
#include "../window/glcheck_function.h"

#include <iostream>

Shadowmap::~Shadowmap()
{
    glDeleteTextures(1, &depth_texture);
    glDeleteFramebuffers(1, &fbo);
}

void Shadowmap::init(int width, int height)
{
    this->width = width;
    this->height = height;

    old_height = 0;
    old_width = 0;

    glGenFramebuffers(1, &fbo);

    glGenTextures(1, &depth_texture);
    glBindTexture(GL_TEXTURE_2D, depth_texture);


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 
                 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_texture, 0);
    GLCheck::checkGLFBstatus(HERE);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Shadowmap::bind(int actually_width, int actually_height)
{
    old_width = actually_width;
    old_height = actually_height;

    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void Shadowmap::unbind(GLuint framebuffer)
{
    glViewport(0, 0, old_width, old_height);    
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
}

GLuint Shadowmap::get_depth_texture()
{
    return depth_texture;
}

void Shadowmap::draw_quad()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depth_texture);
    

    quad.draw();
}

glm::mat4 Shadowmap::get_light_projection()
{
    //static projection for specific scene
    return glm::ortho(-119.0f,202.0f,-75.0f,150.0f, 2014.0f,2590.0f);
}

glm::mat4 Shadowmap::get_light_view(glm::vec3 &light_position)
{
    return glm::lookAt(light_position, glm::vec3(-10.0f, 0.0f, -15.5), glm::vec3(0.0f,1.0f,0.0f));
}
