/**
 * @author Martin Prajka
 * @file g_object.h
 */
#ifndef G_OBJECT_H
#define G_OBJECT_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include "../window/object_properties.h"
#include "../loader/mesh.h"

/**
 * @brief The G_object class the base class for many objects
 */
class G_object
{
protected:
	GLuint vao;
	GLuint textureID;

	//buffery
	GLuint vbo;
	GLuint uvbo;
	GLuint nbo;

	unsigned num_of_vertex;
public:
    G_object() = default;
	virtual ~G_object();
    /**
     * @brief init graphics object from mesh
     * @param mesh mesh input
     * @param texture_name texture file name
     */
	void init(Mesh &mesh, const char* texture_name);

    /**
     * @brief update
     * @param viewMatrix
     * @param model_mat
     * @param light
     * @param shader
     */
    void update(glm::mat4 &viewMatrix,glm::mat4 &model_mat,glm::vec3 &light, GLuint shader);
	void draw();
};

#endif
