#version 330 core

in vec2 uv_coords;

out vec4 color;

uniform sampler2D tex;

void main()
{
	color = texture(tex, uv_coords);
    //debug
    color = vec4(0.5,0.5,0.5, 1.0);
}
