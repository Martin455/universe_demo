/**
 * @author Martin Prajka
 * @file   sun.h
 */
#ifndef SUN_H
#define SUN_H

#include "g_object.h"
#include "sparkle.h"
#include "../particles/eruption.h"
#include "../constants.h"
#include <array>

class Sun: public G_object
{
	Sparkle sparkle;
	GLuint sparkle_vao;
	GLuint sparkl_tex;

    std::array<Eruption, 6> eruptions;
    Eruption animate_eruption;
    Shader eruption_shader;

	GLuint sparkle_buff[2];
public:
	void init(Mesh &mesh);
	void draw(glm::mat4 projMat,glm::mat4 viewMat,double time,glm::vec3 &light, float frame_time, GLuint shader);
	~Sun();
};

#endif
