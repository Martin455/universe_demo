/**
 * @author Martin Prajka
 * @file   water_tank.h
 */
#ifndef WATER_TANK_H
#define WATER_TANK_H

#include "g_object.h"
#include "../constants.h"

class Water_tank: public G_object
{
    glm::mat4 model_matrix;
public:
    Water_tank() = default;

    void init(Mesh &mesh, const char *texture_name);
    void prepare_depth(GLuint depth_shader);
    void update(Shader &shader, GLuint shadowmap);
	void draw();
};

#endif
