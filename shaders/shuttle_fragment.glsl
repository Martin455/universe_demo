#version 330 core

in vec2 uv_coords;
in vec3 pos;
in vec3 normals;

out vec4 color;

uniform sampler2D tex;

uniform vec3 ambientColor;
uniform vec3 diffuzeColor;

float ambientStrength = 0.3;

vec3 positionLight = vec3(-11.46,12.0,-44.4);
vec3 positionLight2 = vec3(13.1,12.0,-44.4);

void main()
{
    vec4 objectColor = texture(tex, uv_coords);

    //ambient lighting
    vec3 ambient = ambientColor * ambientStrength;

    //diffuze 1
    vec3 lightDirection = normalize(positionLight - pos);
    float diffuzeIntenzity = dot(lightDirection, normals);
    vec3 diffuze1 = diffuzeColor * diffuzeIntenzity;

    //diffuze 2
    lightDirection = normalize(positionLight2 - pos);
    diffuzeIntenzity = abs(dot(lightDirection, normals));
    vec3 diffuze2 = diffuzeColor * diffuzeIntenzity;

    vec3 lightIntenzity = ambient + diffuze1 + diffuze2;

    color = vec4(objectColor.rgb * lightIntenzity, objectColor.a);

    //color = vec4(abs(normals), 1.0);
}