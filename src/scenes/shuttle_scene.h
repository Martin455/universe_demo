/**
 * @author Martin Prajka
 * @file   shuttle_scene.h
 */
#ifndef SHUTTLE_SCENE_H
#define SHUTTLE_SCENE_H

#include <cstdlib>
#include "scene.h"
#include "../objects/tower.h"
#include "../objects/shuttle.h"
#include "../objects/building.h"
#include "../objects/satelite.h"
#include "../objects/rocket_tank.h"
#include "../objects/skybox.h"
#include "../objects/water_tank.h"
#include "../objects/terrain.h"
#include "../framebuffers/antialias_fb.h"
#include "../framebuffers/framebuffer.h"
#include "../framebuffers/shadowmap.h"
#include "../framebuffers/bloom_fb.h"
#include "../window/object_properties.h"

class Shuttle_scene: public Scene
{
	Tower tower;
	Shuttle shuttle;
	Rocket_tank rocket_tank;
	Building space_office;

    Terrain terrain;

	//vector of satelites
	std::vector<Satelite*> satelites;

    Shader obj_shader;
    Shader skybox_shader;
    Shader depth_shader;

    Shader debug_shadow;
    Shader default_shader;

	//skybox
	Skybox skybox;

	Water_tank water_tank;

	glm::vec3 light;
	bool end_scene;
	bool cheat_active;
    bool bridge_move;
	glm::vec3 new_camera_position;

	Antialiasing multisampling;
    Shadowmap shadowmapping;
    glm::mat4 bias_matrix;
    Bloom_effect bloom;
    Framebuffer cut_framebuffer;

    void set_animation();
protected:
	virtual void painting();
	virtual void key_event();
    virtual void mouse_event();
public:
	Shuttle_scene() = default;
    virtual void init(GLFWwindow *win,Animation_camera *cam, Basic_window_function *bwf);
	virtual void draw();
    virtual void end();
};

#endif
