/**
 * @author Martin Prajka
 * @file building.h
 */
#ifndef BUILDING_H
#define BUILDING_H

#include "g_object.h"
#include "../constants.h"
#include "satelite.h"

class Building: public G_object
{
	Satelite satelite;
    glm::mat4 model_matrix;
public:
	Building() {}

	void init(Mesh &mesh, const char* texture_name, Mesh &satelite_mesh);

    void prepare_depth_drawing(GLuint depth_shader);
    void update(Shader &shader, GLuint shadowmap);
    void draw(Shader &shader, GLuint shadowmap);
};

#endif
