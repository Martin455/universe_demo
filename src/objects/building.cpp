/**
 * @author Martin Prajka
 * @file building.cpp
 */
#include "building.h"
#include "../window/glcheck_function.h"

void Building::init(Mesh &mesh, const char *texture_name, Mesh &satelite_mesh)
{
	G_object::init(mesh, texture_name);

	satelite.init(satelite_mesh, "./textures/satelite_texture.png");
    satelite.set_position(glm::vec3(48.0f,30.9f,140));
	satelite.set_rotation(45.0f);

    model_matrix = glm::translate(glm::vec3(80.0f,-2.0f,150.0f))
            * glm::scale(glm::vec3(3.35,3.35,3.35))
            * glm::rotate(-deg_to_rad(140.0f), glm::vec3(0,1,0));
}

void Building::prepare_depth_drawing(GLuint depth_shader)
{
    GLuint location = glGetUniformLocation(depth_shader, "modelMatrix");
    glUniformMatrix4fv(location, 1, GL_FALSE, &model_matrix[0][0]);

    glDisable(GL_CULL_FACE);
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES,0,num_of_vertex);
    glEnable(GL_CULL_FACE);

    satelite.prepare_depth(depth_shader);
    satelite.draw();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    GLCheck::checkGLerror(HERE);       
}

void Building::update(Shader &shader, GLuint shadowmap)
{
    shader.set_mat4("model_matrix", model_matrix);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, shadowmap);

    GLCheck::checkGLerror(HERE);
}

void Building::draw(Shader &shader, GLuint shadowmap)
{
	glBindVertexArray(vao);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    satelite.update(shader, shadowmap);
	satelite.draw();
    
	GLCheck::checkGLerror(HERE);
}
