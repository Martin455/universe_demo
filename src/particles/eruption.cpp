/**
 * @author Martin Prajka
 * @file   eruption.cpp
 */
#include "eruption.h"
#include "../window/glcheck_function.h"

Eruption::~Eruption()
{
    glDeleteBuffers(1, &static_buffer);
    glDeleteBuffers(1, &instance_buffer);
    glDeleteTextures(1, &texture);
    glDeleteVertexArrays(1, &vao);
}

void Eruption::init(const char *texture_name, bool anim)
{
    animation = anim;
    GLfloat mesh[] =
    {
        -1.f,0.5f,0,1,
        -1.f,-0.5f,0,0,
        1.f,-0.5f,1,0,
        1.f,0.5f,1,1,
        -1.f,0.5f,0,1,
        1.f,-0.5f,1,0
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &static_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, static_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mesh), mesh, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4,0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                          sizeof(float)*4,(void *)(sizeof(float)* 2));

    glGenBuffers(1, &instance_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, instance_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * PARTICLES, nullptr, GL_STREAM_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3,0);

    glVertexAttribDivisor(0,0);
    glVertexAttribDivisor(1,0);
    glVertexAttribDivisor(2,1);

    texture = Texture::create_texture(texture_name);


    gpu_buffer.reserve(3 * PARTICLES);
    init_parts();
    GLCheck::checkGLerror(HERE);
}

void Eruption::draw(float frame_time, bool loop)
{
    if (!checkActive())
    {
        if (loop)
            init_parts();
        else
            return;
    }

    update(frame_time);

    draw_parts();
}


void Eruption::init_parts()
{
    parts.clear();

    if (animation)
    {
        phi = M_PI;
        var_phi = M_PI;

    }
    else
    {
        phi = RandRange(0.0f, float(M_PI));
        var_phi = RandRange(0.0f, float(M_PI) * 2.f);

    }

    start_position.z = (SUN_ACROSS * 2.0f - 0.7f) * sin(phi)*cos(var_phi);
    start_position.x = (SUN_ACROSS * 2.0f - 0.7f) * sin(phi)*sin(var_phi);
    start_position.y = (SUN_ACROSS * 2.0f - 0.7f) * cos(phi);

    float var_minus;
    float phi_minus;
    if (var_phi<(M_PI+0.01))
        var_minus = M_PI;
    else
        var_minus = 2*M_PI;
    if (phi<(M_PI/2.0 + 0.01))
        phi_minus = M_PI/2.0;
    else
        phi_minus = M_PI;

    normal_rotate = glm::rotate(float((var_minus) - var_phi),glm::vec3(0.0f,0.0f,1.0f))
                    * glm::rotate(float(phi_minus - phi), glm::vec3(1.0f,0.0f,0.0f));

    for (int i = 0; i < PARTICLES; ++i)
    {
        particle_data data;
        data.position = start_position;
        data.radius = RandRange(20.4f, 32.5f);
        data.angle = 360.0f;
		data.speed = float(RandRange(10.0f, 40.0f) + (i % 30));
        data.active = true;

        parts.push_back(data);
    }
}

bool Eruption::checkActive()
{
    for (auto x = parts.begin(); x != parts.end(); ++x)
    {
        if (x->active)
            return true;
    }

    return false;
}

void Eruption::update(float frame_time)
{
    gpu_buffer.clear();
    for (auto x = parts.begin(); x!=parts.end(); ++x)
    {
        if (x->active)
        {
            glm::vec3 translate_vector = glm::vec3(float(std::cos((x->angle/180.0f)*M_PI) * x->radius),
                                                   float(std::sin((x->angle/180.0f)*M_PI) * x->radius),
                                                   0.0f);

            x->position = glm::vec3(glm::translate(SUN_POSITION) * glm::translate(start_position) *
                                    normal_rotate * glm::vec4(translate_vector, 1.0f));

            gpu_buffer.push_back(x->position.x);
            gpu_buffer.push_back(x->position.y);
            gpu_buffer.push_back(x->position.z);

            x->angle -= (x->speed * frame_time);
            x->radius -= frame_time;

            if (x->angle < 0.0f || x->radius < 0.0f)
                x->active = false;
        }
    }
}

void Eruption::draw_parts()
{
    glBindVertexArray(vao);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glBindBuffer(GL_ARRAY_BUFFER, instance_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * PARTICLES, nullptr, GL_STREAM_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * PARTICLES, gpu_buffer.data(), GL_STREAM_DRAW);

    glDrawArraysInstanced(GL_TRIANGLES, 0, 6, gpu_buffer.size() / 3);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    GLCheck::checkGLerror(HERE);
}
