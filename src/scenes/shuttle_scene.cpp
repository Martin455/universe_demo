/**
 * @author Martin Prajka
 * @file   shuttle_scene.cpp
 */
#include "shuttle_scene.h"
#include "../window/glcheck_function.h"

void Shuttle_scene::init(GLFWwindow *win, Animation_camera *cam, Basic_window_function *bwf)
{
	//setting scene
	window = win;
	camera = cam;
	basic_function = bwf;

#ifndef NDEBUG
    animation = true;
#endif

	Loader loader;
    Mesh tower_mesh = loader.load_object("./models/tower.obj");
	Mesh shuttle_mesh = loader.load_object("./models/shuttle.obj");
	Mesh satelite_mesh = loader.load_object("./models/satelite.obj");
	Mesh rocket_tank_mesh = loader.load_object("./models/rocket_tank.obj");
	Mesh skybox_sphere_mesh = loader.load_object("./models/skybox_sphere.obj");
	Mesh water_tank_mesh = loader.load_object("./models/water_tank.obj");
	Mesh space_office_mesh = loader.load_object("./models/building.obj");
	Mesh bridge_mesh = loader.load_object("./models/rocket_bridge.obj");

    obj_shader.set_shader("./shaders/earth_objects_vertex.glsl","./shaders/earth_objects_fragment.glsl");
    skybox_shader.set_shader("./shaders/skybox_vertex.glsl","./shaders/skybox_fragment.glsl");
    depth_shader.set_shader("./shaders/depth_shader_vertex.glsl", "./shaders/depth_shader_fragment.glsl");

    debug_shadow.set_shader("./shaders/framebuffer_default_vertex.glsl", "./shaders/debug_draw_shadows.glsl");
    default_shader.set_shader("./shaders/default_vertex.glsl", "./shaders/default_fragment.glsl");

    terrain.init("./textures/heightmap.png", 2600, 256);
    terrain.set_texture("./textures/mountains_texture.png");

	shuttle.init(shuttle_mesh, "./textures/shuttle_texture.png");

	rocket_tank.init(rocket_tank_mesh, "./textures/rocket_tank_texture.png");

	tower.init(tower_mesh, "./textures/tower_texture.png", satelite_mesh, bridge_mesh);

	space_office.init(space_office_mesh, "./textures/building_texture.png", satelite_mesh);

	water_tank.init(water_tank_mesh, "./textures/water_tank_texture.png");

	skybox.init(skybox_sphere_mesh, "./textures/desert_skybox.png");

	glm::vec3 position_vector = glm::vec3(-155.0f,-2.5f,15.0f);
	for(int i = 0; i < 12; ++i)
	{
		if (i != 0 && i%3==0)
		{
			position_vector.z += 40.0f;
			position_vector.x = -155.0f;
		}
		satelites.push_back(new Satelite(position_vector));
		satelites.back()->init(satelite_mesh, "./textures/satelite_texture.png");
		satelites.back()->set_rotation(rand()/float(RAND_MAX) * 360.0f);

		position_vector.x += 23;
	}

	light = glm::vec3(-1000,777,1800);

    //camera position
    new_camera_position = glm::vec3(-96.7f,3.0f,262.9f);
	camera->set_position(new_camera_position);

	end_scene = false;
	cheat_active = false;
    bridge_move = false;

	//init multisampling buffer
	int height, width;
	glfwGetWindowSize(window,&width,&height);
    camera->set_far_plane(2000.0f);
    camera->setProjectionMat(width, height);

    multisampling.init(width, height, basic_function->get_msaa(), basic_function->get_ssaa());

    //init shadowmapping
    shadowmapping.init(1024, 1024);
    bias_matrix = glm::mat4(
                0.5, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.5, 0.0,
                0.5, 0.5, 0.5, 1.0
                );

    //bloom init
    bloom.init(width, height, 12);
    bloom.set_shaders("./shaders/framebuffer_default_vertex.glsl", "./shaders/framebuffer_default_fragment.glsl");
    bloom.set_blur_shader("./shaders/framebuffer_default_vertex.glsl", "./shaders/blur_fragment.glsl");
    bloom.set_prepare_shader("./shaders/framebuffer_default_vertex.glsl","./shaders/prepare_bloom_fragment.glsl", glm::vec3(0.2126, 0.7152, 0.0722) * 1.025f);
    bloom.set_bloom_shader("./shaders/framebuffer_default_vertex.glsl", "./shaders/bloom_fragment.glsl");

    cut_framebuffer.init(width, height);
    cut_framebuffer.set_shaders("./shaders/framebuffer_default_vertex.glsl", "./shaders/cut_frag.glsl");

    GLCheck::checkGLerror(HERE);

    basic_function->init_frame_time();

    Audio_manager::instance()->create_sound("birds", "./sounds/birds_sing.ogg");
}

void Shuttle_scene::draw()
{
	if (!window)
		return;
    set_animation();
    Audio_manager::instance()->play_sound("birds", true);
	
	start_scene_time = glfwGetTime();	
	do
	{	
		scene_time = glfwGetTime() - start_scene_time;
		key_event();
#ifndef NDEBUG
        if (animation)
        {
            camera->animate_camera(scene_time);
        }
        else
        {
            mouse_event();
        }

        basic_function->fps_counter(scene_time);
#else
        camera->animate_camera(scene_time);
#endif
        painting();
        basic_function->frame_time_counter();

    	glfwSwapBuffers(window);
    	glfwPollEvents();
	}
	while(glfwWindowShouldClose(window)==0 && !end_scene);
    camera->clear_animation();
    for(auto x = satelites.begin(); x != satelites.end(); ++x)
	{
		delete (*x);
	}
    if (Audio_manager::instance()->playing_sound("birds"))
    {
        Audio_manager::instance()->stop_sound("birds");
        Audio_manager::instance()->delete_sound("birds");
    }
}

void Shuttle_scene::set_animation()
{
	//move animation
	glm::vec3 start_position = camera->get_position();
	camera->move_cubic_bezier(start_position, glm::vec3(-64.77f, 5.0f, 131.48f), glm::vec3(34.7f, 5.0f, 191.6f), glm::vec3(-32.4f, 5.0f, 171.57f), 0.0, 8.5);
	camera->move_cubic_bezier(glm::vec3(-64.77f, 5.0f, 131.48f), glm::vec3(-83.67f, 6.0f, -27.76f), glm::vec3(-104.1f, 6.0f, 84.05f), glm::vec3(-61.69f, 6.0f, 10.4f), 8.5, 14.5);
	camera->move_cubic_bezier(glm::vec3(-83.67f, 6.0f, -27.76f), glm::vec3(-162.32f, 10.0f, -116.05f), glm::vec3(-112.97f, 8.0f, -79.42f), glm::vec3(-157.31f, 10.0f, 73.26f), 14.5, 19.5);
	camera->move_cubic_bezier(glm::vec3(-162.32f, 10.0f, -116.05f), glm::vec3(-123.0f, 14.0f, -204.35f), glm::vec3(-169.65f, 12.0f, -170.8f), glm::vec3(-155.38f, 14.0f, -185.84f), 19.5, 22.5);
	camera->move_cubic_bezier(glm::vec3(-123.0f, 14.0f, -204.35f), glm::vec3(74.03f, 18.0f, -213.21f), glm::vec3(-84.05f, 16.0f, -226.71f), glm::vec3(34.7f, 18.0f, -229.41f), 22.5, 30.5);
	camera->move_cubic_bezier(glm::vec3(74.03f, 18.0f, -213.21f), glm::vec3(132.82f, 8.0f, -144.97f), glm::vec3(116.82f, 14.0f, -196.63f), glm::vec3(122.6f, 10.0f, -191.6f), 30.5, 35.5);
	camera->move_cubic_bezier(glm::vec3(132.82f, 8.0f, -144.97f), glm::vec3(108.7f, 5.0f, -3.5f), glm::vec3(147.67f, 7.0f, -82.51f), glm::vec3(129.55f, 5.0f, -22.f), 35.5, 42.5);
	camera->move_cubic_bezier(glm::vec3(108.7f, 5.0f, -3.5f), glm::vec3(3.1f, 3.0f, -12.34f), glm::vec3(92.92f, 4.0f, 10.8f), glm::vec3(27.37f, 3.0f, -12.34f), 42.5, 47.5);
	camera->move_cubic_bezier(glm::vec3(3.1f, 3.0f, -12.34f), glm::vec3(-2.0f, 0.0f, -55.5f), glm::vec3(-12.36f, 1.0f, -7.8f), glm::vec3(-8.87f, 0.0f, -56.3f), 47.5, 50.5);
	camera->move_to_point(glm::vec3(-2.0f, 0.0f, -55.5f), glm::vec3(-0.5, 0.0f, -58.2f), 50.5, 51.5);

	//bridge
	camera->move_to_point(glm::vec3(-0.5, 0.0f, -58.2f), glm::vec3(14.22f, 76.5f, -75.0f), 51.5, 51.51);
	camera->move_to_point(glm::vec3(14.22f, 76.5f, -75.0f), glm::vec3(37.4f, 76.5f, -75.0f), 51.51, 56.01);

	//look animation
	camera->take_look_around(45.0f, glm::vec2(0.0f, -1.0f), 0.0, 1.0);

	camera->take_look_around(180.0f, glm::vec2(0.12f, 1.0f), 2.0, 5.5);

	camera->take_look_around(60.0f, glm::vec2(0.35f, -1.0f), 6.3, 8.3);
	camera->take_look_around(75.0f, glm::vec2(0.0f, 1.0f), 8.3, 11.8);

	camera->take_look_around(120.0f, glm::vec2(-0.08f, 1.0f), 14.8, 18.8);
	camera->look_at_point(glm::vec3(-30.0f, 45.0f, -80.5f), 18.8, 32.3);

	camera->take_look_around(86.0f, glm::vec2(0.0f, 1.0f), 32.3, 37.3);
	camera->take_look_around(170.0f, glm::vec2(0.12f, -1.0f), 37.5, 40.8);
	camera->take_look_around(52.0f, glm::vec2(1.0f, -0.4f), 40.8, 42.8);
	camera->look_at_point(glm::vec3(40.0f, 77.8f, -80.0f), 42.8, 46.8);
	camera->take_look_around(60.0f, glm::vec2(-1.0f, 0.5f), 46.8, 49.0);
	camera->take_look_around(20.0f, glm::vec2(0.0f, -1.0f), 49.0, 49.8);


	camera->look_at_point(glm::vec3(40.0f, 77.8f, -75.0f), 51.62, 52.42);
	camera->take_look_around(120.0, glm::vec2(0.2f, -1.0f), 52.42, 53.92);

	camera->take_look_around(120.0, glm::vec2(-0.2f, 1.0f), 54.42, 55.02);

	//cuts
	camera->set_cut(50.0, 51.0);
	camera->set_cut(52.5, 53.5, false);
	camera->set_cut(55.2, 56.01);
}

void Shuttle_scene::painting()
{
    //create shadow texture
    glCullFace(GL_FRONT);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(2.5f, 10.0f);
    glm::mat4 light_proj = shadowmapping.get_light_projection();
    glm::mat4 light_view = shadowmapping.get_light_view(light);
    glm::mat4 light_space = light_proj * light_view;

    depth_shader.bind();
    GLuint depth_shader_id = depth_shader.get_program_id();
    GLuint light_location = glGetUniformLocation(depth_shader_id, "lightSpaceMatrix");
    glUniformMatrix4fv(light_location, 1, GL_FALSE, &light_space[0][0]);

    int height, width;
    glfwGetWindowSize(window,&width,&height);

    shadowmapping.bind(width, height);

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    shuttle.prepare_depth(depth_shader_id);
    shuttle.draw();

    rocket_tank.prepare_depth(depth_shader_id);
    rocket_tank.draw();
    
    //Exception in preparing draw multiple objects
    space_office.prepare_depth_drawing(depth_shader_id);
    
    //tower
    tower.prepare_depth_drawing(depth_shader_id);
    
    //water tank
    water_tank.prepare_depth(depth_shader_id);
    water_tank.draw();
    
    //satelites
    for (auto x = satelites.begin(); x != satelites.end(); ++x)
    {
        (*x)->prepare_depth(depth_shader_id);
        (*x)->draw();
    }

    shadowmapping.unbind(0);
    depth_shader.unbind();

    glDisable(GL_POLYGON_OFFSET_FILL);
    glCullFace(GL_BACK);
    GLCheck::checkGLerror(HERE);

    //GET shadow texture
    GLuint shadowmap = shadowmapping.get_depth_texture();

    multisampling.bind_fbo();

    glClearColor(0.0f,0.92f,1.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 projection = camera->getProjectionMat();
	glm::mat4 view = camera->getViewMatrix();
	glm::mat4 viewMatrix = projection * view;
	glm::vec3 camera_position = camera->get_position();
	glm::mat4 skybox_model = viewMatrix * glm::translate(camera_position)
		* glm::rotate(deg_to_rad(120), glm::vec3(0,1,0));

    skybox_shader.bind();
    GLuint shader = skybox_shader.get_program_id();
	skybox.draw(skybox_model, shader);
    skybox_shader.unbind();

    obj_shader.bind();
    //set share variables
    obj_shader.set_int("tex", 0);
    obj_shader.set_int("shadow_map", 1);

    obj_shader.set_mat4("projection", projection);
    obj_shader.set_mat4("view", view);

    obj_shader.set_vec3("cam_pos", camera_position);
    obj_shader.set_vec3("light_position", light);

    obj_shader.set_mat4("light_space_matrix", light_space);
    obj_shader.set_mat4("bias", bias_matrix);

    terrain.update(obj_shader, shadowmap);
    terrain.draw_terrain();

    shuttle.update(obj_shader, shadowmap);
    shuttle.draw();

    rocket_tank.update(obj_shader, shadowmap);
    rocket_tank.draw();
    
    //Exception in drawing multiple objects
    space_office.update(obj_shader, shadowmap);
    space_office.draw(obj_shader, shadowmap);

	//satelites
	for (auto x = satelites.begin(); x != satelites.end(); ++x)
	{
		(*x)->update(obj_shader, shadowmap);
		(*x)->draw();
	}

	//water tank
	water_tank.update(obj_shader, shadowmap);
	water_tank.draw();

    //tower
    tower.draw(projection, view, float(scene_time), obj_shader, shadowmap);
    
    obj_shader.unbind();

    //Postprocessing
	//apply multisample and blit to zero buffer
    multisampling.apply(bloom.get_framebuffer(), bloom.get_width(), bloom.get_height());

    bloom.unbind_fbo(0, basic_function->width, basic_function->height);

    bloom.active_bloom();
    bloom.unbind_fbo(cut_framebuffer.get_framebuffer(), basic_function->width, basic_function->height);

    bloom.draw_bloom();

    cut_framebuffer.unbind_fbo(0, basic_function->width, basic_function->height);
    cut_framebuffer.binding_draw();
    GLuint location = glGetUniformLocation(cut_framebuffer.get_shader_id(), "value");
    glUniform1f(location, camera->get_transition());
    cut_framebuffer.draw_quad();

	GLCheck::checkGLerror(HERE);

    if (glm::length(camera_position - glm::vec3(37.4f, 76.5f,-75.0f)) < 0.001f)
    {
        end_scene = true;
    }
}

void Shuttle_scene::key_event()
{
#ifdef NDEBUG
	if(glfwGetKey(window,GLFW_KEY_F) == GLFW_PRESS)
	{
		basic_function->fps_counter(scene_time);    //print fps to stdout
	}
#else
    if (!animation)
    {
        if (glfwGetKey(window,GLFW_KEY_W) == GLFW_PRESS)
        {
            camera->move_forward(basic_function->get_frametime());
        }
        if (glfwGetKey(window,GLFW_KEY_S) == GLFW_PRESS)
        {
            camera->move_backward(basic_function->get_frametime());
        }
        if (glfwGetKey(window,GLFW_KEY_A) == GLFW_PRESS)
        {
            camera->move_left(basic_function->get_frametime());
        }
        if (glfwGetKey(window,GLFW_KEY_D) == GLFW_PRESS)
        {
            camera->move_right(basic_function->get_frametime());
        }
        if(glfwGetKey(window,GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
        {
            camera->deactive_sprint();
        }
        if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_RELEASE)
        {
            camera->deactive_sprint();
        }
        if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        {
            camera->super_sprint();
        }
        if(glfwGetKey(window,GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        {
            camera->active_sprint();
        }
    }
#endif
}

void Shuttle_scene::mouse_event()
{
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    camera->mouseMove(glm::vec2(xpos, ypos));
}

void Shuttle_scene::end()
{
    end_scene = true;
}
