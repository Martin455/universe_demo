/**
 * @author Martin Prajka
 * @file   audio_manager.h
 * @brief  file contains declaration of Audio_manager class
 */
#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#include <SFML/Audio.hpp>
#include <glm/glm.hpp>
#include <map>
#include <string>


/**
 * @brief The Audio_manager class manage each sound or music in application
 * @details This class is separeted from project and can be reuse in another project.
 *          The class implements singleton design pattern
 */
class Audio_manager
{
    //pair for sounds
    struct Sound_n_buffer
    {
      sf::Sound *sound;
      sf::SoundBuffer *buffer;
    };
    //every sound or music has to have original name
    std::map<std::string, sf::Music *> music_list;
    std::map<std::string, Sound_n_buffer> sound_list;

	Audio_manager() = default;
    ~Audio_manager();

    //class instance
    static Audio_manager *instance_am;
public:

    /**
     * @brief alloc memory for instance Audio_manager
     */
    static void init();

    /**
     * @brief get pointer to Audio_manager if it does not exist alloc new one
     * @return pointer to Audio_manager
     */
    static Audio_manager *instance();

    /**
     * @brief free memory
     */
    static void dispose();

    //return true if it's OK false if it's failed
    //music
    bool create_music(std::string name, std::string filename);

    bool find_music(std::string name);

    void play_music(std::string name, bool loop = false);
    void set_music_volume(std::string name, float volume);
    void pause_music(std::string name);
    bool playing_music(std::string name);
    void stop_music(std::string name);
    void delete_music(std::string name);

    //sound
    bool create_sound(std::string name, std::string filename);

    bool find_sound(std::string name);

    void play_sound(std::string name, bool loop = false);
    void set_sound_volume(std::string name, float value);
    void set_sound_position(std::string name, glm::vec3 position, bool relative_to_listener = false);
    void set_min_distance(std::string name, float min_distance, float attenuation = -1.0f);
    bool playing_sound(std::string name);
    void pause_sound(std::string name);
    void stop_sound(std::string name);
    void delete_sound(std::string name);

    glm::vec3 get_sound_position(std::string name);

    //listener
    void set_listener(glm::vec3 position, glm::vec3 direction);
    void set_listener_position(glm::vec3 position);
    void set_listener_direction(glm::vec3 direction);

    glm::vec3 get_listener_position() const;
    glm::vec3 get_listener_direction() const;
};

#endif // AUDIO_MANAGER_H
