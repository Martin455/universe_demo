#version 330 core

in vec2 uv_coords;
out vec4 out_color;

uniform sampler2D tex;
uniform sampler2D tex1;

void main()
{
 	vec3 scene_texel = texture(tex, uv_coords).rgb;
  	out_color = vec4(scene_texel, 1.0);
}