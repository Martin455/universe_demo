/**
 * @author Martin Prajka
 * @file g_object.cpp
 */
#include "g_object.h"
#include <stdexcept>
#include <exception>
#include "../window/glcheck_function.h"

G_object::~G_object()
{
	glDeleteBuffers(1,&vbo);
	glDeleteBuffers(1,&uvbo);
	glDeleteBuffers(1,&nbo);
	glDeleteTextures(1,&textureID);
	glDeleteVertexArrays(1,&vao);
}

void G_object::init(Mesh &mesh, const char* texture_name)
{
	num_of_vertex = mesh.vertices.size();
	//nastaveni vertex objectu
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);
	//nasteveni vertexu
	glGenBuffers(1,&vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER,mesh.get_vertices_byte(),&mesh.vertices[0],GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,0);
	//nastaveni UV souradnic
	glGenBuffers(1,&uvbo);
	glBindBuffer(GL_ARRAY_BUFFER,uvbo);
	glBufferData(GL_ARRAY_BUFFER,mesh.get_texture_uv_byte(),&mesh.texture_uv[0],GL_STATIC_DRAW);	
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(float)*2,0);

	//nastaveni normal
	glGenBuffers(1,&nbo);
	glBindBuffer(GL_ARRAY_BUFFER,nbo);
	glBufferData(GL_ARRAY_BUFFER,mesh.get_normals_byte(),&mesh.normals[0],GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,0);

	//nastaveni textury
	textureID = Texture::create_texture(texture_name);

	GLCheck::checkGLerror(HERE);
}

void G_object::update(glm::mat4 &viewMatrix,glm::mat4 &model_mat,glm::vec3 &light, GLuint shader)
{
    glm::mat4 full_trans = viewMatrix * model_mat;
    
    GLuint uni_location = glGetUniformLocation(shader,"fulltrans_matrix");
    glUniformMatrix4fv(uni_location,1,GL_FALSE,&full_trans[0][0]);

    GLuint light_location = glGetUniformLocation(shader, "light_position");
    glUniform3fv(light_location,1,&light[0]);

    GLuint model_location = glGetUniformLocation(shader,"model_matrix");
    glUniformMatrix4fv(model_location,1,GL_FALSE,&model_mat[0][0]);

    GLCheck::checkGLerror(HERE);
}

void G_object::draw()
{

	glBindVertexArray(vao);
	glBindTexture(GL_TEXTURE_2D,textureID);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);
	GLCheck::checkGLerror(HERE);	
}
