#version 330 core

layout(location=0) in vec2 pos;
layout(location=1) in vec2 uv;
layout(location=2) in vec4 position; //4 is scale
layout(location=3) in float collision; //1 is not < 0 don't draw

uniform mat4 perspective;
uniform mat4 transform_matrix;
uniform float rotate_angle;

mat4 matrix = mat4(1.0, 0.0, 0.0, 0.0,
                   0.0, 1.0, 0.0, 0.0,
                   0.0, 0.0, 1.0, 0.0,
                   0.0, 0.0, 0.0, 1.0);

out vec2 uv_coords;

out float collision_val;

void main()
{
    collision_val = collision;

    matrix[3].xyz = position.xyz;
    float scale = position.w;

    mat4 model_matrix = transform_matrix * matrix;

    //billboarding
    model_matrix[0].xyz = vec3(1.0,0,0);
    model_matrix[1].xyz = vec3(0,1.0,0);
    model_matrix[2].xyz = vec3(0,0,1.0);

    float x = pos.x * cos(rotate_angle) - pos.y * sin(rotate_angle);
    float y = pos.y * cos(rotate_angle) + pos.x * sin(rotate_angle);

    gl_Position = perspective * model_matrix * vec4(vec2(x,y) * scale,0.0,1.0);
    uv_coords = uv;    
}
