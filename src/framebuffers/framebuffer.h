/**
 * @author Martin Prajka
 * @file framebuffer.h
 */
#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include <sstream>
#include "../window/object_properties.h"

/**
 * @brief The Quad_object class is help object for quad drawing
 */
class Quad_object
{
	GLuint vao;
	GLuint bo;

public:
	Quad_object();
	~Quad_object();

	void draw();
	void draw(glm::mat4 &perspective, glm::mat4 &view, glm::mat4 &transform);
};

/**
 * @brief The Framebuffer class: base class for working with framebuffer object
 */
class Framebuffer
{
protected:
	GLuint fbo;
	GLuint rbo;

	std::vector<GLuint> textures;
	GLuint program_fb;

	int width = 0;
    int height = 0;

	Quad_object quad;

	virtual void set_textures(int count);
	virtual void set_render_buffer();
public:
	Framebuffer() = default;
	virtual ~Framebuffer();

    void init(int width, int height,int supersampling = 1, int count_of_textures = 1);
	void set_shaders(const char *vertex, const char *fragment);	
	
	//binding data from framebuffer
	void bind_fbo();
    void unbind_fbo(GLuint old_fbo, int old_width, int old_height);
	virtual void binding_draw();

	void draw_quad();
	virtual void draw_quad(glm::mat4 &perspective, glm::mat4 &view);

	std::vector<GLuint> get_textures();
	GLuint get_framebuffer();
    int get_width() const;
    int get_height() const;
    GLuint get_shader_id() const;
};

#endif
