/**
 * @author Martin Prajka
 * @file glwindow.cpp
 * @brief file contains implement of GLWindow class
 */
#include "glwindow.h"
#include <iostream>
#include "glcheck_function.h"

GLWindow::~GLWindow()
{
	if (window!=nullptr)
		glfwDestroyWindow(window);
	if (basic_function!=nullptr)
		delete basic_function;
}

void GLWindow::init()
{
	if(!glfwInit())
		return;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    #ifdef NDEBUG
    bool cursor = false;
    #else
    bool cursor = true;
    #endif
    configuration(cursor);

    if(!window)
    	return;

    glfwMakeContextCurrent(window);
    glfwSetInputMode(window,GLFW_STICKY_KEYS, GL_TRUE);
    glfwGetWindowSize(window, &basic_function->width, &basic_function->height);
    
    glfwSetWindowUserPointer(window,this);

    glfwSetWindowSizeCallback(window,resizeGL);
    glfwSetKeyCallback(window, key_event);

    glfwSetCursorPos(window,basic_function->width/2.0,basic_function->height/2.0);

    end_application = false;
}

void GLWindow::configuration(bool &cursor)
{

#ifdef NDEBUG
    int w,h;
    const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    w = mode->width;
    h = mode->height;
	window = glfwCreateWindow(w,h,"Universe demo",glfwGetPrimaryMonitor(),nullptr);
#else	
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	window = glfwCreateWindow(1200, 720, "Universe debug", nullptr, nullptr);
	glfwSetWindowSizeLimits(window, 1200, 720, 1200, 720);
#endif
	if (!cursor)
	{
		glfwSetInputMode(window,GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}

    basic_function = new Basic_window_function(window, cursor);
    vsync = true; //default value

    std::fstream file("universe.config", std::fstream::in);
    if (!file.is_open())
    {
        //Do nothing because Basic_window_function has default value for MSAA 4 and SSAA 1
        //and VSYNC is true
        return;
    }

    std::string line;
    int msaa = 0;
    int ssaa = 0;    
    while (std::getline(file, line))    //getting data from config file
    {
        if (line.find("MSAA=") != std::string::npos)
        {
            try
            {
                msaa = std::stoi(line.substr(5, line.size() - 5),nullptr);
            }
            catch(...)
            {
                msaa = 4;
            }
        }
        if (line.find("SSAA=") != std::string::npos)
        {
            try
            {
                ssaa = std::stoi(line.substr(5, line.size() - 5),nullptr);
            }
            catch(...)
            {
                ssaa = 1;
            }
        }
        if (line.find("VSYNC=") != std::string::npos)
        {
            try
            {
                vsync = (bool)std::stoi(line.substr(6, line.size() - 6),nullptr);
            }
            catch(...)
            {
                vsync = true;
            }
        }
    }
    basic_function->set_msaa(msaa);
    basic_function->set_ssaa(ssaa);
}
void GLWindow::exec()
{
	if (!window)
		return;
	//initGL
	initializeGL();
    //setting vsync
    glfwSwapInterval(vsync ? 1 : 0);

	init_vector_scenes();

    Audio_manager::init();
	//draw scenes
	for (auto scene = scenes.begin(); scene != scenes.end(); scene++)
	{
		(*scene)->init(window, &camera, basic_function);
        active_scene = *scene;
		(*scene)->draw();
		delete (*scene);
		(*scene) = nullptr;
        if (end_application)
            break;
	}
	delete_scenes();   
	Audio_manager::dispose();
}

void GLWindow::initializeGL()
{
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
		return;
	glGetError();

    //print GPU HW
	std::cout << glGetString(GL_VENDOR) << " " << glGetString(GL_RENDERER) << " " << glGetString(GL_VERSION) << std::endl;
	
    //setting opengl
    glEnable(GL_CULL_FACE);	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
    glCullFace(GL_BACK);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


#ifndef NDEBUG
    //debug callback
    GLint flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(GLWindow::debug_output, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
#endif

	GLCheck::checkGLerror(HERE);
    camera.setProjectionMat(basic_function->width, basic_function->height); //with default farplane
}

void GLWindow::init_vector_scenes()
{
	//Shuttle scene
	scenes.push_back(new Shuttle_scene());
	//Flight scene
	scenes.push_back(new Flight_scene());
	//universe scene
	scenes.push_back(new Universe_scene());
}
void GLWindow::delete_scenes()
{
	for (auto scene = scenes.begin(); scene != scenes.end(); scene++)
	{
		if ((*scene)!=nullptr)
			delete (*scene);
	}
    active_scene = nullptr;
}

void GLWindow::resizeGL(GLFWwindow *window,int _width,int _height)
{
	GLWindow *win = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));
	win->basic_function->width = _width;
	win->basic_function->height = _height;
	
	win->camera.setProjectionMat(win->basic_function->width, win->basic_function->height);
	glViewport(0,0,win->basic_function->width, win->basic_function->height);
}

void GLWindow::key_event(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    //For compilator (without warning "unused variables")
    (void)scancode;
    (void)mods;

    GLWindow *win = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        if (win->active_scene)
            win->active_scene->end();

        win->end_application = true;
    }
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
        if (win->active_scene)
            win->active_scene->end();
    }
    if (key == GLFW_KEY_K && action == GLFW_PRESS)
    {
        win->basic_function->switch_cursor_visible();
    }
#ifndef NDEBUG
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
		if (win->active_scene)
		{
			win->active_scene->animation = false;
			win->camera.set_transition(1.0f);
		}
    }
#endif
}

void GLWindow::debug_output(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam)
{
    //warning
    (void)length;
    (void)userParam;
	// ignore non-significant error/warning codes
	//if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}
