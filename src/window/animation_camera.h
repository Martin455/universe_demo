/**
 * @author Martin Prajka
 * @brief  Extensions for Camera
 * @file   animation_camera.h
 */
#ifndef ANIMATION_CAMERA_H
#define ANIMATION_CAMERA_H

#include "camera.h"
#include <queue>
#include <cmath>

/**
 * @brief The Animation_camera class expands original Camera class
 * @details The Class implements animation logic for camera
 *              - automatic movement (linear and bezier interpolation)
 *              - automatic look (linear interpolation and look at point)
 *              - and transition for cut framebuffer (linear interpolation)
 */
class Animation_camera: public Camera
{
    enum class kind_move{
      linear, quadratic, cubic
    };
    enum class kind_look{
        angle, lookat
    };
    enum class kind_cut{
        transition, nothing
    };
    struct look_data{
        double look_end_time;
        double look_start_time;
        float end_angle;
        glm::vec2 axis_vec;
        glm::vec3 look_point;
        kind_look k;
    };
	struct move_data {
		double move_end_time;		
		double move_start_time;
		double max_time;
		glm::vec3 start_position;
		glm::vec3 end_position;
        glm::vec3 control1;
        glm::vec3 control2;
        kind_move k;
	};
    struct cut_data{
        double cut_end_time;
        double cut_start_time;
        kind_cut k;
        float end_state;
        bool cut_animation;
    };

	double animation_time = 0.0;

    bool look_animation;
    bool move_animation;
    bool cut_animation;

    glm::vec3 default_view;
	glm::vec3 default_position;

    void look_around();
    void look_at();

	void linear_move();
    void quadratic_bezier();
    void cubic_bezier();

    std::queue<look_data> look_queue;
	std::queue<move_data> move_queue;
    std::queue<cut_data> cut_queue;

    float transition;
    float default_transition;

    void cut();
public:
    Animation_camera(): Camera(), look_animation(false), move_animation(false)
    {
        transition = 1.0f;
        cut_animation = false;
		default_view = glm::vec3(0.f, 0.f, -1.f);
    }

    void take_look_around(float end_angle, const glm::vec2 &axis_vec,double start_time, double end_time);
    void look_at_point(glm::vec3 look_point, double start_time, double end_time);
    
	void move_to_point(const glm::vec3 &start_point,const glm::vec3 &target_point, double start_time, double end_time);
	void move_quadratic_bezier(const glm::vec3 &start_point, const glm::vec3 &target_point, const glm::vec3 &control_point, double start_time, double end_time);    
	void move_cubic_bezier(const glm::vec3 &start_point, const glm::vec3 &target_point, const glm::vec3 &control_point1, const glm::vec3 &control_point2, double start_time, double end_time);

    void set_cut(double start_time, double end_time, bool to_dark = true);
    void no_cut(double time);

    void set_transition(float value);

	void animate_camera(double time);
    void clear_animation();

    float get_transition() const;
};

#endif // ANIMATION_CAMERA_H
