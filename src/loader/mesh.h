/**
 * @author Martin Prajka
 * @file   mesh.h
 */
#ifndef MESH_H
#define MESH_H

#include <vector>
#include <glm/glm.hpp>

struct Mesh {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texture_uv;
	std::vector<unsigned int> indices;

	unsigned get_vertices_byte() const
	{
		return vertices.size() * 3 * sizeof(float);
	}
	unsigned get_normals_byte() const
	{
		return normals.size() * 3 * sizeof(float);
	}
	unsigned get_texture_uv_byte() const
	{
		return texture_uv.size() * 2 * sizeof(float);
	}
	unsigned get_indices_byte() const
	{
		return indices.size() * sizeof(unsigned int);
	}
};

#endif
