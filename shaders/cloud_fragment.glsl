#version 330 core

in vec2 uv_coords;
out vec4 output_color;

in float collision_val;

uniform sampler2D tex;

void main()
{
    if (collision_val < 0.0)   //if is collision so don't draw particle
        discard;

    float alpha = max(texture(tex, uv_coords).a - (1.0 - collision_val), 0.0);

    output_color = vec4(vec3(0.75,0.75,0.75), alpha);
}
