#version 330 core

layout(location=0) in vec2 pos;
layout(location=1) in vec2 uv_coord;

uniform mat4 perspective;
uniform mat4 view;

uniform float scale_number;

out vec2 uv;
out float life;

void main()
{
	mat4 model_matrix = view;

	model_matrix[0].xyz = vec3(1.0,0.0,0.0);
	model_matrix[1].xyz = vec3(0.0,1.0,0.0);
	model_matrix[2].xyz = vec3(0.0,0.0,1.0);

	uv = uv_coord;
	life = scale_number;
    gl_Position = perspective * model_matrix * vec4(pos * scale_number, 0.0, 1.0);
}
