/**
 * @author Martin Prajka
 * @file   skybox.cpp
 */
#include "skybox.h"
#include "../window/glcheck_function.h"

void Skybox::init(Mesh &mesh, const char *texture_file)
{
	num_of_vertex = mesh.vertices.size();

	//nastaveni vertex objectu
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	//nasteveni vertexu
	glGenBuffers(1,&vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER,mesh.get_vertices_byte(),&mesh.vertices[0],GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,0);
	//nastaveni UV souradnic
	glGenBuffers(1,&uvbo);
	glBindBuffer(GL_ARRAY_BUFFER,uvbo);
	glBufferData(GL_ARRAY_BUFFER,mesh.get_texture_uv_byte(),&mesh.texture_uv[0],GL_STATIC_DRAW);	

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(float)*2,0);
	//nastaveni textury
	textureID = Texture::create_texture(texture_file);

	GLCheck::checkGLerror(HERE);
}

void Skybox::draw(glm::mat4 &transformMat, GLuint shader)
{
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glCullFace(GL_FRONT);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,textureID);
	glBindVertexArray(vao);	

	GLuint uni_location = glGetUniformLocation(shader,"projection");
	glUniformMatrix4fv(uni_location,1,GL_FALSE,&transformMat[0][0]);

	glDrawArrays(GL_TRIANGLES,0,num_of_vertex);

	glEnable(GL_DEPTH_TEST);
	
	glDepthMask(GL_TRUE);
	glCullFace(GL_BACK);
	GLCheck::checkGLerror(HERE);
}
