#version 330 core

layout(location=0) in vec2 pos;
layout(location=1) in vec2 uv_coord;
layout(location=2) in vec3 position;

mat4 trans_instance = mat4(1.0, 0.0, 0.0, 0.0,
                         0.0, 1.0, 0.0, 0.0,
                         0.0, 0.0, 1.0, 0.0,
                         0.0, 0.0, 0.0, 1.0);

uniform mat4 projection;
uniform mat4 view;

out vec2 uv;

void main()
{
    trans_instance[3].xyz = position.xyz;

    mat4 modelView = view * trans_instance;
    modelView[0].xyz = vec3(1.0,0,0);
    modelView[1].xyz = vec3(0,1.0,0);
    modelView[2].xyz = vec3(0,0,1.0);

    gl_Position = projection * modelView * vec4(pos * 2.0, 0.0, 1.0);
    uv = uv_coord;
}
