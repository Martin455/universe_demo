/**
 * @file bloom_fb.cpp
 * @author Martin Prajka
 * @brief Implementation Bloom_effect class
 */
#include "bloom_fb.h"
#include "../window/glcheck_function.h"

Bloom_effect::~Bloom_effect()
{
	glDeleteTextures(2, textures_bfb);
	glDeleteTextures(1, &hdr_tex);
	glDeleteRenderbuffers(1, &hdr_rbo);
	glDeleteFramebuffers(2, b_framebuffers);
	glDeleteFramebuffers(1, &hdr_fb);
	glUseProgram(0);
	glDeleteProgram(blur_shader);
	glDeleteProgram(bloom_shader);
	glDeleteProgram(prepare_shader);
}

void Bloom_effect::init(int width, int height, int down_sample)
{
	//default framebuffer for prerending scene
    Framebuffer::init(width, height, 2);
    this->down_sample = down_sample;
    
	init_hdr_fb();
	init_bfb();
}
void Bloom_effect::init_hdr_fb()
{
	glGenFramebuffers(1, &hdr_fb);
	glBindFramebuffer(GL_FRAMEBUFFER, hdr_fb);
	glGenTextures(1, &hdr_tex);

    glBindTexture(GL_TEXTURE_2D, hdr_tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,width/down_sample, height/down_sample, 0, GL_RGB, GL_FLOAT, NULL);
        
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);  // we clamp to the edge as the blur filter would otherwise sample repeated texture values!
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, hdr_tex, 0);


	GLCheck::checkGLFBstatus(HERE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
void Bloom_effect::init_bfb()
{
 	glGenFramebuffers(2, b_framebuffers);
 	glGenTextures(2, textures_bfb);
 	set_textures_bfb();
 	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Bloom_effect::set_textures_bfb()
{
	//specific texture so I can't use Texture:: 
	for (unsigned i = 0; i < 2; ++i)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, b_framebuffers[i]);
		glBindTexture(GL_TEXTURE_2D, textures_bfb[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width/down_sample, height/down_sample, 0, GL_RGB, GL_FLOAT, NULL);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures_bfb[i], 0);

		GLCheck::checkGLFBstatus(HERE);
	}
}
void Bloom_effect::debug_draw(GLuint previos_fb)
{
	glBindFramebuffer(GL_FRAMEBUFFER, previos_fb);
	glViewport(0, 0, width, height);
	
	glUseProgram(program_fb);

	GLuint loc = glGetUniformLocation(program_fb, "tex");
	glUniform1i(loc, 0);
	loc = glGetUniformLocation(program_fb, "tex1");
	glUniform1i(loc, 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures_bfb[0]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textures_bfb[1]);

	draw_quad();

	GLCheck::checkGLerror(HERE);
}
void Bloom_effect::prepare_texture()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, hdr_fb);
 	glViewport(0, 0, width/down_sample, height/down_sample);

	glUseProgram(prepare_shader);
	glBindTexture(GL_TEXTURE_2D,textures[0]); //whole scene in texture

	quad.draw();

}
void Bloom_effect::active_bloom()
{
	prepare_texture();

	bool horizontal = true;
	bool first = true;
	unsigned amount = 6;

	//Activation blur effect
	glUseProgram(blur_shader);

	for (unsigned i = 0; i < amount; ++i)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, b_framebuffers[horizontal]);
		glViewport(0, 0, width/down_sample, height/down_sample);

		GLuint horizontal_location = glGetUniformLocation(blur_shader, "horizontal");
		glUniform1i(horizontal_location, horizontal);
		GLuint bind_texture = first ? hdr_tex : textures_bfb[!horizontal];
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, bind_texture);

		quad.draw();
		
	 	horizontal = !horizontal;
	 	if (first)
	 		first = false;
	}

	final_texture = unsigned(!horizontal);
}
void Bloom_effect::draw_bloom()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(bloom_shader);

	GLuint texture_location = glGetUniformLocation(bloom_shader, "tex_scene");
	glUniform1i(texture_location, 0);
	texture_location = glGetUniformLocation(bloom_shader, "tex_blur");
	glUniform1i(texture_location, 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textures_bfb[final_texture]);

	quad.draw();
}
void Bloom_effect::set_blur_shader(const char *vertex, const char *fragment)
{
	blur_shader = Shader::create_program(vertex, fragment);
	glUseProgram(blur_shader);
	glUseProgram(0);
}
void Bloom_effect::set_bloom_shader(const char *vertex, const char *fragment)
{
	bloom_shader = Shader::create_program(vertex, fragment);
}
void Bloom_effect::set_prepare_shader(const char *vertex, const char *fragment, const glm::vec3 &threshold)
{
	prepare_shader = Shader::create_program(vertex, fragment);
    glUseProgram(prepare_shader);
    GLuint location = glGetUniformLocation(prepare_shader, "threshold");
    glm::vec3 vec = threshold;
    glUniform3fv(location, 1, &vec[0]);
    glUseProgram(0);
}
