/**
 * @author Martin Prajka
 * @file   skybox.h
 */
#ifndef SKYBOX_H
#define SKYBOX_H

#include "g_object.h"

class Skybox: public G_object
{
public:
	void init(Mesh &mesh, const char *texture_file);
	void draw(glm::mat4 &transformMat, GLuint shader);
};

#endif
