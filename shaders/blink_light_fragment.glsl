#version 330 core

in vec2 uv;

uniform sampler2D tex;
uniform float time;

out vec4 out_color;

void main()
{
	float alpha = texture(tex,uv).g;
	vec3 red_color = vec3(1.0,0.0,0.0);

	float out_alpha = alpha * abs(sin(time));

	out_color = vec4(red_color, out_alpha);
}