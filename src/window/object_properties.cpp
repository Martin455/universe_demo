/**
 * @author Martin Prajka
 * @file object_properties.cpp
 */
#include "object_properties.h"
#include <exception>
#include <stdexcept>
#include <sstream>
#include <iostream>

//SHADER CLASS

//static methods
GLuint Shader::create_program(const char *vertexfile,const char *fragmentfile)
{
	std::string fragmentCode = get_shader_code(fragmentfile);
	std::string vertexCode = get_shader_code(vertexfile);
	const char *tmp_vex = vertexCode.c_str();
	const char *tmp_freg = fragmentCode.c_str();

	GLuint vertexshader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexshader,1,&tmp_vex,0);
	glShaderSource(fragmentshader,1,&tmp_freg,0);

	glCompileShader(vertexshader);
	glCompileShader(fragmentshader);

	check_shader(vertexshader);
	check_shader(fragmentshader);

	GLuint program = glCreateProgram();
	glAttachShader(program,vertexshader);
	glAttachShader(program,fragmentshader);
	glLinkProgram(program);

	check_program(program);

	glDeleteShader(vertexshader);
    glDeleteShader(fragmentshader);
    return program;
}

std::string Shader::get_shader_code(const char *filename)
{
	std::string shader_code("");

	std::ifstream f_stream(filename, std::ios::in);
	if(!f_stream.is_open())
		throw std::runtime_error("Can't open shader file");

	std::string line("");
	while(getline(f_stream,line))
		shader_code += "\n" + line;
	f_stream.close();

	return shader_code;
}

void Shader::check_shader(GLuint shaderID)
{
    GLint compileStatus;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compileStatus);
    if (compileStatus != GL_TRUE)
    {
        GLint infoLogLength;
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar* buffer = new GLchar[infoLogLength]();

        glGetShaderInfoLog(shaderID, infoLogLength, nullptr, buffer);
        std::stringstream s;
        s << __FILE__ << ": " << __func__ << ": " << __LINE__ << ": " << buffer << std::endl;
        delete[] buffer;
        throw s.str();
	}
}
void Shader::check_program(GLuint programID)
{
	GLint linkStatus;
    glGetProgramiv(programID, GL_LINK_STATUS, &linkStatus);
    if (linkStatus != GL_TRUE)
    {
        GLint infoLogLength;
        glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar* buffer = new GLchar[infoLogLength]();

        glGetProgramInfoLog(programID, infoLogLength, nullptr, buffer);
        std::stringstream s;
        s << __FILE__ << ": " << __func__ << ": " << __LINE__ << ": " << buffer << std::endl;
        delete[] buffer;
        throw s.str();
	}
}
//instance methods
Shader::~Shader()
{
    glUseProgram(0);
    glDeleteProgram(id);
}

void Shader::set_shader(const char *vertexfile,const char *fragmentfile)
{
    id = create_program(vertexfile, fragmentfile);
}

void Shader::bind()
{
    glUseProgram(id);
}
void Shader::unbind()
{
    glUseProgram(0);   
}
GLuint Shader::get_program_id()
{
    return id;
}

void Shader::set_mat4(const char *name, glm::mat4 &matrix)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}

void Shader::set_mat3(const char *name, glm::mat3 &matrix)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniformMatrix3fv(location, 1, GL_FALSE, &matrix[0][0]);
}

void Shader::set_vec4(const char *name, glm::vec4 &vec)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniform4fv(location, 1, &vec[0]);
}

void Shader::set_vec3(const char *name, glm::vec3 &vec)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniform3fv(location, 1, &vec[0]);
}


void Shader::set_float(const char *name, float value)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniform1f(location, value);
}

void Shader::set_int(const char *name, int value)
{
    GLuint location = glGetUniformLocation(id, name);
    glUniform1i(location, value);
}

//TEXTURE CLASS
GLuint Texture::create_texture(const char *filename, bool mipmap)
{
	int img_w,img_h;
	unsigned char *img = SOIL_load_image(filename,&img_w,&img_h,0,SOIL_LOAD_RGBA);
	if (!img)
	{
		std::stringstream s;
        s << __FILE__ << ": " << __func__ << ": " << __LINE__ << " Can't create texture: " << filename;
		throw std::runtime_error(s.str());
	}
	
	GLuint texture_id;
	glGenTextures(1,&texture_id);
	glBindTexture(GL_TEXTURE_2D,texture_id);

	glTexImage2D(GL_TEXTURE_2D,0, GL_RGBA, img_w, img_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);
	SOIL_free_image_data(img);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
	
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	
	if (mipmap)
	{
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);	
	}

	return texture_id;
}

GLuint Texture::create_empty_texture(int width, int height, bool wrap)
{
	GLuint texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	if (wrap)
	{
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);	
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

	return texture_id;
}

GLuint Texture::create_multisample_texture(int samples, int width, int height)
{
	GLuint texture_id;

	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, texture_id);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGBA8, width, height, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

	return texture_id;
}
