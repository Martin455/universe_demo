/**
 * @author Martin Prajka
 * @file   flight_scene.h
 */
#ifndef FLIGHT_SCENE_H
#define FLIGHT_SCENE_H

#include "scene.h"
#include "../objects/skybox_dynamic.h"
#include "../objects/rocket_interier_object.h"
#include "../framebuffers/antialias_fb.h"
#include "../window/object_properties.h"
#include "../particles/local_fog.h"
#include "../framebuffers/blur_framebuffer.h"
#include <vector>
#include <array>

class Flight_scene: public Scene
{
    //shaders
    Shader obj_glow_shader;
    Shader obj_shader;
    Shader skybox_shader;
    Shader window_shader;
    Shader blur_shader;

	//objects
    std::array<Rocket_interier_object, 8> objects;

    Local_fog clouds;
    std::vector<glm::vec3> clouds_position;

    //collision box for rocket
    std::vector<glm::vec3> collision_block;

    void sort_clouds(std::vector<glm::vec3> &clouds_vec, glm::vec3 &cam_pos);

    //Inner window class
    class Window_obj
    {
        glm::mat4 model_matrix;
        float shear;
        GLuint vao;
        GLuint vbo;
    public:
        ~Window_obj();
        void init(int number_of_win); //0 = left, 4 right
        void update(glm::mat4 &projection, glm::mat4 &viewMatrix, GLuint shader);
        void draw();
    };

    std::vector<Window_obj> windows;
    
    Skybox_dynamic skybox;

    Antialiasing multisampling;
    Framebuffer cut_framebuffer;
    Blur_framebuffer blur_framebuffer;

    //light colors
    glm::vec3 ambient_color;
    glm::vec3 diffuze_color;

    void set_scene_colors(GLuint programID);

    //varaibles for skybox and dynamic change lighting
    bool switch_colors;
    bool dark_sky;
    bool stars;
    double dark_time;

    float clouds_speed;
    bool shake_effect;

    float frequency;

    float dark_mix;
    float stars_mix;

	bool end_scene;

    void set_animation();
protected:
	virtual void painting();
	virtual void key_event();
    virtual void mouse_event();
public:
	Flight_scene() = default;
    virtual void init(GLFWwindow *win,Animation_camera *cam, Basic_window_function *bwf);
	virtual void draw();
    virtual void end();
};
#endif
