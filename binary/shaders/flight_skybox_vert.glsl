#version 330 core

layout(location=0) in vec3 vertexs;
layout(location=1) in vec2 uv;

out vec2 uv_coords;

uniform mat4 projection;

void main()
{
    vec4 position = projection * vec4(vertexs,1.0);
    gl_Position = position.xyww;
    uv_coords = uv;
}
