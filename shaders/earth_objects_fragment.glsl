#version 330 core

in vec2 uv_coords;
in vec3 pos;
in vec3 normal;
in vec4 frag_pos_light;
in vec4 view_space;

out vec4 color;

uniform sampler2D tex;
uniform sampler2DShadow shadow_map;

uniform vec3 light_position;
uniform vec3 cam_pos;

float get_shadow()
{
    // PCF algorithm
    float shadow = 0.0;
    vec2 texel_size = 1.0 / textureSize(shadow_map, 0);

    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            shadow += textureProj(shadow_map, vec4(frag_pos_light.xy
                                    + vec2(x, y)
                                    * texel_size, frag_pos_light.zw));

        }
    }
    //float bias = max(0.05 * (1.0 - dot(normal, light_position)), 0.005);

    //9.0 samples
    return shadow / 9.0;
}

void main()
{
    //white light with ambient strength 0.4
    vec3 ambientLight = vec3(0.4,0.4,0.4);

    vec3 normals = normalize(normal);
	//diffuze
    vec3 light_direction = normalize(light_position - pos);
	vec3 color_diffuze = vec3(0.55,0.55,0.55); 
        float diffuze_intenzity = dot(light_direction,normals);
    vec3 light_intenzity = color_diffuze * diffuze_intenzity;

	//spectular
    vec3 eye_direction = normalize(cam_pos-pos); //camera_pos - pos
    vec3 reflection_direction = reflect(-light_direction, normals);
    float spectular_intenzity = max(dot(reflection_direction, eye_direction), 0.0);
    spectular_intenzity = pow(spectular_intenzity, 8.0);
    light_intenzity += color_diffuze * spectular_intenzity;

    if (dot(normal, light_position) < 0.001)
    {
        light_intenzity = light_intenzity + ambientLight;
    }
    else
    {
        float shadow = get_shadow();
        light_intenzity = (shadow * light_intenzity) + ambientLight;
    }

    float dist = abs(view_space.z / view_space.w);

    //Fog examples
    //linear fog
//    float fog_start = 25.0;
//    float fog_end = 200.0;
//    float fog_factor = (fog_end - dist) / (fog_end - fog_start);
    //exponencional fog
//    float fog_density = 0.01;
//    float fog_factor = exp(-(dist * fog_density));
    //square exponencional fog
//    float fog_density = 0.01;
//    float fog_factor = exp(-((dist * fog_density)*(dist * fog_density)));

    float fog_density = 0.002;

    float fog_factor = exp(-((dist * fog_density) * (dist * fog_density)));

    fog_factor = clamp(fog_factor, 0.0, 1.0 );

    vec4 object_color = texture(tex, uv_coords);
    vec3 final_color = mix((object_color.rgb * light_intenzity), vec3(0.6,0.6,0.6),1.0 - fog_factor);

    color = vec4(final_color, object_color.a);
}
