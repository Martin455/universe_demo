/**
 * @author Martin Prajka
 * @file   rocket_interier_object.cpp
 */
#include "rocket_interier_object.h"
#include "../window/glcheck_function.h"

void Rocket_interier_object::set_model_transform(const glm::mat4 &transform_matrix)
{
    transform = transform_matrix;
}

void Rocket_interier_object::set_glow_texture(const char *glow_texture)
{
    this->glow_texture = Texture::create_texture(glow_texture);
}

void Rocket_interier_object::update(GLuint shader_id, glm::mat4 &perspective, glm::mat4 &view_matrix)
{
    GLuint location = glGetUniformLocation(shader_id,"perspective");
    glUniformMatrix4fv(location,1,GL_FALSE,&perspective[0][0]);

    location = glGetUniformLocation(shader_id,"view");
    glUniformMatrix4fv(location,1,GL_FALSE,&view_matrix[0][0]);

    location = glGetUniformLocation(shader_id, "model");
    glUniformMatrix4fv(location,1,GL_FALSE,&transform[0][0]);

    if (glow_texture != 0)
    {
        glActiveTexture(GL_TEXTURE0);
        location = glGetUniformLocation(shader_id, "tex");
        glUniform1i(location, 0);
        glBindTexture(GL_TEXTURE_2D,textureID);

        glActiveTexture(GL_TEXTURE1);
        location = glGetUniformLocation(shader_id, "glow_tex");
        glUniform1i(location, 1);
        glBindTexture(GL_TEXTURE_2D,glow_texture);
    }

    GLCheck::checkGLerror(HERE);
}

void Rocket_interier_object::draw()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES, 0, num_of_vertex);

    GLCheck::checkGLerror(HERE);
}

Rocket_interier_object::~Rocket_interier_object()
{
    glDeleteTextures(1, &glow_texture);
}
