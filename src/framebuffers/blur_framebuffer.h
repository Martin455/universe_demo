/**
 * @file blur_framebuffer.h
 * @author Martin Prajka
 * @brief Declaration Blur_framebuffer class
 */
#ifndef BLUR_FRAMEBUFFER_H
#define BLUR_FRAMEBUFFER_H

#include "framebuffer.h"
#include <array>

class Blur_framebuffer
{
    GLuint fbo;
    GLuint rbo;

    std::array<GLuint, 2> textures;

    int width, height;
    int active_texture; //only index to array

    Quad_object quad;

public:
    Blur_framebuffer() = default;
    virtual ~Blur_framebuffer();

    void init(int height, int width);
    void bind();
    void unbind(GLuint old_fbo, int old_width, int old_height);

    void binding_draw(Shader &shader);
    void draw();
    void swap_textures();
    GLuint get_fbo() const;
    int get_width() const;
    int get_height() const;
};

#endif // BLUR_FRAMEBUFFER_H
