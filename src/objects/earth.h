/**
 * @author Martin Prajka
 * @file earth.h
 */
#ifndef EARTH_H
#define EARTH_H

#include "planet.h"
#include "../loader/loader.h"
#include "../window/object_properties.h"

class Earth: public Planet
{
	G_object *clouds = nullptr;
	glm::mat4 cloud_matrix;
    Shader cloud_shader;
    GLuint default_shader;

public:
	Earth(const float _angle,const float _def_axis, const float _maj_axis,
		   const float _min_axis, const float _max_speed, const float _avg_speed, 
		   const float _min_speed,const float _speed_rotate, const float _scale)
	: Planet(_angle,_def_axis,_maj_axis,_min_axis,_max_speed,_avg_speed,
			 _min_speed,_speed_rotate,_scale) {}
	
    virtual void update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime);
	virtual void draw();
	virtual ~Earth();

	void set_clouds(); //pro pripadne nastaveni oblacnosti zeme (zap/vyp)
};

#endif
