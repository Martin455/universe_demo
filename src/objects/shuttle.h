/**
 * @author Martin Prajka
 * @file   shuttle.h
 */
#ifndef SHUTTLE_H
#define SHUTTLE_H

#include "g_object.h"
#include "../constants.h"

class Shuttle:public G_object
{
    glm::mat4 model_matrix;
public:
    Shuttle() = default;

    void init(Mesh &mesh, const char *texture_name);
    void prepare_depth(GLuint depth_shader);
    void update(Shader &shader, GLuint shadowmap);
    void draw();
};
#endif
