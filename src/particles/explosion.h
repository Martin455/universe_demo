/**
 * @author Martin Prajka
 * @file   explosion.h
 */
#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include <array>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <thread>
#include "../window/object_properties.h"
#include "../loader/mesh.h"
#include "../audio/audio_manager.h"

class Explosion
{
	//GL staff for drawing
	//Count of items are static
	std::array<GLuint, 3> vertex_arrays;
	std::array<GLuint, 10> textures;
	std::array<GLuint, 3> shaders;
	std::array<GLuint, 2> stream_buffers;
	//GL buffers for static data
	std::array<GLuint, 3> buffers;

	glm::mat4 perspective;
	glm::mat4 transform_matrix;
    bool sound;

	unsigned count_rock_vertecies;
    //Data for particles
	struct particle_data {
		glm::vec3 position;
		glm::vec3 direction;
		float life;
		bool active;
		float speed;
        int kind;
	};
    //data for particles (emitor particles)
	struct line_fire {
		glm::vec3 position;
		glm::vec3 direction;
		float life;
		float speed;
        std::vector<particle_data> fires;
	};

	bool is_back_direction; //for reverse gear in flash
	float shockwave_scale;
	float flash_speed;
    float dust_transition; //for fire parts
	
	//storage for particles data
	std::vector<particle_data> frf_data; //flash rock fire data [frf]
	std::vector<line_fire> line_emiter;

	//GPU buffers data
	std::vector<GLfloat> gpu_frf; //GPU flash rock fire
	std::vector<GLfloat> gpu_emiter;

	//Constants for explosion
	enum {
		//count of particle
		NUM_FIRE = 5500, NUM_ROCK = 1000, NUM_FLASH = 200,
		NUM_LINE_EMITERS = 60, NUM_MAX_LINES = 20,
		//kind of particle
		FIRE_PARTS = 0, ROCK_PARTS = 1, FLASH_PARTS = 2,
		FIRE_LINE_PARTS = 3
	};
	enum kind_texture {
		SMOKET = 0, SMOKET2, SMOKET3, SMOKET4,
        ROCKET, ROCKT, FIRET, FLASHT, SWT, FIRET2
	};

	const float GLOBAL_SPEED = 1.0f;
	float frame_time;

	//init object methods
	void set_object_buffers(GLfloat *mesh, size_t mesh_size, Mesh &rock);
	void set_vao_frf();
	void set_vao_line_emiter();
	void set_vao_shockwave();

	void set_textures();
	void set_shaders();

	//inits explosion methods
	void init_fire();
	void init_rock();
	void init_flash();
	void init_line_emiter();
    void init_line(line_fire &emiter);

	//update methods
	void update_rock();
	void update_fire();
	void update_flash();
	void update_line_emiter();
	void update_line_fire(line_fire &emiter);

	void update_gpu();
	
	bool check_active();
	bool is_flash;

	//help variables for checking and repeat explosion
	bool first_repeat;
	double last_time;

	//draw methods
	void draw_frf();
	void draw_line_emiter(double time);	
	void draw_shockwave();
public:
	Explosion() = default;
	~Explosion();

	void init(Mesh &meteor);
	void draw(glm::mat4 perspective, glm::mat4 view_matrix,glm::vec3 &position, float frametime, bool repeat_explosion = false, double time = 0.0);
};

#endif
