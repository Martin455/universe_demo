#version 330 core

in vec2 uv_coords;
out vec4 out_color;

uniform sampler2D tex_scene;
uniform sampler2D tex_blur;

void main()
{
	const float gamma = 2.2;
	vec3 scene_texel = texture(tex_scene, uv_coords).rgb;
	vec3 bloom_color = texture(tex_blur, uv_coords).rgb;

	scene_texel += bloom_color;

	//scene_texel = pow (scene_texel, vec3(1.0/gamma));

	out_color = vec4(scene_texel, 1.0);
}