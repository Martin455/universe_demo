/**
 * @author Martin Prajka
 * @file   uranus.h
 */
#ifndef URANUS_H
#define URANUS_H

#include "planet.h"
#include "ring.h"
#include "../window/object_properties.h"

class Uranus: public Planet
{
	Meteor_Ring ring;
	double time_for_ring;
    Shader ring_shader;
public:
	Uranus(const float _angle,const float _def_axis, const float _maj_axis,
		   const float _min_axis, const float _max_speed, const float _avg_speed, 
		   const float _min_speed,const float _speed_rotate, const float _scale)
	: Planet(_angle,_def_axis,_maj_axis,_min_axis,_max_speed,_avg_speed,
			 _min_speed,_speed_rotate,_scale) {}
	
    virtual void update(double time, glm::vec3 &sun_position, glm::mat4 &viewMatrix, GLuint shader, float frametime);
    virtual void draw();

	virtual ~Uranus() = default;
	void init_ring(G_object &meteor);
};

#endif //URANUS_H
