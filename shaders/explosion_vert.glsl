#version 330 core

layout(location=0) in vec2 pos;
layout(location=1) in vec2 uv_c;
layout(location=2) in vec4 instanc_pos; // prvek w = zivot castice
layout(location=3) in float kind;

mat4 transform_i = mat4(1.0, 0.0, 0.0, 0.0,
						0.0, 1.0, 0.0, 0.0,
						0.0, 0.0, 1.0, 0.0,
						0.0, 0.0, 0.0, 1.0);

uniform mat4 per_mat;
uniform mat4 view;

uniform float dust_transition;

out vec2 uv;
out float len;
out float life;
flat out int instance_id;
flat out int kindi;

const float a = -0.1;
const float b = 1.5;


void main()
{
	kindi = int(kind);
    instance_id = gl_InstanceID;
    
	len = length(instanc_pos.xyz);

    //move instance
	transform_i[3].xyz = instanc_pos.xyz;

	mat4 modelView = view * transform_i;
	
	//billboarding
	modelView[0].xyz = vec3(1.0,0,0);
	modelView[1].xyz = vec3(0,1.0,0);
	modelView[2].xyz = vec3(0,0,1.0);

	uv = uv_c;
	life = instanc_pos.w;	
	vec2 position;

    //scale
	if (kindi == 0)
    {
        float scale = dust_transition + 1.0;
        position = pos * pow(scale, 1.5);
    }
	else if (kindi == 1)
        position = pos * 0.08;
	else if (kindi == 2)
        position = pos * 2.6;
	else if (kindi == 3)
	{
		float life_particle = a * instanc_pos.w + b;

        position = pos * 0.5 * life_particle;
    }

    gl_Position = per_mat * modelView * vec4(position, 0.0,1.0);
}
