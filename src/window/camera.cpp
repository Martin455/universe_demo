/**
 * @author Martin Prajka
 * @brief  File with implementation Camera class
 * @file   camera.cpp
 */
#include "camera.h"

Camera::Camera(): 
	viewDirection(0.0f,0.0f,-1.0f), UP(0.0f,1.0f,0.0f), cameraSpeed(4.5f), mouseSensitive(0.45f) 
{
    far_plane = 500.0f; //default farplane
}

void Camera::move_forward(float frametime)
{
	position += (cameraSpeed * frametime) * viewDirection;
}

void Camera::move_backward(float frametime)
{
	position += (-cameraSpeed * frametime) * viewDirection;
}

void Camera::move_left(float frametime)
{
	glm::vec3 strafe_dir = glm::cross(viewDirection,UP);
	position += (-cameraSpeed * frametime) * strafe_dir;
}

void Camera::move_right(float frametime)
{
	glm::vec3 strafe_dir = glm::cross(viewDirection,UP);
	position += (cameraSpeed * frametime) * strafe_dir;
}

void Camera::active_sprint()
{
	cameraSpeed = 15.7f;
}
void Camera::super_sprint()
{
    cameraSpeed = 30.8f;
}

void Camera::deactive_sprint()
{
	cameraSpeed = 4.5f;
}

void Camera::mouseMove(const glm::vec2 &new_position)
{
	glm::vec2 delta_position = new_position - old_mouse_position;

	if (glm::length(delta_position) > 40.0f)
	{
		old_mouse_position = new_position;
		return;
	}

	viewDirection = glm::mat3(glm::rotate(deg_to_rad(-delta_position.x)*mouseSensitive,UP)) * viewDirection;
	glm::vec3 rotate_y = glm::cross(viewDirection,UP);
	viewDirection = glm::mat3(glm::rotate(deg_to_rad(-delta_position.y)*mouseSensitive,rotate_y)) * viewDirection;
	old_mouse_position = new_position;
}

void Camera::setProjectionMat(int width, int height)
{
    projectMat = glm::perspective(deg_to_rad(60),((float)width)/height,0.1f,far_plane);
}

void Camera::set_position(glm::vec3 new_position)
{
    position = new_position;
}

void Camera::set_direction(glm::vec3 new_direction)
{
	viewDirection = new_direction;
}

void Camera::set_far_plane(float value)
{
    far_plane = value;
}

glm::mat4 Camera::getProjectionMat() const
{
	return projectMat;
}

glm::mat4 Camera::getViewMatrix() const
{
	return glm::lookAt(position,position + viewDirection,UP);
}

glm::vec3 Camera::get_position() const 
{
    return position;
}

glm::vec3 Camera::get_direction() const
{
    return viewDirection;
}

float Camera::get_far_plane() const
{
    return far_plane;
}
